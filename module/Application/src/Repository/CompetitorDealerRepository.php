<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-2-18
 * Time: 12:33
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class CompetitorDealerRepository extends EntityRepository
{
    public function getBrands()
    {
        $querybuilder = $this->createQueryBuilder('c');

        //TODO get just brands from specific project
        $result = $querybuilder->select('c.brand')
            ->groupBy('c.brand')
            ->orderBy('c.brand', 'ASC')
            ->getQuery()->getResult();

        $result = array_column($result, 'brand');
        $result = array_combine($result, $result);

        return $result;
    }
}
