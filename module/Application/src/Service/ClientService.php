<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:12
 */

namespace Application\Service;

use Application\Entity\Client;
use Doctrine\ORM\EntityManager;

class ClientService
{
    /**
     * @var \Doctrine\ORM\EntityManager $entityManager
     */
    private $entityManager;

    /**
     * ClientService constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getClients()
    {
        return $this->entityManager->getRepository(Client::class)->findAll();
    }
}
