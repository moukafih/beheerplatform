<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 16-11-17
 * Time: 16:06
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="visits")
 *
 * @category Application
 * @package  Entity
 */
class VisitCompetitor extends Visit
{
    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\CompetitorDealer")
     * @ORM\JoinColumn(name="location", referencedColumnName="dealer_id")
     * @var \Application\Entity\AbstractDealer $location
     */
    protected $location;
}
