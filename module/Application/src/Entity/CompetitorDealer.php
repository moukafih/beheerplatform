<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 16-11-17
 * Time: 10:09
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\CompetitorDealerRepository")
 * @ORM\Table(name="external_location")
 *
 * @category Application
 * @package  Entity
 */
class CompetitorDealer extends AbstractDealer
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $brand;

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /** @param $data */
    public function exchangeArray($data)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->brand = isset($data['brand']) ? $data['brand'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->address = isset($data['address']) ? $data['address'] : null;
        $this->zipcode = isset($data['zipcode']) ? $data['zipcode'] : null;
        $this->city = isset($data['city']) ? $data['city'] : null;
    }

    /** @return mixed */
    public function getArrayCopy()
    {
        // TODO: Implement getArrayCopy() method.
    }
}
