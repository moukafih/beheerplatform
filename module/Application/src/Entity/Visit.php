<?php

/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 11-4-17
 * Time: 12:59
 */

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="visits")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"renault" = "VisitRenault", "competitor" = "VisitCompetitor"})
 *
 * @category Application
 * @package  Entity
 */
abstract class Visit implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Project")
     * @ORM\JoinColumn(name="project", referencedColumnName="id")
     * @var \Application\Entity\Project $project
     */
    private $project;

    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="Renault\Entity\User")
     * @ORM\JoinColumn(name="advisor", referencedColumnName="user_id")
     * @var \Renault\Entity\User $advisor
     */
    private $advisor;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var datetime
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $remark;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    private $active = true;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $reason;

    /**
     * One Visit has One Report.
     * @ORM\OneToOne(targetEntity="Survey\Entity\Report", mappedBy="visit", orphanRemoval=true)
     * @var \Survey\Entity\Report
     */
    private $report;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return \Application\Entity\AbstractDealer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param \Application\Entity\AbstractDealer $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return \Renault\Entity\User
     */
    public function getAdvisor()
    {
        return $this->advisor;
    }

    /**
     * @param \Renault\Entity\User $advisor
     */
    public function setAdvisor($advisor)
    {
        $this->advisor = $advisor;
    }

    /**
     * @return datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return \Survey\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param \Survey\Entity\Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    public function exchangeArray($data)
    {
        $this->id     = isset($data['id']) ? $data['id'] : null;
        $this->project = isset($data['project']) ? $data['project'] : null;
        $this->location  = isset($data['location']) ? $data['location'] : null;
        $this->advisor  = isset($data['advisor']) ? $data['advisor'] : null;
        $this->date  = isset($data['date']) ? $data['date'] : null;
        $this->remark  = isset($data['remark']) ? $data['remark'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'project' => $this->project,
            'location'  => $this->location,
            'advisor'  => $this->advisor,
            'date'  => $this->date,
            'remark'  => $this->remark,
        ];
    }
}
