<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 14:52
 */

namespace Application\Entity;

use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

trait EntityTrait
{
    /**
     * @return \Zend\ServiceManager\ServiceManager
     */
    private function getServiceManager()
    {
        $appConfig = require __DIR__ . '/../../../../config/application.config.php';
        if (file_exists(__DIR__ . '/../../../../config/development.config.php')) {
            $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../../../../config/development.config.php');
        }
        // Run the application!
        $application = Application::init($appConfig);
        $serviceManager = $application->getServiceManager();

        return $serviceManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function getEntityManager()
    {
        $serviceManager = $this->getServiceManager();

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $serviceManager->get('doctrine.entitymanager.orm_renault');

        return $em;
    }
}
