<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 11:38
 */

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Renault\Entity\Wave;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="projects")
 *
 * @category Application
 * @package  Entity
 */
class Project implements EntityInterface
{
    use EntityTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Client")
     * @ORM\JoinColumn(name="client", referencedColumnName="id")
     * @var \Application\Entity\Client $client
     */
    private $client;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('lead_sales', 'lead_aftersales', 'feedback_sales', 'feedback_aftersales')")
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private $wave;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var integer
     */
    private $year;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    private $archive = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    private $competitor = false;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Question", mappedBy="project", orphanRemoval=true)
     * @var \Doctrine\Common\Collections\ArrayCollection $questions
     */
    private $questions;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Visit", mappedBy="project", orphanRemoval=true)
     * @var \Doctrine\Common\Collections\ArrayCollection $visits
     */
    private $visits;

    public function __construct()
    {
        $this->visits = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param string $wave
     */
    public function setWave($wave)
    {
        $this->wave = $wave;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isArchive()
    {
        return $this->archive;
    }

    /**
     * @param bool $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return bool
     */
    public function isCompetitor()
    {
        return $this->competitor;
    }

    /**
     * @param bool $competitor
     */
    public function setCompetitor($competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $visits
     */
    public function setVisits(ArrayCollection $visits)
    {
        $this->visits = $visits;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions)
    {
        $this->questions = $questions;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id           = isset($data['id']) ? $data['id'] : null;
        $this->client       = isset($data['client']) ? $data['client'] : null;
        $this->type         = isset($data['type']) ? $data['type'] : null;
        $this->wave         = isset($data['wave']) ? $data['wave'] : null;
        $this->label        = isset($data['label']) ? $data['label'] : null;
        $this->year         = isset($data['year']) ? $data['year'] : null;
        $this->description  = isset($data['description']) ? $data['description'] : null;
        $this->archive      = isset($data['archive']) ? $data['archive'] : null;
        $this->competitor   = isset($data['competitor']) ? $data['competitor'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'            => $this->id,
            'client'        => $this->client->getId(),
            'type'          => $this->type,
            'wave'          => $this->wave,
            'label'         => $this->label,
            'year'          => $this->year,
            'description'   => $this->description,
            'archive'       => $this->archive,
            'competitor'    => $this->competitor,
        ];
    }

    /** @ORM\PostPersist */
    public function doAdd()
    {
        if ($this->isCompetitor()) {
            return;
        }

        $em = $this->getEntityManager();

        $wave = new Wave();
        $wave->exchangeArray([
            'type'          => $this->getType(),
            'wave'          => $this->getWave(),
            'external_id'   => $this->getId(),
            'country'       => 1,
        ]);

        $em->persist($wave);
        $em->flush();
    }
}
