<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 13-11-17
 * Time: 11:29
 */

namespace Application\Entity;

interface EntityInterface
{
    /**
     * @param $data
     */
    public function exchangeArray($data);

    /**
     * @return mixed
     */
    public function getArrayCopy();
}
