<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 16-11-17
 * Time: 16:05
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="visits")
 *
 * @category Application
 * @package  Entity
 */
class VisitRenault extends Visit
{
    /**
     * @ORM\ManyToOne(targetEntity="Renault\Entity\Dealer")
     * @ORM\JoinColumn(name="location", referencedColumnName="dealer_id")
     * @var \Renault\Entity\Dealer $location
     */
    protected $location;
}
