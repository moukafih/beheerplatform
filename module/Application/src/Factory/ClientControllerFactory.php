<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Application\Factory;

use Application\Controller\ClientController;
use Application\Service\ClientService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ClientControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(ClientService::class);
        return new ClientController($dataService);
    }
}
