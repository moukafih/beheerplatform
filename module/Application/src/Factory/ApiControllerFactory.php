<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 11-8-17
 * Time: 11:24
 */

namespace Application\Factory;

use Application\Controller\ApiController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApiControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ApiController($container);
    }
}
