<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 11-8-17
 * Time: 9:12
 */

namespace Application\Controller;

use Interop\Container\ContainerInterface;

use Question\Service\QuestionService;
use Survey\Service\AnswerService;
use Visit\Service\VisitService;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ApiController extends AbstractActionController
{
    /** @var ContainerInterface $container */
    private $container;

    /**
     * ApiController constructor.
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function indexAction()
    {
        return [];
    }

    public function jsonrpcAction()
    {
        $user = $this->identity();
        if (!$user ||  \User\Acl\MyAcl::DEFAULT_ROLE == $user->getRole()) {
            return $this->getResponse()->setStatusCode(403);
        }

        $view = new ViewModel();
        $view->setTerminal(true);

        $server = new \Zend\Json\Server\Server();

        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            // Indicate the URL endpoint, and the JSON-RPC version used:
            $server->setTarget('/json-rpc.php')
                ->setEnvelope(\Zend\Json\Server\Smd::ENV_JSONRPC_2);

            // Grab the SMD
            $smd = $server->getServiceMap();

            // Return the SMD to the client
            header('Content-Type: application/json');
            echo $smd;
            return;
        }

        $visitService = $this->container->get(VisitService::class);
        $server->setClass($visitService, 'visitService');

        $answerService = $this->container->get(AnswerService::class);
        $server->setClass($answerService, 'answerService');

        $questionService = $this->container->get(QuestionService::class);
        $server->setClass($questionService, 'questionService');

        $server->handle();

        return $view;
    }
}
