<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:10
 */

namespace Application\Controller;

use Application\Service\ClientService;
use Zend\Mvc\Controller\AbstractActionController;

class ClientController extends AbstractActionController
{
    /**
     * @var ClientService $clientService
     */
    private $clientService;

    /**
     * ClientController constructor.
     * @param ClientService $clientService
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function indexAction()
    {
        return [
            'clients' => $this->clientService->getClients()
        ];
    }
}
