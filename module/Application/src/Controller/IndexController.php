<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:10
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        if ($user = $this->identity()) {
            return $this->redirect()->toRoute('user');
        }

        return $this->redirect()->toRoute('user/login');
    }
}
