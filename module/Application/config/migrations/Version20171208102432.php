<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208102432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visits (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, date DATETIME NOT NULL, remark LONGTEXT DEFAULT NULL, active TINYINT(1) NOT NULL, reason LONGTEXT DEFAULT NULL, advisor INT DEFAULT NULL, discr VARCHAR(255) NOT NULL, location INT DEFAULT NULL, INDEX IDX_444839EA2FB3D0EE (project), INDEX IDX_444839EA19ADC9F4 (advisor), INDEX IDX_444839EA5E9E89CB (location), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projects (id INT AUTO_INCREMENT NOT NULL, client INT DEFAULT NULL, type ENUM(\'lead_sales\', \'lead_aftersales\', \'feedback_sales\', \'feedback_aftersales\'), wave LONGTEXT NOT NULL, `label` LONGTEXT NOT NULL, year INT NOT NULL, description LONGTEXT DEFAULT NULL, competitor TINYINT(1) NOT NULL, INDEX IDX_5C93B3A4C7440455 (client), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE external_location (dealer_id INT AUTO_INCREMENT NOT NULL, brand VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, zipcode VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, fax VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, latitude VARCHAR(255) DEFAULT NULL, longitude VARCHAR(255) DEFAULT NULL, PRIMARY KEY(dealer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE visits ADD CONSTRAINT FK_444839EA2FB3D0EE FOREIGN KEY (project) REFERENCES projects (id)');
        $this->addSql('ALTER TABLE projects ADD CONSTRAINT FK_5C93B3A4C7440455 FOREIGN KEY (client) REFERENCES clients (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visits DROP FOREIGN KEY FK_444839EA2FB3D0EE');
        $this->addSql('ALTER TABLE projects DROP FOREIGN KEY FK_5C93B3A4C7440455');

        $this->addSql('DROP TABLE visits');
        $this->addSql('DROP TABLE projects');
        $this->addSql('DROP TABLE external_location');
        $this->addSql('DROP TABLE clients');
    }
}
