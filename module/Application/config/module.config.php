<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'client' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/client[/:action]',
                    'constraints' => [
                        'action'        => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\ClientController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'api' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api[/:action]',
                    'constraints' => [
                        'action'        => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => Controller\ApiController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class  => InvokableFactory::class,
            Controller\ClientController::class => Factory\ClientControllerFactory::class,
            Controller\ApiController::class    => Factory\ApiControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\ClientService::class => Factory\ClientServiceFactory::class,
        ]
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'import/success' => __DIR__ . '/../view/application/import/success.phtml',
            'partial/paginator' => __DIR__ . '/../view/partials/paginator.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'acl' => [
        'resources' => [
            'allow' => [
                Controller\IndexController::class  => [
                    'index' => 'guest',
                ],
                Controller\ClientController::class => [
                    'index' => 'admin',
                ],
                Controller\ApiController::class    => [
                    'index' => 'user',
                    'jsonrpc' => 'guest',
                ],
            ],
            'deny' => [
                Controller\ClientController::class => [
                    'index' => 'guest',
                ],
            ]
        ]
    ]
];
