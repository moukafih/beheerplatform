<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-12-17
 * Time: 11:45
 */

namespace ApplicationTest\Controller;

use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ApiControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }

    public function testCanSaveList()
    {
        $postData = [
            'jsonrpc' => '2.0',
            'method' => 'answerService.saveList',
            'params' => [
                'data' => [
                    'visit_id' => 2033,
                    'answers' => [
                        ["question" => 187, "answer" => "Ja", "remark" => null, "score" => 10],
                        ["question" => 188, "answer" => "Nee", "remark" => null, "score"=> 0]
                    ]
                ]
            ]
        ];
        $this->dispatch('/api/jsonrpc', 'POST', $postData);
    }
}
