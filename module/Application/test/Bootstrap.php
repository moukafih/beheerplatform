<?php
namespace ApplicationTest;

require_once 'TestCase.php';

use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

class Bootstrap
{
    protected static $serviceManager;
    protected static $config;
    protected static $bootstrap;

    public static function init()
    {
        // Load the user-defined test configuration file, if it exists; otherwise, load
        if (is_readable(__DIR__ . '/TestConfig.php')) {
            $testConfig = include __DIR__ . '/TestConfig.php';
        } else {
            $testConfig = include __DIR__ . '/TestConfig.php.dist';
        }

        $zf2ModulePaths = [];

        if (isset($testConfig['module_listener_options']['module_paths'])) {
            $modulePaths = $testConfig['module_listener_options']['module_paths'];
            foreach ($modulePaths as $modulePath) {
                if (($path = static::findParentPath($modulePath))) {
                    $zf2ModulePaths[] = $path;
                }
            }
        }

        $zf2ModulePaths  = implode(PATH_SEPARATOR, $zf2ModulePaths) . PATH_SEPARATOR;
        $zf2ModulePaths .= getenv('ZF2_MODULES_TEST_PATHS') ?: (defined('ZF2_MODULES_TEST_PATHS') ? ZF2_MODULES_TEST_PATHS : '');

        $vendorPath = static::findParentPath('vendor');
        $loader = include $vendorPath . '/autoload.php';

        if (! class_exists(Application::class)) {
            throw new \RuntimeException(
                "Unable to load application.\n"
                . "- Type `composer install` if you are developing locally.\n"
                . "- Type `vagrant ssh -c 'composer install'` if you are using Vagrant.\n"
                . "- Type `docker-compose run zf composer install` if you are using Docker.\n"
            );
        }

        // Retrieve configuration
        $appConfig = require __DIR__ . '/../../../config/application.config.php';
        if (file_exists(__DIR__ . '/../../../config/development.config.php')) {
            $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../../../config/development.config.php');
        }
        // Run the application!
        $application = Application::init($appConfig);
        $serviceManager = $application->getServiceManager();
        static::$serviceManager = $serviceManager;
        static::initDoctrine($serviceManager);
        static::$config = $appConfig;

        // use ModuleManager to load this module and it's dependencies
        /*$baseConfig = [
            'module_listener_options' => [
                'module_paths' => explode(PATH_SEPARATOR, $zf2ModulePaths),
            ],
        ];

        $config = ArrayUtils::merge($baseConfig, $testConfig);

        $serviceManager = new ServiceManager(new ServiceManagerConfig());
        $serviceManager->setService('ApplicationConfig', $config);
        $serviceManager->get('ModuleManager')->loadModules();

        static::$serviceManager = $serviceManager;
        static::initDoctrine($serviceManager);
        static::$config = $config;*/
    }

    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    public static function getConfig()
    {
        return static::$config;
    }

    public static function initDoctrine($serviceManager)
    {
        $serviceManager->setAllowOverride(true);
        $config = $serviceManager->get('Config');

        $config['doctrine']['connection']['orm_default'] = [
            'configuration' => 'orm_default',
            'eventmanager' => 'orm_default',
            'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
            'params' => [
                'host' => '172.16.6.3',
                'port' => '3306',
                'user' => 'ismail',
                'password' => 'meknes',
                'dbname' => 'dunyazorg',
            ]
        ];

        //$serviceManager->setService('Config', $config);
        //$serviceManager->get('doctrine.entity_resolver.default_orm');
        TestCase::setServiceManager($serviceManager);
    }

    protected static function findParentPath($path)
    {
        $dir = __DIR__;
        $previousDir = '.';
        while (!is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) {
                return false;
            }
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }
}

Bootstrap::init();
