<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-8-17
 * Time: 11:33
 */

namespace Survey\Service;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use DOMPDFModule\View\Model\PdfModel;
use Survey\Entity\Answer;
use Survey\Entity\AnswerPhoto;
use Survey\Entity\Report;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class AnswerService implements EventManagerAwareInterface
{
    protected $events;

    /** @var \Doctrine\ORM\EntityManager $entitymanager */
    private $entitymanager;

    private $viewPdfRenderer;

    /**
     * FeedService constructor.
     * @param \Doctrine\ORM\EntityManager $entitymanager
     * @param $viewPdfRenderer
     */
    public function __construct(EntityManager $entitymanager, $viewPdfRenderer)
    {
        $this->entitymanager = $entitymanager;
        $this->viewPdfRenderer = $viewPdfRenderer;
    }

    /**
     * @param int $id
     * @return null|\Survey\Entity\Answer
     */
    public function get($id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\Answer')->findOneBy(['id'=> $id]);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function save($data)
    {
        if (empty($data) || $data['id'] == null) {
            throw new \Exception('Missing answer ID.');
        }

        $entity = $this->entitymanager->getRepository('Survey\Entity\Answer')->findOneBy(['id' => $data['id']]);

        $hydrator = new DoctrineHydrator($this->entitymanager);
        $entity = $hydrator->hydrate($data->toArray(), $entity);

        $this->entitymanager->flush();

        return ['status' => 'success', 'message' => 'Saved'];
    }

    public function delete($data)
    {
        if (empty($data) || $data['id'] == null) {
            throw new \Exception('Missing answer ID.');
        }

        $answer = $this->entitymanager->find('Survey\Entity\Answer', $data['id']);
        if ($answer) {
            $this->entitymanager->remove($answer);
            $this->entitymanager->flush();
        }

        return ['status' => 'success', 'message' => 'Deleted'];
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function saveList($data)
    {

        $date = new \DateTime('now');
        error_log(print_r($data, true), 3, __DIR__ . '/../../../../data/uploads/reports/visit-' . $data['visit_id'] . '-[' . $date->format("d-m-Y H:i:s") . '].log');

        /** @var \Application\Entity\Visit $visit */
        $visit = $this->entitymanager->getRepository('Application\Entity\VisitRenault')->findOneBy(['id'=> $data['visit_id']]);

        if (! $visit) {
            $visit = $this->entitymanager->getRepository('Application\Entity\VisitCompetitor')->findOneBy(['id'=> $data['visit_id']]);
        }

        if (! $visit) {
            throw new \Exception('visit not found');
        }

        /** @var \Doctrine\Common\Collections\ArrayCollection $reports */
        $report = $visit->getReport();

        // TODO one report per visit
        if (! $report || true) {
            $report = new Report();
            $report->setVersion('v1');
            $report->setCreatedDate(new \DateTime());
            $report->setVisit($visit);
            $this->entitymanager->persist($report);

            $this->addAnswers($data, $report);
            $this->generatePdf($report);

            return ['status' => 'success', 'message' => 'Saved'];
        }

        return ['status' => 'success', 'message' => 'Report already exists'];

        //$params = ['report' => $report];
        //$this->getEventManager()->trigger('saveList', null, $params);
    }

    private function saveBase64Image($data, $prefix = '')
    {
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);

        $image = base64_decode($data); // try to decode image

        if ($image) {
            // store image
            $filename = uniqid($prefix);
            $filepath = "./data/uploads/";
            if ($type == 'data:image/png') {
                $ext .= '.png';
            } elseif ($type == 'data:image/jpeg') {
                $ext = '.jpeg';
            } elseif ($type == 'data:image/jpg') {
                $ext = '.jpeg';
            }
            return (file_put_contents($filepath . $filename . $ext, $image)) ? $filepath . $filename . $ext : false;
        } else {
            // store raw data for debug or repair
            $filename = 'error_' . date() . '.txt';
            $filepath = "./data/uploads/";
            return (file_put_contents($filepath . $filename, $data)) ? $filepath . $filename : false;
        }
    }

    /**
     * @param $id
     * @return null|\Survey\Entity\AnswerPhoto
     */
    public function getPhoto($id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\AnswerPhoto')->findOneBy(['id'=> $id]);
    }

    /**
     * @param array $answerArr
     * @param \Survey\Entity\Answer $answer
     */
    private function savePhotos(array $answerArr, Answer $answer)
    {
        if (empty($answerArr['addon_photo_arr'])) {
            return;
        }

        foreach ($answerArr['addon_photo_arr'] as $key => $image_base64) {
            $filepath = $this->saveBase64Image($image_base64, "AXS_{$key}_");

            $answerPhoto = new AnswerPhoto();
            $answerPhoto->setAnswer($answer);
            $answerPhoto->setFilepath($filepath);

            $this->entitymanager->persist($answerPhoto);
        }
    }

    /**
     * @param array $data
     * @param \Survey\Entity\Report $report
     */
    private function addAnswers($data, Report $report)
    {
        $hydrator = new DoctrineHydrator($this->entitymanager);

        foreach ($data['answers'] as $answerArr) {
            if (is_null($answerArr['answer'])){
                $answerArr['answer'] = '';
            }

            $answer = new Answer();
            $answer = $hydrator->hydrate($answerArr, $answer);

            $answer->setReport($report);
            if (! empty($answerArr['score'])) {
                $answer->setScore($answerArr['score']);
            }

            $this->savePhotos($answerArr, $answer);

            $report->getAnswers()->add($answer);

            $this->entitymanager->flush();
        }
    }

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers([
            __CLASS__,
            get_called_class(),
        ]);
        $this->events = $events;
        return $this;
    }

    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    /**
     * @param \Survey\Entity\Report $report
     */
    private function generatePdf($report)
    {
        $pdf = new PdfModel();
        $pdf->setOption('filename', 'visit-report-' . $report->getId()); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); //"portrait | landscape"
        $pdf->setOption('isRemoteEnabled', true);
        $pdf->setTemplate('report/pdf');
        // To set view variables
        $pdf->setVariables([
            'report' => $report
        ]);

        $result = $this->viewPdfRenderer->render($pdf);

        $local_path = __DIR__ . '/../../../../data/uploads/reports/';
        $date = new \DateTime('now');
        $new_file_name = 'visit-' . $report->getVisit()->getId() . '_report-' . $report->getId() . '-[' . $date->format("d-m-Y H:i:s") . '].pdf';

        $fp = fopen($local_path . $new_file_name, 'w');
        fwrite($fp, $result);
        fclose($fp);
    }
}
