<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:09
 */

namespace Survey\Service;

use Survey\Entity\Comment;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class CommentService
{
    /** @var EntityManager $entitymanager */
    private $entitymanager;

    /**
     * CommentService constructor.
     * @param EntityManager $entitymanager
     */
    public function __construct(EntityManager $entitymanager)
    {
        $this->entitymanager = $entitymanager;
    }

    /**
     * @param $id
     * @return null|\Survey\Entity\Comment
     */
    public function get($id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\Comment')->findOneBy(['id' => $id]);
    }

    /**
     * @param $report_id
     * @return array
     */
    public function getList($report_id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\Comment')->findBy(['report' => $report_id], ['time' => 'DESC']);
    }

    /**
     * @param array $data
     */
    public function save($data)
    {
        $entity = new Comment();
        $hydrator = new DoctrineHydrator($this->entitymanager);
        $hydrator->hydrate($data, $entity);

        $this->entitymanager->persist($entity);
        $this->entitymanager->flush();
    }
}
