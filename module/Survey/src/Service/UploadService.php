<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:09
 */

namespace Survey\Service;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Doctrine\ORM\EntityManager;
use Survey\Entity\Upload;

class UploadService
{
    /**
     * @var EntityManager $entitymanager
     */
    private $entitymanager;

    /**
     * FeedService constructor.
     * @param EntityManager $entitymanager
     */
    public function __construct(EntityManager $entitymanager)
    {
        $this->entitymanager = $entitymanager;
    }

    /**
     * @param $id
     * @return null|\Survey\Entity\Upload
     */
    public function get($id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\Upload')->findOneBy(['id'=> $id]);
    }

    /**
     * @param $report_id
     * @return array
     */
    public function getList($report_id)
    {
        return $this->entitymanager->getRepository('Survey\Entity\Upload')->findBy(['report' => $report_id], ['time' => 'DESC']);
    }

    public function save($data)
    {
        $entity = new Upload();
        $hydrator = new DoctrineHydrator($this->entitymanager);
        $hydrator->hydrate($data, $entity);

        $filepath = $data['image']['tmp_name'];
        $entity->setFilepath($filepath);
        $entity->setTime(new \DateTime());

        $this->entitymanager->persist($entity);
        $this->entitymanager->flush();

        return [
            'status' => 'success'
        ];
    }
}
