<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 13-10-16
 * Time: 13:26
 */

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ReportFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $filename = __DIR__ .'/../../../../data/salesafter_beheersplatform_reports.sql';
        $sql = file_get_contents($filename);  // Read file contents
        $manager->getConnection()->exec($sql);  // Execute native SQL

        $manager->flush();
    }

    public function getOrder()
    {
        return 10; // number in which order to load fixtures
    }
}
