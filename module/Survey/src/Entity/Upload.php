<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 10-8-17
 * Time: 15:24
 */

namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="uploads")
 *
 * @category Survey
 * @package  Entity
 */
class Upload
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Report", inversedBy="uploads")
     * @ORM\JoinColumn(name="report", referencedColumnName="id")
     * @var \Survey\Entity\Report $report
     */
    private $report;

    /**
     * @ORM\ManyToOne(targetEntity="Renault\Entity\User")
     * @ORM\JoinColumn(name="author", referencedColumnName="user_id")
     * @var \Renault\Entity\User $author
     */
    private $author;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var datetime
     */
    private $time;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $filepath;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Survey\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param \Survey\Entity\Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return datetime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param datetime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * @param string $filepath
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}
