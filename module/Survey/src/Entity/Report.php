<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 13:08
 */

namespace Survey\Entity;

use Application\Entity\EntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Renault\Entity\EmailAlertCheck;
use Renault\Entity\FeedbackAfterSalesScore;
use Renault\Entity\FeedbackSalesScore;
use Renault\Entity\LeadAfterSalesScore;
use Renault\Entity\LeadSalesScore;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="reports")
 *
 * @category Application
 * @package  Entity
 */
class Report
{
    use EntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * One Report has One Visit.
     * @ORM\OneToOne(targetEntity="Application\Entity\Visit", inversedBy="report")
     * @ORM\JoinColumn(name="visit", referencedColumnName="id")
     * @var \Application\Entity\Visit $visit
     */
    private $visit;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Upload", mappedBy="report", orphanRemoval=true)
     * @var \Doctrine\Common\Collections\ArrayCollection $uploads
     */
    private $uploads;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Comment", mappedBy="report", orphanRemoval=true)
     * @var \Doctrine\Common\Collections\ArrayCollection $uploads
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Answer", mappedBy="report", orphanRemoval=true, cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection $answers
     */
    private $answers;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var datetime
     */
    private $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var datetime
     */
    private $updated_date;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('v1', 'v2', 'def')")
     * @var string
     */
    private $version;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YES', 'NO')")
     * @var string
     */
    private $check_n = 'NO';

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YES', 'NO')")
     * @var string
     */
    private $check_r = 'NO';

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YES', 'NO')")
     * @var string
     */
    private $publish = 'NO';

    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->uploads = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Application\Entity\Visit
     */
    public function getVisit()
    {
        return $this->visit;
    }

    /**
     * @param \Application\Entity\Visit $visit
     */
    public function setVisit($visit)
    {
        $this->visit = $visit;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $uploads
     */
    public function setUploads($uploads)
    {
        $this->uploads = $uploads;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $comments
     */
    public function setComments(ArrayCollection $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return datetime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param datetime $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return datetime
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param datetime $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getCheckN()
    {
        return $this->check_n;
    }

    /**
     * @param string $check_n
     */
    public function setCheckN($check_n)
    {
        $this->check_n = $check_n;
    }

    /**
     * @return string
     */
    public function getCheckR()
    {
        return $this->check_r;
    }

    /**
     * @param string $check_r
     */
    public function setCheckR($check_r)
    {
        $this->check_r = $check_r;
    }

    /**
     * @return string
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * @param string $publish
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    }

    public function getScore()
    {
        $answers = $this->getAnswers()->toArray();

        if ($this->visit->getProject()->getId() == 4) {
            $process = $this->getScorePerCategoryId(1);
            $Klantbeleving = $this->getScorePerCategoryId(2);

            return ($process * 0.8) + ($Klantbeleving * 0.2);
        }

        return $this->calculate_score($answers);
    }

    /*public function getScorePerCategory($category)
    {
        $answers = $this->getAnswers()->toArray();

        $answers = array_map(function ($element) use ($category) {
            /** @var \Survey\Entity\Answer $element *
            if ($element->getCategory()->getName() == $category || $element->getCategory()->getParentName() == $category) {
                return $element;
            }
        }, $answers);

        return $this->calculate_score($answers);
    }*/

    public function getScorePerCategoryId($category_id)
    {
        $answers = $this->getAnswers()->toArray();

        $answers = array_map(function ($element) use ($category_id) {
            /** @var \Survey\Entity\Answer $element */
            if ($element->getCategory()->getId() == $category_id || ($element->getCategory()->getParent() && $element->getCategory()->getParent()->getId() == $category_id)) {
                return $element;
            }
        }, $answers);

        $answers = array_filter($answers);

        $r = 0;
        $s = 0;
        /** @var \Survey\Entity\Answer $answer */
        foreach ($answers as $answer) {
            if ($answer->getScore() === null || $answer->getScore() === "") {
                $s++;
            }
        }

        $c = count($answers);
        
        if ($c > 0) {
            $r = $s / $c;
        }



        if ($r === 1) {
            return 'nvt';
        }

        return $this->calculate_score($answers);
    }

    private function calculate_score($answers)
    {
        //$scores = array_column($answers, 'answer'); // PHP ^7
        $sum = 0;
        $scores = array_map(function ($element) {
            /** @var \Survey\Entity\Answer $element */

            if ($element) {
                $points = array_map(function ($element2) {
                    return $element2->getPoints();
                }, $element->getQuestion()->getOptions()->toArray());
                return ['score' => $element->getScore(), 'max' => (! empty($points)) ? max($points) : 0];
            }
        }, $answers);

        $total = array_sum(array_map(
            function ($element) {
                return $element['score'];
            },
            $scores
        ));

        $maxTotal = array_sum(array_map(
            function ($element) {
                if ($element['score'] != '') {
                    return $element['max'];
                }
            },
            $scores
        ));

        $maxTotal = ($maxTotal > 0) ? $maxTotal : 1;

        $end_score = ($total / $maxTotal) * 100;

        return round($end_score, 1);
    }

    /** @ORM\PreRemove */
    public function doDelScore()
    {
        $location = $this->getVisit()->getLocation();
        if (! $location instanceof \Renault\Entity\Dealer) {
            return;
        }

        $project = $this->getVisit()->getProject();

        $dealer_code = $location->getDealerCode();
        $em = $this->getEntityManager();

        $score = null;
        switch ($project->getType()) {
            case 'lead_sales':
                $score = $em->getRepository('Renault\Entity\LeadSalesScore')->findOneBy(['report_id' => $this->getId()]);
                break;

            case 'lead_aftersales':
                $score = $em->getRepository('Renault\Entity\LeadAfterSalesScore')->findOneBy(['report_id' => $this->getId()]);
                break;

            case 'feedback_sales':
                $score = $em->getRepository('Renault\Entity\FeedbackSalesScore')->findOneBy(['report_id' => $this->getId()]);
                $emailalert_check = $em->getRepository('Renault\Entity\EmailAlertCheck')->findOneBy(['dealer_code' => $dealer_code, 'source' => 'mystery-feedback-sales', 'in_queue' => false]);
                if ($emailalert_check) {
                    $em->remove($emailalert_check);
                }
                break;

            case 'feedback_aftersales':
                $score = $em->getRepository('Renault\Entity\FeedbackAfterSalesScore')->findOneBy(['report_id' => $this->getId()]);
                $emailalert_check = $em->getRepository('Renault\Entity\EmailAlertCheck')->findOneBy(['dealer_code' => $dealer_code, 'source' => 'mystery-feedback-aftersales', 'in_queue' => false]);
                if ($emailalert_check) {
                    $em->remove($emailalert_check);
                }
                break;
        }

        if ($score) {
            $em->remove($score);
        }
        $em->flush();
    }

    /** @ORM\PostUpdate */
    public function doAddScore()
    {
        $project = $this->getVisit()->getProject();
        if ($project->isCompetitor()) {
            return;
        }

        $em = $this->getEntityManager();

        if ($this->getPublish() == 'NO') {
            $this->doDelScore();

            return;
        }

        //$wave_code = $project->getYear() . '_2';

        $wave = $em->getRepository('Renault\Entity\Wave')->findOneBy(['external_id' => $project->getId()]);

        if ($wave === null) {
            return;
        }

        $dealer_code = $this->getVisit()->getLocation()->getDealerCode();

        $data = [
            'wave'          => $wave,
            'dealer_code'   => $dealer_code,
            'visit_date'    => $this->getVisit()->getDate(),
            'report_id'     => $this->getId(),
            'total'           => $this->getScore(),
        ];

        //error_log('type: ' . $project->getType(), 3, __DIR__ . '/../../../../data/uploads/project.log');

        $score= null;
        switch ($project->getType()) {
            case 'lead_sales':

                $score = $em->getRepository('Renault\Entity\LeadSalesScore')->findOneBy(['report_id' => $this->getId()]);
                if (! $score) {
                    $score = new LeadSalesScore();
                    $em->persist($score);
                }

                $score->exchangeArray(array_merge($data, [
                    'proces'          => $this->getScorePerCategoryId(1),
                    'klantbeleving'   => $this->getScorePerCategoryId(2),

                ]));

                break;

            case 'lead_aftersales':

                $score = $em->getRepository('Renault\Entity\LeadAfterSalesScore')->findOneBy(['report_id' => $this->getId()]);
                if (! $score) {
                    $score = new LeadAfterSalesScore();
                    $em->persist($score);
                }

                $score->exchangeArray(array_merge($data, [
                    'proces'          => $this->getScorePerCategoryId(1),
                    'klantbeleving'   => $this->getScorePerCategoryId(2),
                ]));

                break;

            case 'feedback_sales':

                $score = $em->getRepository('Renault\Entity\FeedbackSalesScore')->findOneBy(['report_id' => $this->getId()]);
                if (! $score) {
                    $score = new FeedbackSalesScore();
                    $em->persist($score);
                }

                $score->exchangeArray(array_merge($data, [
                    'warm_welkom'           => $this->getScorePerCategoryId(12),
                    'help_mij_kiezen'       => $this->getScorePerCategoryId(13),
                    'een_transparante_koop' => $this->getScorePerCategoryId(14),
                    'beleving'              => null, //$this->getScorePerCategoryId(15),
                    'lead'                  => null,
                    'aankomst'              => null,//$this->getScorePerCategoryId(4),
                    'showroom'              => $this->getScorePerCategoryId(5),
                    'ontvangst'             => $this->getScorePerCategoryId(6),
                    'inventarisatie'        => $this->getScorePerCategoryId(7),
                    'presentatie'           => $this->getScorePerCategoryId(8),
                    'proefrit'              => null,
                    'offerte'               => $this->getScorePerCategoryId(9),
                    'thema'                 => $this->getScorePerCategoryId(32),
                ]));

                $emailalert_check = $em->getRepository('Renault\Entity\EmailAlertCheck')->findOneBy(['dealer_code' => $dealer_code, 'source' => 'mystery-feedback-sales']);
                if (! $emailalert_check) {
                    $emailalert_check = new EmailAlertCheck();
                    $emailalert_check->exchangeArray([
                        'dealer_code'   => $dealer_code,
                        'source'        => 'mystery-feedback-sales',
                        'url'           => 'mystery-feedback-sales/view/' . $dealer_code . '/13',
                        'in_queue'      => false,
                        'created'       => new \DateTime('now'),
                    ]);
                    $em->persist($emailalert_check);
                }

                break;

            case 'feedback_aftersales':

                $score = $em->getRepository('Renault\Entity\FeedbackAfterSalesScore')->findOneBy(['report_id' => $this->getId()]);
                if (! $score) {
                    $score = new FeedbackAfterSalesScore();
                    $em->persist($score);
                }

                $score->exchangeArray(array_merge($data, [
                    'lead'                              => null,
                    'warm_welkom'                       => $this->getScorePerCategoryId(12),
                    'beleving'                          => $this->getScorePerCategoryId(15),
                    'als_er_toch_iets_verkeerd_gaat'    => $this->getScorePerCategoryId(17),
                    'vertrouwde_service'                => $this->getScorePerCategoryId(20),
                    'oplevering'                        => $this->getScorePerCategoryId(16),
                ]));



                $emailalert_check = $em->getRepository('Renault\Entity\EmailAlertCheck')->findOneBy(['dealer_code' => $dealer_code, 'source' => 'mystery-feedback-aftersales']);
                if (! $emailalert_check) {
                    $emailalert_check = new EmailAlertCheck();
                    $emailalert_check->exchangeArray([
                        'dealer_code'   => $dealer_code,
                        'source'        => 'mystery-feedback-aftersales',
                        'url'           => 'mystery-feedback-aftersales/view/' . $dealer_code . '/2017_2',
                        'in_queue'      => false,
                        'created'       => new \DateTime('now'),
                    ]);
                    $em->persist($emailalert_check);
                }

                break;
        }

        $em->flush();
    }
}
