<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 13:16
 */

namespace Survey\Entity;

use Application\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="question_options")
 *
 * @category Survey
 * @package  Entity
 */
class QuestionOption implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Question")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     * @var \Survey\Entity\Question $question
     */
    private $question;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $option;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $points;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    private $photo_required = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    private $skipTo = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getSkipTo()
    {
        return $this->skipTo;
    }

    /**
     * @param int $skipTo
     */
    public function setSkipTo($skipTo)
    {
        $this->skipTo = $skipTo;
    }

    /**
     * @return bool
     */
    public function isPhotoRequired()
    {
        return $this->photo_required;
    }

    /**
     * @param bool $photo_required
     */
    public function setPhotoRequired($photo_required)
    {
        $this->photo_required = $photo_required;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id             = isset($data['id']) ? $data['id'] : null;
        $this->question       = isset($data['question']) ? $data['question'] : null;
        $this->option         = isset($data['option']) ? $data['option'] : null;
        $this->points         = isset($data['points']) ? $data['points'] : null;
        $this->photo_required = isset($data['photo_required']) ? $data['photo_required'] : null;
        $this->skipTo         = isset($data['skipTo']) ? $data['skipTo'] : null;
    }

    /**
     * @return mixed
     */
    public function getArrayCopy()
    {
        return [
            'id'             => $this->id,
            'question'       => $this->question,
            'option'         => $this->option,
            'points'         => $this->points,
            'photo_required' => $this->photo_required,
            'skipTo'         => $this->skipTo,
        ];
    }
}
