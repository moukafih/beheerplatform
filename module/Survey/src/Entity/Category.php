<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 13:15
 */

namespace Survey\Entity;

use Application\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="categories")
 *
 * @category Survey
 * @package  Entity
 */
class Category implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, columnDefinition="ENUM('lead_sales', 'lead_aftersales', 'feedback_sales', 'feedback_aftersales')")
     * @var string
     */
    private $type;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @var \Survey\Entity\Category $parent
     */
    private $parent;

    /**
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="Survey\Entity\Category", mappedBy="parent")
     * @var \Doctrine\Common\Collections\ArrayCollection $children
     */
    private $children;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Question", mappedBy="category", orphanRemoval=true)
     * @var \Doctrine\Common\Collections\ArrayCollection $questions
     */
    private $questions;

    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        switch ($this->type) {
            case 'lead_sales': return 'Mystery Lead Sales';
            case 'lead_aftersales': return 'Mystery Lead Aftersales';
            case 'feedback_sales': return 'Mystery Feedback Sales';
            case 'feedback_aftersales': return 'Mystery Feedback Aftersales';
            default: return '-';
        }
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \Survey\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param \Survey\Entity\Category $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    public function hasChilderen()
    {
        return (count($this->getChildren()) > 1) ? true : false;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        if (!$this->parent) {
            return '';
        }

        return $this->parent->getName();
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        if (! $this->parent) {
            return false;
        }

        return true;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id       = isset($data['id']) ? $data['id'] : null;
        $this->type     = isset($data['type']) ? $data['type'] : null;
        $this->parent   = isset($data['parent']) ? $data['parent'] : null;
        $this->name     = isset($data['name']) ? $data['name'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'        => $this->id,
            'type'      => $this->type,
            'parent'    => $this->parent,
            'name'      => $this->name,
        ];
    }
}
