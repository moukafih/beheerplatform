<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 13:41
 */

namespace Survey\Entity;

use Application\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="answers", uniqueConstraints={@ORM\UniqueConstraint(name="report_idx", columns={"report", "question"})})
 *
 * @category Survey
 * @package  Entity
 */
class Answer implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Question")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     * @var \Survey\Entity\Question $question
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Report")
     * @ORM\JoinColumn(name="report", referencedColumnName="id")
     * @var \Survey\Entity\Report $report
     */
    private $report;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private $answer;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $score;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $remark;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\AnswerPhoto", mappedBy="answer", orphanRemoval=true, cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection $photos
     */
    private $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param Question $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param string $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }

    public function getCategory()
    {
        return $this->getQuestion()->getCategory();
    }

    public function getScoreInPercentage()
    {
        $max = ($this->getMaxScore() > 0) ? $this->getMaxScore() : 1;
        if (is_numeric($this->score)) {
            return (($this->score / $max) * 100);
        }

        return 'nvt';
    }

    public function getMaxScore()
    {
        $options = $this->getQuestion()->getOptions()->toArray();

        $points = array_map(function ($element) {
            return $element->getPoints();
        }, $options);
        
        //$points = array_column($options, 'points'); // PHP ^7
        return (! empty($points)) ? max($points) : 0;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id     = isset($data['id']) ? $data['id'] : null;
        $this->question = isset($data['question']) ? $data['question'] : null;
        $this->report  = isset($data['report']) ? $data['report'] : null;
        $this->answer  = isset($data['answer']) ? $data['answer'] : null;
        $this->score  = isset($data['score']) ? $data['score'] : null;
        $this->remark  = isset($data['remark']) ? $data['remark'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'question' => $this->question->getId(),
            'report'  => $this->report->getId(),
            'answer'  => $this->answer,
            'score'  => $this->score,
            'remark'  => $this->remark,
        ];
    }

    /** @ORM\PreRemove */
    public function doRemove()
    {
        $date = new \DateTime('now');
        error_log('[' . $date->format("d-m-Y H:i:s") . ']' . PHP_EOL . print_r($this->getArrayCopy(), true), 3, __DIR__ . '/../../../../data/uploads/reports/deleted-answers.log');
    }
}
