<?php

namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="answer_photos")
 *
 * @category Survey
 * @package  Entity
 */
class AnswerPhoto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Answer", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="answer", referencedColumnName="id")
     * @var \Survey\Entity\Answer $answer
     */
    private $answer;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $filepath;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Survey\Entity\Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param \Survey\Entity\Answer $answer
     */
    public function setAnswer(Answer $answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * @param string $filepath
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    public function exchangeArray($data)
    {
        $this->id     = isset($data['id']) ? $data['id'] : null;
        $this->answer  = isset($data['answer']) ? $data['answer'] : null;
        $this->filepath  = isset($data['filepath']) ? $data['filepath'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'answer'  => $this->answer->getId(),
            'filepath'  => $this->filepath,
        ];
    }

    /** @ORM\PreRemove */
    public function doRemovePhoto()
    {
        error_log(print_r($this->getArrayCopy(), true), 3, __DIR__ . '/../../../../data/uploads/reports/deleted-photos.log');
        if (file_exists($this->filepath)) {
            unlink($this->filepath);
        }
    }
}
