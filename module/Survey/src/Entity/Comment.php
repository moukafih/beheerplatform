<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 10-8-17
 * Time: 15:01
 */

namespace Survey\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comments")
 *
 * @category Survey
 * @package  Entity
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Report", inversedBy="comments")
     * @ORM\JoinColumn(name="report", referencedColumnName="id")
     * @var \Survey\Entity\Report $report
     */
    private $report;

    /**
     * @ORM\ManyToOne(targetEntity="Renault\Entity\User")
     * @ORM\JoinColumn(name="author", referencedColumnName="user_id")
     * @var \Renault\Entity\User $author
     */
    private $author;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var datetime
     */
    private $time;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private $comment;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return \Renault\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param \Renault\Entity\User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return datetime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param datetime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}
