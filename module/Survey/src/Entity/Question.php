<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 13:15
 */

namespace Survey\Entity;

use Application\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="questions", uniqueConstraints={@ORM\UniqueConstraint(name="questions_idx", columns={"project", "qnumber"})})
 *
 * @category Survey
 * @package  Entity
 */
class Question implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $qnumber;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Project", inversedBy="questions")
     * @ORM\JoinColumn(name="project", referencedColumnName="id", onDelete="SET NULL")
     * @var \Application\Entity\Project $project
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\Category")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     * @var \Survey\Entity\Category $category
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Survey\Entity\QuestionType")
     * @ORM\JoinColumn(name="question_type", referencedColumnName="id")
     * @var \Survey\Entity\QuestionType $type
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    private $question;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var boolean
     */
    private $optional = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $instruction;

    /** @ORM\OneToMany(targetEntity="Survey\Entity\QuestionOption", mappedBy="question", cascade={"persist", "remove"}, orphanRemoval=true) */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="Survey\Entity\Answer", mappedBy="question", orphanRemoval=true, cascade={"persist", "remove"})
     * @var \Doctrine\Common\Collections\ArrayCollection $answers
     */
    private $answers;

    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->answers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQnumber()
    {
        return $this->qnumber;
    }

    /**
     * @param mixed $qnumber
     */
    public function setQnumber($qnumber)
    {
        $this->qnumber = $qnumber;
    }

    /**
     * @return \Application\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param \Application\Entity\Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return QuestionType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param QuestionType $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getInstruction()
    {
        return $this->instruction;
    }

    /**
     * @param string $instruction
     */
    public function setInstruction($instruction)
    {
        $this->instruction = $instruction;
    }

    /**
     * @return bool
     */
    public function isOptional()
    {
        return $this->optional;
    }

    /**
     * @param bool $optional
     */
    public function setOptional($optional)
    {
        $this->optional = $optional;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $options
     */
    public function addOptions(Collection $options)
    {
        /** @var /Survey/Entity/Option $option */
        foreach ($options as $option) {
            $option->setQuestion($this);
            $this->options->add($option);
        }
    }

    public function removeOptions(Collection $options)
    {
        foreach ($options as $option) {
            $option->setQuestion(null);
            $this->options->removeElement($option);
        }
    }

    public function exchangeArray($data)
    {
        $this->id     = isset($data['id']) ? $data['id'] : null;
        $this->qnumber = isset($data['qnumber']) ? $data['qnumber'] : null;
        $this->project = isset($data['project']) ? $data['project'] : null;
        $this->category = isset($data['category']) ? $data['category'] : null;
        $this->type = isset($data['type']) ? $data['type'] : null;
        $this->question = isset($data['question']) ? $data['question'] : null;
        $this->image = isset($data['image']) ? $data['image'] : null;
        $this->optional = isset($data['optional']) ? $data['optional'] : null;
        $this->instruction = isset($data['instruction']) ? $data['instruction'] : null;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return [
            'id'     => $this->id,
            'qnumber' => $this->qnumber,
            'project' => $this->project,
            'category' => $this->category,
            'type' => $this->type,
            'question' => $this->question,
            'image' => $this->image,
            'optional' => $this->optional,
            'instruction' => $this->instruction,
        ];
    }
}
