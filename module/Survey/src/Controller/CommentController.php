<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:06
 */

namespace Survey\Controller;

namespace Survey\Controller;

use Survey\Form\CommentForm;
use Survey\Service\CommentService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class CommentController
 * @package Survey\Controller
 */
class CommentController extends AbstractActionController
{
    /** @var CommentService $commentService */
    private $commentService;

    /** @var CommentForm $commentForm */
    private $commentForm;

    /**
     * CommentController constructor.
     * @param CommentService $commentService
     * @param \Survey\Form\CommentForm $commentForm
     */
    public function __construct(CommentService $commentService, CommentForm $commentForm)
    {
        $this->commentService = $commentService;
        $this->commentForm = $commentForm;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');

        if (!$report_id) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        //try {
        $comments = $this->commentService->getList($report_id);
        //} catch (\Exception $e) {
        //    echo $e->getMessage();
        //}

        $this->commentForm->prepare();

        $viewData = [
            'project_id' => $project_id,
            'report_id' => $report_id,
            'form' => $this->commentForm,
            'comments' => $comments,
            'params' => $this->params()->fromQuery(),
        ];

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $viewData;
        }

        $this->commentForm->setData($request->getPost());

        if (! $this->commentForm->isValid()) {
            return $viewData;
        }

        $data = $this->commentForm->getData();
        $data['report'] = $report_id;
        $data['author'] = $this->identity()->getId();
        $data['time'] = new \DateTime();

        $this->commentService->save($data);

        $comments = $this->commentService->getList($report_id);
        return [
            'project_id' => $project_id,
            'report_id' => $report_id,
            'form' => $this->commentForm,
            'comments' => $comments,
            'params' => $this->params()->fromQuery(),
            'status' => 'success'
        ];
    }

    public function deleteAction()
    {
        return [];
    }
}
