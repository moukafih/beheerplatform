<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:06
 */

namespace Survey\Controller;

use Survey\Form\UploadForm;
use Survey\Service\UploadService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class UploadController
 * @package Survey\Controller
 */
class UploadController extends AbstractActionController
{
    /** @var UploadService $uploadService */
    private $uploadService;

    /** @var UploadForm $uploadForm */
    private $uploadForm;

    /**
     * UploadController constructor.
     *
     * @param UploadService $uploadService
     * @param UploadForm $uploadForm
     */
    public function __construct(UploadService $uploadService, UploadForm $uploadForm)
    {
        $this->uploadService = $uploadService;
        $this->uploadForm = $uploadForm;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');

        if (!$report_id) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        $uploads = $this->uploadService->getList($report_id);

        return [
            'project_id' => $project_id,
            'report_id' => $report_id,
            'uploads' => $uploads,
            'params' => $this->params()->fromQuery(),
        ];
    }

    public function uploadAction()
    {
        $view = new ViewModel();
        $request = $this->getRequest();
        $view->setTerminal(true);

        if (!$request->isPost()) {
            return $view;
        }

        $post = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getFiles()->toArray()
        );

        $this->uploadForm->setData($post);
        if (!$this->uploadForm->isValid() && ! $request->isXmlHttpRequest()) {
            return $view;
        }

        if (!$this->uploadForm->isValid() &&  $request->isXmlHttpRequest()) {
            return new JsonModel([
                'status' => 'error',
                'message' => 'Form-data invalid'
            ]);
        }

        $data = $this->uploadForm->getData();

        try {
            $result = $this->uploadService->save($data);
        } catch (\Exception $e) {
            $result = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonModel($result);
        }
    }

    public function imageAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');
        $upload_id = (int) $this->getEvent()->getRouteMatch()->getParam('upload');

        if ($upload_id == 0) {
            return $this->redirect()->toRoute('upload/index', ['action' => 'index', 'project' => $project_id, 'report' => $report_id]);
        }

        $upload = $this->uploadService->get($upload_id);

        if ($upload == null) {
            return $this->redirect()->toRoute('upload/index', ['action' => 'index', 'project' => $project_id, 'report' => $report_id]);
        }

        $filepath = $upload->getFilepath();
        
        if (false !== imagecreatefromjpeg($filepath)) {
            $img = imagecreatefromjpeg($filepath);
            header('Content-Type: image/jpeg');
            imagejpeg($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefromgif($filepath)) {
            $img = imagecreatefromgif($filepath);
            header('Content-Type: image/gif');
            imagegif($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefrompng($filepath)) {
            $img = imagecreatefrompng($filepath);
            header('Content-Type: image/png');
            imagepng($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefrombmp($filepath)) {
            $img = imagecreatefrombmp($filepath);
            header('Content-Type: image/bmp');
            imagebmp($img);
            imagedestroy($img);
        }
    }

    public function deleteAction()
    {
        return [];
    }
}
