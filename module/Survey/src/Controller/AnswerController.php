<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-8-17
 * Time: 16:43
 */

namespace Survey\Controller;

use Survey\Service\AnswerService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class AnswerController
 * @package Survey\Controller
 */
class AnswerController extends AbstractActionController
{
    /**
     * @var AnswerService $answerService
     */
    private $answerService;

    /**
     * AnswerController constructor.
     * @param AnswerService $answerService
     */
    public function __construct(AnswerService $answerService)
    {
        $this->answerService = $answerService;
    }

    public function xhrSaveAction()
    {
        $view = new ViewModel();
        $request = $this->getRequest();
        $view->setTerminal(true);

        if (!$request->isPost()) {
            return $view;
        }

        try {
            $result = $this->answerService->save($request->getPost());
        } catch (\Exception $e) {
            $result = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonModel($result);
        }
    }

    public function xhrDeleteAction()
    {
        $view = new ViewModel();
        $request = $this->getRequest();
        $view->setTerminal(true);

        if (!$request->isPost()) {
            return $view;
        }

        try {
            $result = $this->answerService->delete($request->getPost());
        } catch (\Exception $e) {
            $result = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonModel($result);
        }
    }

    public function imageAction()
    {
        $photo_id = (int) $this->getEvent()->getRouteMatch()->getParam('photo');

        $photo = $this->answerService->getPhoto($photo_id);

        if ($photo == null) {
        }

        $filepath = $photo->getFilepath();

        if (false !== imagecreatefromjpeg($filepath)) {
            $img = imagecreatefromjpeg($filepath);
            header('Content-Type: image/jpeg');
            imagejpeg($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefromgif($filepath)) {
            $img = imagecreatefromgif($filepath);
            header('Content-Type: image/gif');
            imagegif($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefrompng($filepath)) {
            $img = imagecreatefrompng($filepath);
            header('Content-Type: image/png');
            imagepng($img);
            imagedestroy($img);
        } elseif (false !== imagecreatefrombmp($filepath)) {
            $img = imagecreatefrombmp($filepath);
            header('Content-Type: image/bmp');
            imagebmp($img);
            imagedestroy($img);
        }
    }
}
