<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:12
 */

namespace Survey\Factory;

use Survey\Service\UploadService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UploadServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');

        return new UploadService($entitymanager);
    }
}
