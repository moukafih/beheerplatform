<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Survey\Factory;

use Survey\Controller\AnswerController;
use Survey\Form\AnswerForm;
use Survey\Service\AnswerService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AnswerControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(AnswerService::class);
        //$entitymanager = $container->get('doctrine.entitymanager.orm_default');
        //$reportForm = new AnswerForm($entitymanager);

        return new AnswerController($dataService);
    }
}
