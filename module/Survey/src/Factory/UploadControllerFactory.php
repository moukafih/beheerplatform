<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:12
 */

namespace Survey\Factory;

use Survey\Controller\UploadController;
use Survey\Form\UploadForm;
use Survey\Service\UploadService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UploadControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $uploadService = $container->get(UploadService::class);
        $uploadForm = new UploadForm();

        return new UploadController($uploadService, $uploadForm);
    }
}
