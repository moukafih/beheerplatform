<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:55
 */

namespace Survey\Factory;

use Survey\Service\AnswerService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AnswerServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $viewPdfRenderer = $container->get('ViewPdfRenderer');

        return new AnswerService($entitymanager, $viewPdfRenderer);
    }
}
