<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:12
 */

namespace Survey\Factory;

use Survey\Controller\CommentController;
use Survey\Form\CommentForm;
use Survey\Service\CommentService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $commentService = $container->get(CommentService::class);
        $reportForm = new CommentForm();

        return new CommentController($commentService, $reportForm);
    }
}
