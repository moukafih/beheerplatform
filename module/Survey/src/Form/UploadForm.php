<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 15-8-17
 * Time: 9:25
 */

namespace Survey\Form;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class UploadForm extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type' => Element\Hidden::class,
            'name' => 'report',
        ]);

        $this->add([
            'type' => Element\Hidden::class,
            'name' => 'author',
        ]);

        // File Input
        $file = new Element\File('image');
        $file->setLabel('Image Upload');
        $file->setAttribute('id', 'image');

        $this->add($file);

        $this->add([
            'name' => 'comment',
            'type' => 'textarea',
            'options' => [
                'label' => 'comment',
            ],
            'attributes' => [
                'placeholder' => 'Comment',
            ]
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        $fileInput = new InputFilter\FileInput('image');
        $fileInput->setRequired(true);

        $fileInput->getValidatorChain()
            //->attachByName('filesize', ['max' => 2048000])
            //->attachByName('filemimetype',  ['mimeType' => 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'])
            ->attachByName('Zend\Validator\File\Extension', ['extension' => ['jpeg', 'jpg','png','gif','bmp']]);

        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            [
                'target'    => './data/uploads/image',
                'overwrite' => false,
                'randomize' => true,
                'use_upload_name' => false,
                'use_upload_extension' => true,
            ]
        );
        $inputFilter->add($fileInput);

        /*$inputFilter->add([
            'name'     => 'remark',
            'required' => true,
            'filters'  => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Zend\Validator\NotEmpty',
                    'options' => [],
                ],
                [
                    'name' => 'Zend\Validator\StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 256,
                    ],
                ],
            ]
        ]);*/

        $this->setInputFilter($inputFilter);
    }
}
