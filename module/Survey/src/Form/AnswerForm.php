<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-8-17
 * Time: 16:39
 */

namespace Survey\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class AnswerForm extends Form
{
    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    private function addElements()
    {
    }
}
