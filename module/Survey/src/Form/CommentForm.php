<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 8-8-17
 * Time: 15:14
 */

namespace Survey\Form;

use Zend\InputFilter;
use Zend\Form\Form;

class CommentForm extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'title',
            'options' => [
                'label' => 'Title',
            ],
            'attributes' => [
                'placeholder' => 'Title',
            ]
        ]);

        $this->add([
            'name' => 'comment',
            'type' => 'textarea',
            'options' => [
                'label' => 'comment',
            ],
            'attributes' => [
                'placeholder' => 'Comment',
            ]
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Post',
            ],
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        $inputFilter->add([
            'name'     => 'title',
            'required' => true,
            'filters'  => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Zend\Validator\NotEmpty',
                    'options' => [],
                ],
                [
                    'name' => 'Zend\Validator\StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 256,
                    ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name'     => 'comment',
            'required' => true,
            'filters'  => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Zend\Validator\NotEmpty',
                    'options' => [],
                ],
                [
                    'name' => 'Zend\Validator\StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 256,
                    ],
                ],
            ]
        ]);

        $this->setInputFilter($inputFilter);
    }
}
