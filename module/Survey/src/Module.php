<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Survey;

use Survey\Event\PdfListener;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function init(ModuleManager $moduleManager)
    {
        //echo "User init <br/>";
    }

    public function onBootstrap(MvcEvent $event)
    {
        //echo "User bootstrap <br/>";
        $application = $event->getApplication();

        /*$serviceManager = $application->getServiceManager();
        $eventManager = $application->getEventManager();
        $sharedManager = $eventManager->getSharedManager();

        $accessListener = new PdfListener();
        $accessListener->attachShared($sharedManager);*/
    }
}
