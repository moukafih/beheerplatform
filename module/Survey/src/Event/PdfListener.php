<?php

/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 11-9-17
 * Time: 14:21
 */
namespace Survey\Event;

use Survey\Service\AnswerService;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\MvcEvent;

class PdfListener
{
    use ListenerAggregateTrait;

    public function attachShared(SharedEventManagerInterface $manager)
    {
        $this->listeners[] = $manager->attach(AnswerService::class, 'saveList', [$this, 'generatePdf'], 100);
    }

    public function generatePdf(\Zend\EventManager\Event $event)
    {
        $name = $event->getName();
        $report = $event->getParam('report');
        $date = new \DateTime('now');

        error_log("[" . $date->format("d-m-Y H:i:s") . "] Event:" . $name . " ; " . $report->getId() . " is klaar\n", 3, "./my-login.log");
    }
}
