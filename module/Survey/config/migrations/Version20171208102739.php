<?php

namespace Survey\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208102739 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE question_types (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, type ENUM(\'lead_sales\', \'lead_aftersales\', \'feedback_sales\', \'feedback_aftersales\'), name VARCHAR(255) NOT NULL, INDEX IDX_3AF34668727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question_options (id INT AUTO_INCREMENT NOT NULL, question INT DEFAULT NULL, `option` VARCHAR(255) NOT NULL, points VARCHAR(255) DEFAULT NULL, photo_required TINYINT(1) NOT NULL, skipTo INT DEFAULT NULL, INDEX IDX_DEE92F9AB6F7494E (question), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comments (id INT AUTO_INCREMENT NOT NULL, report INT DEFAULT NULL, time DATETIME NOT NULL, title VARCHAR(255) NOT NULL, comment LONGTEXT NOT NULL, author INT DEFAULT NULL, INDEX IDX_5F9E962AC42F7784 (report), INDEX IDX_5F9E962ABDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, category INT DEFAULT NULL, question_type INT DEFAULT NULL, qnumber INT NOT NULL, question LONGTEXT NOT NULL, image LONGTEXT DEFAULT NULL, optional TINYINT(1) NOT NULL, instruction LONGTEXT DEFAULT NULL, INDEX IDX_8ADC54D52FB3D0EE (project), INDEX IDX_8ADC54D564C19C1 (category), INDEX IDX_8ADC54D5D230BAC6 (question_type), UNIQUE INDEX questions_idx (project, qnumber), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reports (id INT AUTO_INCREMENT NOT NULL, visit INT DEFAULT NULL, created_date DATETIME NOT NULL, updated_date DATETIME DEFAULT NULL, version ENUM(\'v1\', \'v2\', \'def\'), check_n ENUM(\'YES\', \'NO\'), check_r ENUM(\'YES\', \'NO\'), publish ENUM(\'YES\', \'NO\'), INDEX IDX_F11FA745437EE939 (visit), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answers (id INT AUTO_INCREMENT NOT NULL, question INT DEFAULT NULL, report INT DEFAULT NULL, answer LONGTEXT NOT NULL, score VARCHAR(255) DEFAULT NULL, remark LONGTEXT DEFAULT NULL, INDEX IDX_50D0C606B6F7494E (question), INDEX IDX_50D0C606C42F7784 (report), UNIQUE INDEX report_idx (report, question), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uploads (id INT AUTO_INCREMENT NOT NULL, report INT DEFAULT NULL, time DATETIME NOT NULL, filepath VARCHAR(255) NOT NULL, comment LONGTEXT DEFAULT NULL, author INT DEFAULT NULL, INDEX IDX_96117F18C42F7784 (report), INDEX IDX_96117F18BDAFD8C8 (author), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answer_photos (id INT AUTO_INCREMENT NOT NULL, answer INT DEFAULT NULL, filepath VARCHAR(255) NOT NULL, INDEX IDX_FA8FDA7EDADD4A25 (answer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF34668727ACA70 FOREIGN KEY (parent_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE question_options ADD CONSTRAINT FK_DEE92F9AB6F7494E FOREIGN KEY (question) REFERENCES questions (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962AC42F7784 FOREIGN KEY (report) REFERENCES reports (id)');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D52FB3D0EE FOREIGN KEY (project) REFERENCES projects (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D564C19C1 FOREIGN KEY (category) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE questions ADD CONSTRAINT FK_8ADC54D5D230BAC6 FOREIGN KEY (question_type) REFERENCES question_types (id)');
        $this->addSql('ALTER TABLE reports ADD CONSTRAINT FK_F11FA745437EE939 FOREIGN KEY (visit) REFERENCES visits (id)');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C606B6F7494E FOREIGN KEY (question) REFERENCES questions (id)');
        $this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C606C42F7784 FOREIGN KEY (report) REFERENCES reports (id)');
        $this->addSql('ALTER TABLE uploads ADD CONSTRAINT FK_96117F18C42F7784 FOREIGN KEY (report) REFERENCES reports (id)');
        $this->addSql('ALTER TABLE answer_photos ADD CONSTRAINT FK_FA8FDA7EDADD4A25 FOREIGN KEY (answer) REFERENCES answers (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reports DROP FOREIGN KEY FK_F11FA745437EE939');

        $this->addSql('ALTER TABLE questions DROP FOREIGN KEY FK_8ADC54D52FB3D0EE');

        $this->addSql('ALTER TABLE questions DROP FOREIGN KEY FK_8ADC54D5D230BAC6');
        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF34668727ACA70');
        $this->addSql('ALTER TABLE questions DROP FOREIGN KEY FK_8ADC54D564C19C1');
        $this->addSql('ALTER TABLE question_options DROP FOREIGN KEY FK_DEE92F9AB6F7494E');
        $this->addSql('ALTER TABLE answers DROP FOREIGN KEY FK_50D0C606B6F7494E');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962AC42F7784');
        $this->addSql('ALTER TABLE answers DROP FOREIGN KEY FK_50D0C606C42F7784');
        $this->addSql('ALTER TABLE uploads DROP FOREIGN KEY FK_96117F18C42F7784');
        $this->addSql('ALTER TABLE answer_photos DROP FOREIGN KEY FK_FA8FDA7EDADD4A25');

        $this->addSql('DROP TABLE question_types');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE question_options');
        $this->addSql('DROP TABLE comments');
        $this->addSql('DROP TABLE questions');
        $this->addSql('DROP TABLE reports');
        $this->addSql('DROP TABLE answers');
        $this->addSql('DROP TABLE uploads');
        $this->addSql('DROP TABLE answer_photos');
    }
}
