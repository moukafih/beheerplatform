<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-8-17
 * Time: 12:58
 */

namespace Survey;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'report' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/report',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index[/:report]',
                            'constraints' => [
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ExcelController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'pdf' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/pdf[/:report]',
                            'constraints' => [
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\PdfController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:project[/:report]]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ExcelController::class,
                                'action'     => 'delete',
                            ],
                        ],
                    ],
                    'export' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/export/:project',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ExcelController::class,
                                'action'     => 'export',
                            ],
                        ],
                    ],
                    'publish' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/publish',
                            'defaults' => [
                                'controller' => Controller\ExcelController::class,
                                'action'     => 'xhrPublish',
                            ],
                        ],
                    ],
                ],
            ],
            'comment' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/comment',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index[/:project/:report]',
                            'constraints' => [
                                'project' => '[0-9]+',
                                'report' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\CommentController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:report[/:comment]]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\CommentController::class,
                                'action'     => 'delete',
                            ],
                        ],
                    ]
                ],
            ],
            'upload' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/upload',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index[/:project/:report]',
                            'constraints' => [
                                'project' => '[0-9]+',
                                'report' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\UploadController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'upload' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/upload',
                            'defaults' => [
                                'controller' => Controller\UploadController::class,
                                'action'     => 'upload',
                            ],
                        ],
                    ],
                    'image' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/image[/:project/:report/:upload]',
                            'constraints' => [
                                'project'         => '[0-9]+',
                                'report'          => '[0-9]+',
                                'upload'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\UploadController::class,
                                'action'     => 'image',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:report[/:comment]]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\UploadController::class,
                                'action'     => 'delete',
                            ],
                        ],
                    ]
                ],
            ],
            'answer' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/answer',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'xhrSave' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/save',
                            'defaults' => [
                                'controller' => Controller\AnswerController::class,
                                'action'     => 'xhrSave',
                            ],
                        ],
                    ],
                    'xhrDelete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete',
                            'defaults' => [
                                'controller' => Controller\AnswerController::class,
                                'action'     => 'xhrDelete',
                            ],
                        ],
                    ],
                    'image' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/image[/:photo]',
                            'constraints' => [
                                'photo' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\AnswerController::class,
                                'action'     => 'image',
                            ],
                        ],
                    ],
                ]
            ],
        ]
    ],

    'controllers' => [
        'factories' => [
            Controller\AnswerController::class => Factory\AnswerControllerFactory::class,
            Controller\CommentController::class => Factory\CommentControllerFactory::class,
            Controller\UploadController::class => Factory\UploadControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\AnswerService::class => Factory\AnswerServiceFactory::class,
            Service\CommentService::class => Factory\CommentServiceFactory::class,
            Service\UploadService::class => Factory\UploadServiceFactory::class,
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    'acl' => [
        'resources' => [
            'allow' => [
                Controller\CommentController::class => [
                    'index' => ['admin'],
                    'save' => ['admin'],
                    'delete' => ['admin'],
                ],
                Controller\UploadController::class => [
                    'index' => ['admin','country'],
                    'upload' => ['user'],
                    'delete' => ['admin'],
                    'image' => ['admin','country'],
                ],
                Controller\AnswerController::class => [
                    'image' => ['admin','country'],
                    'xhrSave' => ['admin','country'],
                    'xhrDelete' => ['admin','country'],
                ],
            ]
        ]
    ]
];
