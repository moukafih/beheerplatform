<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 22-11-17
 * Time: 11:03
 */

namespace SurveyTest\Service;

use Application\Entity\Project;
use Application\Entity\VisitRenault;
use ApplicationTest\TestCase;
use Project\Service\ProjectService;
use Survey\Service\AnswerService;
use Visit\Service\VisitService;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class AnswerServiceTest extends TestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /** @var  \Survey\Service\AnswerService */
    private $service;

    /** @var \Application\Entity\VisitRenault */
    private $visit;

    public function setUp()
    {
        $this->entityManager = $this->getEntityManager();
        $this->projectService = new ProjectService($this->entityManager);
        $this->visitService = new VisitService($this->entityManager);

        $client = $this->entityManager->getRepository('Application\Entity\Client')->find(1);
        $location_renault = $this->entityManager->getRepository('Renault\Entity\Dealer')->find(187);
        $advisor = $this->entityManager->getRepository('Renault\Entity\User')->findOneBy(['email' => 'beheer@nbeyond.nl']);

        $project = new Project();

        $project->exchangeArray([
            'client' => $client,
            'type' => 'lead_sales',
            'wave' => 'Meetronde 4',
            'label' => uniqid(),
            'year' => '2017',
            'description' => 'test',
        ]);

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $visit = new VisitRenault();
        $visit->exchangeArray([
            'project' => $project,
            'location'  => $location_renault,
            'advisor'  => $advisor,
            'date'  => new \DateTime('2017-12-14'),
            'remark'  => 'test',
        ]);

        $this->entityManager->persist($visit);
        $this->entityManager->flush();

        $this->visit = $visit;

        $this->service = $this->getServiceManager()->get(AnswerService::class);

        parent::setUp();
    }

    public function testCanSaveList()
    {
        $data = [
            "visit_id" => $this->visit->getId(),
            "answers" => [
                ["question" => 129, "answer" => "Nee, geen wegaanduiding", "remark" => null,"score"=> 0],
                ["question" => 130, "answer" => "Nee, parkeerplaats niet aangegeven", "remark" => null, "score" => 0],
                ["question" => 131, "answer" => "Ja", "remark" => null, "score" => 10],
            ]
        ];

        $response = $this->service->saveList($data);

        $this->assertArrayHasKey('status', $response);
        $this->assertEquals(['status' => 'success', 'message' => 'Saved'], $response);
    }
}
