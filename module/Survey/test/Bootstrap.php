<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 22-11-17
 * Time: 16:26
 */

namespace SurveyTest;

use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Application;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\ArrayUtils;
use RuntimeException;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);
class Bootstrap
{
    protected static $serviceManager;
    protected static $config;
    protected static $bootstrap;
    public static function init()
    {
        // Load the user-defined test configuration file, if it exists; otherwise, load
        if (is_readable(__DIR__ . '/TestConfig.php')) {
            $testConfig = include __DIR__ . '/TestConfig.php';
        } else {
            $testConfig = include __DIR__ . '/TestConfig.php.dist';
        }

        if (is_readable(__DIR__ . '/../../../vendor/autoload.php')) {
            $loader = include __DIR__ . '/../../../vendor/autoload.php';
        }

        $appConfig = require __DIR__ . '/../../../config/application.config.php';
        if (file_exists(__DIR__ . '/../../../config/development.config.php')) {
            $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../../../config/development.config.php');
        }

        $appConfig = ArrayUtils::merge($appConfig, $testConfig);
        $application = Application::init($appConfig);

        static::$serviceManager =  $application->getServiceManager();
        static::$config = $appConfig;
    }
    public static function getServiceManager()
    {
        return static::$serviceManager;
    }
    public static function getConfig()
    {
        return static::$config;
    }
}
Bootstrap::init();
