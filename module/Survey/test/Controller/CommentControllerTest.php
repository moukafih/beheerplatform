<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 22-11-17
 * Time: 11:09
 */

namespace SurveyTest\Controller;

use Survey\Controller\CommentController;
use Survey\Entity\Comment;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class CommentControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [
            'acl' => [
                'resources' => [
                    'allow' => [
                        'SurveyTest\Controller\CommentControllerTest' => [
                            'index' => ['admin'],
                            'save' => ['admin'],
                            'delete' => ['admin'],
                            'testIndexActionCanBeAccessed' => ['admin'],
                        ],
                    ]
                ]
            ]
        ];

        $this->setApplicationConfig(ArrayUtils::merge(
        // Grabbing the full application configuration:
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));
        parent::setUp();

        $services = $this->getApplicationServiceLocator();
        $config = $services->get('config');
        unset($config['db']);
        $services->setAllowOverride(true);
        $services->setService('config', $config);
        $services->setAllowOverride(false);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/comment/index/3/746');
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('Survey');
        $this->assertControllerName(CommentController::class);
        $this->assertControllerClass('CommentController');
        $this->assertMatchedRouteName('comment/index');
    }
}
