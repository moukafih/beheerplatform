<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 15:32
 */

namespace Competitor;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'competitor' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/competitor',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index',
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'save' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/save[/:competitor]',
                            'constraints' => [
                                'competitor'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\SaveController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:competitor]',
                            'constraints' => [
                                'competitor'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\DeleteController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ],
            ]
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class  => Factory\IndexControllerFactory::class,
            Controller\SaveController::class   => Factory\SaveControllerFactory::class,
            Controller\DeleteController::class => Factory\DeleteControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\CompetitorService::class => Factory\CompetitorServiceFactory::class,
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'acl' => [
        'resources' => [
            'allow' => [
                Controller\IndexController::class  => [
                    'index' => 'user',
                ],
                Controller\SaveController::class   => [
                    'index' => 'admin',
                ],
                Controller\DeleteController::class => [
                    'index' => 'admin',
                ],
            ],
            'deny' => [

            ]
        ]
    ]
];
