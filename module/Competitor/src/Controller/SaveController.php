<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 10:15
 */

namespace Competitor\Controller;

use DoctrineModule\Validator\UniqueObject;
use Competitor\Form\CompetitorForm;
use Competitor\Service\CompetitorService;
use Application\Entity\CompetitorDealer;
use Zend\Mvc\Controller\AbstractActionController;

class SaveController extends AbstractActionController
{
    /** @var \Competitor\Service\CompetitorService $competitorService */
    private $competitorService;

    /** @var \Competitor\Form\CompetitorForm $competitorForm */
    private $competitorForm;

    /**
     * CompetitorController constructor.
     * @param \Competitor\Service\CompetitorService $competitorService
     * @param \Competitor\Form\CompetitorForm $competitorForm
     */
    public function __construct(CompetitorService $competitorService, CompetitorForm $competitorForm)
    {
        $this->competitorService = $competitorService;
        $this->competitorForm = $competitorForm;
    }

    public function indexAction()
    {
        $competitor_id = (int) $this->getEvent()->getRouteMatch()->getParam('competitor');

        if (0 === $competitor_id) {
            $this->competitorForm->remove('id');
        }

        $viewData = [
            'form' => $this->competitorForm,
        ];

        /** @var \Application\Entity\CompetitorDealer $competitor */
        $competitor = new CompetitorDealer();

        if ($competitor_id > 0) {
            $competitor = $this->competitorService->get($competitor_id);
            $viewData['competitor_id'] = $competitor_id;
        }

        if ($competitor_id > 0 && $competitor == null) {
            return $this->redirect()->toRoute('competitor/index', ['action' => 'index']);
        }

        $this->competitorForm->bind($competitor);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->competitorForm->setData($postData);

        if (! $this->competitorForm->isValid()) {
            return $viewData;
        }

        $entity = $this->competitorForm->getData();

        try {
            $this->competitorService->save($entity);
        } catch (\Exception $exception) {
            $viewData['message'] = $exception->getMessage();
            return $viewData;
        }

        return $this->redirect()->toRoute('competitor/index', ['action' => 'index']);
    }
}
