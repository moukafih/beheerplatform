<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Competitor\Controller;

use Competitor\Service\CompetitorService;
use Zend\Mvc\Controller\AbstractActionController;

class DeleteController extends AbstractActionController
{
    /**
     * @var \Competitor\Service\CompetitorService $competitorService
     */
    private $competitorService;

    /**
     * DeleteController constructor.
     * @param \Competitor\Service\CompetitorService $competitorService
     */
    public function __construct(CompetitorService $competitorService)
    {
        $this->competitorService = $competitorService;
    }

    public function indexAction()
    {
        $competitor_id = (int) $this->getEvent()->getRouteMatch()->getParam('competitor');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$competitor_id) {
            return $this->redirect()->toRoute('competitor/index', ['action' => 'index']);
        }

        $competitor = $this->competitorService->get($competitor_id);

        if ($competitor == null) {
            return $this->redirect()->toRoute('competitor/index', ['action' => 'index']);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'competitor_id'    => $competitor_id,
                'competitor' => $competitor,
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->competitorService->delete($id);
        }

        return $this->redirect()->toRoute('competitor/index', ['action' => 'index']);
    }
}
