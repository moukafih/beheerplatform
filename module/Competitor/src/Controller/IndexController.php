<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:31
 */

namespace Competitor\Controller;

use Competitor\Service\CompetitorService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /** @var \Competitor\Service\CompetitorService $competitorService */
    private $competitorService;

    /**
     * CompetitorController constructor.
     * @param \Competitor\Service\CompetitorService $competitorService
     */
    public function __construct(CompetitorService $competitorService)
    {
        $this->competitorService = $competitorService;
    }

    public function indexAction()
    {
        return [
            'competitors' => $this->competitorService->getList()
        ];
    }
}
