<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 25-1-18
 * Time: 11:06
 */

namespace Competitor\Service;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Application\Entity\CompetitorDealer;

class CompetitorService
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * FeedService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(CompetitorDealer::class)->findOneBy(['id'=> $id]);
    }

    /**
     * @param $project_id
     * @return array
     */
    public function getList()
    {
        return $this->entityManager->getRepository(CompetitorDealer::class)->findAll();
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function save($entity)
    {
        if (! $entity instanceof CompetitorDealer) {
            throw new \Exception('CompetitorDealer object is required');
        }

        if ($entity->getId() == null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function delete($id)
    {
        $question = $this->entityManager->find(CompetitorDealer::class, $id);
        if ($question) {
            $this->entityManager->remove($question);
            $this->entityManager->flush();
        }
    }
}
