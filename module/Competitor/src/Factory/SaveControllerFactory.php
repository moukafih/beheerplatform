<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Competitor\Factory;

use Competitor\Form\CompetitorForm;
use Competitor\Controller\SaveController;
use Competitor\Service\CompetitorService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(CompetitorService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $categoryForm = new CompetitorForm($entityManager);
        return new SaveController($dataService, $categoryForm);
    }
}
