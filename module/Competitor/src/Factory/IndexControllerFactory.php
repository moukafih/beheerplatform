<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Competitor\Factory;

use Competitor\Controller\IndexController;
use Competitor\Service\CompetitorService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(CompetitorService::class);
        return new IndexController($dataService);
    }
}
