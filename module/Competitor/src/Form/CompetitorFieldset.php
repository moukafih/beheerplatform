<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-11-17
 * Time: 12:27
 */

namespace Competitor\Form;

use Application\Entity\CompetitorDealer;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Fieldset;
use Zend\Form\Element;
use Zend\InputFilter;

use Zend\InputFilter\InputFilterProviderInterface;

class CompetitorFieldset extends Fieldset implements InputFilterProviderInterface, ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('competitor');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject(new CompetitorDealer());

        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    private function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'brand',
            'type' => 'text',
            'options' => [
                'label' => 'Brand',
            ],
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'name' => 'address',
            'type' => 'text',
            'options' => [
                'label' => 'Address',
            ],
        ]);

        $this->add([
            'name' => 'zipcode',
            'type' => 'text',
            'options' => [
                'label' => 'Zipcode',
            ],
        ]);

        $this->add([
            'name' => 'city',
            'type' => 'text',
            'options' => [
                'label' => 'City',
            ],
        ]);

        $this->add([
            'name' => 'phone',
            'type' => 'text',
            'options' => [
                'label' => 'Phone',
            ],
        ]);
        $this->add([
            'name' => 'fax',
            'type' => 'text',
            'options' => [
                'label' => 'Fax',
            ],
        ]);

        $this->add([
            'name' => 'email',
            'type' => 'text',
            'options' => [
                'label' => 'Email',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'brand' => [
                'required' => true,
            ],
            'name' => [
                'required' => true,
            ],
        ];
    }
}
