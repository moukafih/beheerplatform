<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207130320 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_nl_lead_aftersales_scores CHANGE proces proces NUMERIC(10, 0) DEFAULT NULL, CHANGE klantbeleving klantbeleving NUMERIC(10, 0) DEFAULT NULL, CHANGE total total NUMERIC(10, 0) DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_lead_sales_scores CHANGE proces proces NUMERIC(10, 0) DEFAULT NULL, CHANGE klantbeleving klantbeleving NUMERIC(10, 0) DEFAULT NULL, CHANGE total total NUMERIC(10, 0) DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_sales_scores CHANGE total total NUMERIC(10, 0) DEFAULT NULL');
        $this->addSql('ALTER TABLE data_nl_feedback_aftersales_scores CHANGE total total NUMERIC(10, 0) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
