<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207134210 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE data_nl_lead_aftersales_scores CHANGE proces proces DOUBLE PRECISION DEFAULT NULL, CHANGE klantbeleving klantbeleving DOUBLE PRECISION DEFAULT NULL, CHANGE total total DOUBLE PRECISION NOT NULL');

        $this->addSql('ALTER TABLE data_nl_lead_sales_scores CHANGE proces proces DOUBLE PRECISION DEFAULT NULL, CHANGE klantbeleving klantbeleving DOUBLE PRECISION DEFAULT NULL, CHANGE total total DOUBLE PRECISION NOT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_sales_scores CHANGE lead lead DOUBLE PRECISION DEFAULT NULL, CHANGE warm_welkom warm_welkom DOUBLE PRECISION DEFAULT NULL, CHANGE beleving beleving DOUBLE PRECISION DEFAULT NULL, CHANGE aankomst aankomst DOUBLE PRECISION DEFAULT NULL, CHANGE showroom showroom DOUBLE PRECISION DEFAULT NULL, CHANGE ontvangst ontvangst DOUBLE PRECISION DEFAULT NULL, CHANGE inventarisatie inventarisatie DOUBLE PRECISION DEFAULT NULL, CHANGE presentatie presentatie DOUBLE PRECISION DEFAULT NULL, CHANGE proefrit proefrit DOUBLE PRECISION DEFAULT NULL, CHANGE offerte offerte DOUBLE PRECISION DEFAULT NULL, CHANGE help_mij_kiezen help_mij_kiezen DOUBLE PRECISION DEFAULT NULL, CHANGE een_transparante_koop een_transparante_koop DOUBLE PRECISION DEFAULT NULL, CHANGE total total DOUBLE PRECISION NOT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_aftersales_scores CHANGE lead lead DOUBLE PRECISION DEFAULT NULL, CHANGE warm_welkom warm_welkom DOUBLE PRECISION DEFAULT NULL, CHANGE beleving beleving DOUBLE PRECISION DEFAULT NULL, CHANGE als_er_toch_iets_verkeerd_gaat als_er_toch_iets_verkeerd_gaat DOUBLE PRECISION DEFAULT NULL, CHANGE vertrouwde_service vertrouwde_service DOUBLE PRECISION DEFAULT NULL, CHANGE oplevering oplevering DOUBLE PRECISION DEFAULT NULL, CHANGE total total DOUBLE PRECISION NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
