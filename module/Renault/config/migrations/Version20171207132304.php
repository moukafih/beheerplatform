<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207132304 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');



        $this->addSql('ALTER TABLE data_nl_lead_aftersales_scores CHANGE visit_date visit_date DATETIME DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_lead_sales_scores CHANGE visit_date visit_date DATETIME DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_sales_scores CHANGE visit_date visit_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE data_nl_feedback_aftersales_scores CHANGE visit_date visit_date DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
