<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207133416 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_nl_lead_aftersales_scores CHANGE proces proces NUMERIC(5, 2) DEFAULT NULL, CHANGE klantbeleving klantbeleving NUMERIC(5, 2) DEFAULT NULL, CHANGE visit_date visit_date DATETIME NOT NULL, CHANGE total total NUMERIC(5, 2) DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_lead_sales_scores CHANGE proces proces NUMERIC(5, 2) DEFAULT NULL, CHANGE klantbeleving klantbeleving NUMERIC(5, 2) DEFAULT NULL, CHANGE visit_date visit_date DATETIME NOT NULL, CHANGE total total NUMERIC(5, 2) DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_sales_scores CHANGE lead lead NUMERIC(5, 2) DEFAULT NULL, CHANGE warm_welkom warm_welkom NUMERIC(5, 2) DEFAULT NULL, CHANGE beleving beleving NUMERIC(5, 2) DEFAULT NULL, CHANGE aankomst aankomst NUMERIC(5, 2) DEFAULT NULL, CHANGE showroom showroom NUMERIC(5, 2) DEFAULT NULL, CHANGE ontvangst ontvangst NUMERIC(5, 2) DEFAULT NULL, CHANGE inventarisatie inventarisatie NUMERIC(5, 2) DEFAULT NULL, CHANGE presentatie presentatie NUMERIC(5, 2) DEFAULT NULL, CHANGE proefrit proefrit NUMERIC(5, 2) DEFAULT NULL, CHANGE offerte offerte NUMERIC(5, 2) DEFAULT NULL, CHANGE help_mij_kiezen help_mij_kiezen NUMERIC(5, 2) DEFAULT NULL, CHANGE een_transparante_koop een_transparante_koop NUMERIC(5, 2) DEFAULT NULL, CHANGE visit_date visit_date DATETIME NOT NULL, CHANGE total total NUMERIC(5, 2) DEFAULT NULL');

        $this->addSql('ALTER TABLE data_nl_feedback_aftersales_scores CHANGE lead lead NUMERIC(5, 2) DEFAULT NULL, CHANGE warm_welkom warm_welkom NUMERIC(5, 2) DEFAULT NULL, CHANGE beleving beleving NUMERIC(5, 2) DEFAULT NULL, CHANGE als_er_toch_iets_verkeerd_gaat als_er_toch_iets_verkeerd_gaat NUMERIC(5, 2) DEFAULT NULL, CHANGE vertrouwde_service vertrouwde_service NUMERIC(5, 2) DEFAULT NULL, CHANGE oplevering oplevering NUMERIC(5, 2) DEFAULT NULL, CHANGE visit_date visit_date DATETIME NOT NULL, CHANGE total total NUMERIC(5, 2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
