<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207103917 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE salesafter_dashboard.waves (id INT AUTO_INCREMENT NOT NULL, type ENUM(\'lead_sales\', \'lead_aftersales\', \'feedback_sales\', \'feedback_aftersales\'), wave VARCHAR(255) NOT NULL, country INT NOT NULL, external_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE salesafter_dashboard.data_nl_lead_aftersales_scores (id INT AUTO_INCREMENT NOT NULL, wave_id INT DEFAULT NULL, report_id INT NOT NULL, proces NUMERIC(10, 0) NOT NULL, klantbeleving NUMERIC(10, 0) NOT NULL, code_dealer INT NOT NULL, visit_date DATETIME NOT NULL, total NUMERIC(10, 0) NOT NULL, INDEX IDX_7699100E9461E358 (wave_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE salesafter_dashboard.data_nl_lead_sales_scores (id INT AUTO_INCREMENT NOT NULL, wave_id INT DEFAULT NULL, report_id INT NOT NULL, proces NUMERIC(10, 0) NOT NULL, klantbeleving NUMERIC(10, 0) NOT NULL, code_dealer INT NOT NULL, visit_date DATETIME NOT NULL, total NUMERIC(10, 0) NOT NULL, INDEX IDX_8131AF739461E358 (wave_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE salesafter_dashboard.data_nl_feedback_sales_scores (id INT AUTO_INCREMENT NOT NULL, wave_id INT DEFAULT NULL, report_id INT NOT NULL, lead NUMERIC(10, 0) DEFAULT NULL, warm_welkom NUMERIC(10, 0) DEFAULT NULL, beleving NUMERIC(10, 0) DEFAULT NULL, aankomst NUMERIC(10, 0) DEFAULT NULL, showroom NUMERIC(10, 0) DEFAULT NULL, ontvangst NUMERIC(10, 0) DEFAULT NULL, inventarisatie NUMERIC(10, 0) DEFAULT NULL, presentatie NUMERIC(10, 0) DEFAULT NULL, proefrit NUMERIC(10, 0) DEFAULT NULL, offerte NUMERIC(10, 0) DEFAULT NULL, help_mij_kiezen NUMERIC(10, 0) DEFAULT NULL, een_transparante_koop NUMERIC(10, 0) DEFAULT NULL, code_dealer INT NOT NULL, visit_date DATETIME NOT NULL, total NUMERIC(10, 0) NOT NULL, INDEX IDX_4EF4D5289461E358 (wave_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE salesafter_dashboard.data_nl_feedback_aftersales_scores (id INT AUTO_INCREMENT NOT NULL, wave_id INT DEFAULT NULL, report_id INT NOT NULL, lead NUMERIC(10, 0) DEFAULT NULL, warm_welkom NUMERIC(10, 0) DEFAULT NULL, beleving NUMERIC(10, 0) DEFAULT NULL, als_er_toch_iets_verkeerd_gaat NUMERIC(10, 0) DEFAULT NULL, vertrouwde_service NUMERIC(10, 0) DEFAULT NULL, oplevering NUMERIC(10, 0) DEFAULT NULL, code_dealer INT NOT NULL, visit_date DATETIME NOT NULL, total NUMERIC(10, 0) NOT NULL, INDEX IDX_E6DE31069461E358 (wave_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE salesafter_dashboard.data_nl_lead_aftersales_scores ADD CONSTRAINT FK_7699100E9461E358 FOREIGN KEY (wave_id) REFERENCES salesafter_dashboard.waves (id)');
        $this->addSql('ALTER TABLE salesafter_dashboard.data_nl_lead_sales_scores ADD CONSTRAINT FK_8131AF739461E358 FOREIGN KEY (wave_id) REFERENCES salesafter_dashboard.waves (id)');
        $this->addSql('ALTER TABLE salesafter_dashboard.data_nl_feedback_sales_scores ADD CONSTRAINT FK_4EF4D5289461E358 FOREIGN KEY (wave_id) REFERENCES salesafter_dashboard.waves (id)');

        $this->addSql('ALTER TABLE salesafter_dashboard.data_nl_feedback_aftersales_scores ADD CONSTRAINT FK_E6DE31069461E358 FOREIGN KEY (wave_id) REFERENCES salesafter_dashboard.waves (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE salesafter_dashboard.data_nl_lead_aftersales_scores');
        $this->addSql('DROP TABLE salesafter_dashboard.data_nl_lead_sales_scores');
        $this->addSql('DROP TABLE salesafter_dashboard.waves');
        $this->addSql('DROP TABLE salesafter_dashboard.data_nl_feedback_sales_scores');
        $this->addSql('DROP TABLE salesafter_dashboard.data_nl_feedback_aftersales_scores');
    }
}
