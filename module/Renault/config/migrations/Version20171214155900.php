<?php

namespace Renault\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171214155900 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE emailalert_check CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE source source VARCHAR(255) NOT NULL, CHANGE url url VARCHAR(255) NOT NULL, CHANGE in_queue in_queue TINYINT(1) NOT NULL, CHANGE created created DATETIME NOT NULL, ADD PRIMARY KEY (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
