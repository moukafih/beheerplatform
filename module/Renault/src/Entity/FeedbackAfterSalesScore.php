<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-12-17
 * Time: 10:17
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.data_nl_feedback_aftersales_scores")
 *
 * @category Renault
 * @package  Entity
 */
class FeedbackAfterSalesScore extends MysteryScore
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $lead;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $warm_welkom;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $beleving;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $als_er_toch_iets_verkeerd_gaat;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $vertrouwde_service;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $oplevering;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->lead      = isset($data['lead']) ? $data['lead'] : null;
        $this->warm_welkom      = isset($data['warm_welkom']) ? $data['warm_welkom'] : null;
        $this->beleving         = isset($data['beleving']) ? $data['beleving'] : null;

        $this->als_er_toch_iets_verkeerd_gaat  = isset($data['als_er_toch_iets_verkeerd_gaat']) ? $data['als_er_toch_iets_verkeerd_gaat'] : null;
        $this->vertrouwde_service  = isset($data['vertrouwde_service']) ? $data['vertrouwde_service'] : null;
        $this->oplevering  = isset($data['oplevering']) ? $data['oplevering'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return array_merge(parent::getArrayCopy(), [
            'lead'       => $this->lead,
            'warm_welkom'       => $this->warm_welkom,
            'beleving'          => $this->beleving,

            'als_er_toch_iets_verkeerd_gaat'          => $this->als_er_toch_iets_verkeerd_gaat,
            'vertrouwde_service'          => $this->vertrouwde_service,
            'oplevering'         => $this->oplevering,
        ]);
    }
}
