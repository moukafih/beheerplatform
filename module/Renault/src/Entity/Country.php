<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 17-3-17
 * Time: 10:46
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.country")
 *
 * @category Renault
 * @package  Entity
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="country_id")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cluster")
     * @ORM\JoinColumn(name="cluster_id", referencedColumnName="cluster_id")
     * @var Cluster $cluster
     */
    private $cluster;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name_short;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $locale_primary;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $locale_secondary;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $state;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Cluster
     */
    public function getCluster()
    {
        return $this->cluster;
    }

    /**
     * @param Cluster $cluster
     */
    public function setCluster($cluster)
    {
        $this->cluster = $cluster;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameShort()
    {
        return $this->name_short;
    }

    /**
     * @param string $name_short
     */
    public function setNameShort($name_short)
    {
        $this->name_short = $name_short;
    }

    /**
     * @return string
     */
    public function getLocalePrimary()
    {
        return $this->locale_primary;
    }

    /**
     * @param string $locale_primary
     */
    public function setLocalePrimary($locale_primary)
    {
        $this->locale_primary = $locale_primary;
    }

    /**
     * @return string
     */
    public function getLocaleSecondary()
    {
        return $this->locale_secondary;
    }

    /**
     * @param string $locale_secondary
     */
    public function setLocaleSecondary($locale_secondary)
    {
        $this->locale_secondary = $locale_secondary;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
}
