<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-12-17
 * Time: 10:17
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.data_nl_feedback_sales_scores")
 *
 * @category Renault
 * @package  Entity
 */
class FeedbackSalesScore extends MysteryScore
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $lead;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $warm_welkom;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $beleving;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $aankomst;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $showroom;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $ontvangst;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $inventarisatie;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $presentatie;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $proefrit;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $offerte;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $help_mij_kiezen;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $een_transparante_koop;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $thema;

    /**
     * @return float
     */
    public function getAankomst()
    {
        return $this->aankomst;
    }

    /**
     * @param float $aankomst
     */
    public function setAankomst($aankomst)
    {
        $this->aankomst = $aankomst;
    }

    /**
     * @return float
     */
    public function getShowroom()
    {
        return $this->showroom;
    }

    /**
     * @param float $showroom
     */
    public function setShowroom($showroom)
    {
        $this->showroom = $showroom;
    }

    /**
     * @return float
     */
    public function getOntvangst()
    {
        return $this->ontvangst;
    }

    /**
     * @param float $ontvangst
     */
    public function setOntvangst($ontvangst)
    {
        $this->ontvangst = $ontvangst;
    }

    /**
     * @return float
     */
    public function getInventarisatie()
    {
        return $this->inventarisatie;
    }

    /**
     * @param float $inventarisatie
     */
    public function setInventarisatie($inventarisatie)
    {
        $this->inventarisatie = $inventarisatie;
    }

    /**
     * @return float
     */
    public function getPresentatie()
    {
        return $this->presentatie;
    }

    /**
     * @param float $presentatie
     */
    public function setPresentatie($presentatie)
    {
        $this->presentatie = $presentatie;
    }

    /**
     * @return float
     */
    public function getProefrit()
    {
        return $this->proefrit;
    }

    /**
     * @param float $proefrit
     */
    public function setProefrit($proefrit)
    {
        $this->proefrit = $proefrit;
    }

    /**
     * @return float
     */
    public function getOfferte()
    {
        return $this->offerte;
    }

    /**
     * @param float $offerte
     */
    public function setOfferte($offerte)
    {
        $this->offerte = $offerte;
    }

    /**
     * @return float
     */
    public function getBeleving()
    {
        return $this->beleving;
    }

    /**
     * @param float $beleving
     */
    public function setBeleving($beleving)
    {
        $this->beleving = $beleving;
    }

    /**
     * @return float
     */
    public function getWarmWelkom()
    {
        return $this->warm_welkom;
    }

    /**
     * @param float $warm_welkom
     */
    public function setWarmWelkom($warm_welkom)
    {
        $this->warm_welkom = $warm_welkom;
    }

    /**
     * @return float
     */
    public function getHelpMijKiezen()
    {
        return $this->help_mij_kiezen;
    }

    /**
     * @param float $help_mij_kiezen
     */
    public function setHelpMijKiezen(float $help_mij_kiezen)
    {
        $this->help_mij_kiezen = $help_mij_kiezen;
    }

    /**
     * @return float
     */
    public function getEenTransparanteKoop()
    {
        return $this->een_transparante_koop;
    }

    /**
     * @param float $een_transparante_koop
     */
    public function setEenTransparanteKoop($een_transparante_koop)
    {
        $this->een_transparante_koop = $een_transparante_koop;
    }

    /**
     * @return float
     */
    public function getThema()
    {
        return $this->thema;
    }

    /**
     * @param float $thema
     */
    public function setThema($thema)
    {
        $this->thema = $thema;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->lead      = isset($data['lead']) ? $data['lead'] : null;
        $this->warm_welkom      = isset($data['warm_welkom']) ? $data['warm_welkom'] : null;
        $this->beleving         = isset($data['beleving']) ? $data['beleving'] : null;

        $this->aankomst         = isset($data['aankomst']) ? $data['aankomst'] : null;
        $this->showroom         = isset($data['showroom']) ? $data['showroom'] : null;
        $this->ontvangst        = isset($data['ontvangst']) ? $data['ontvangst'] : null;
        $this->inventarisatie   = isset($data['inventarisatie']) ? $data['inventarisatie'] : null;
        $this->presentatie      = isset($data['presentatie']) ? $data['presentatie'] : null;
        $this->proefrit         = isset($data['proefrit']) ? $data['proefrit'] : null;
        $this->offerte          = isset($data['offerte']) ? $data['offerte'] : null;
        $this->help_mij_kiezen  = isset($data['help_mij_kiezen']) ? $data['help_mij_kiezen'] : null;
        $this->een_transparante_koop  = isset($data['een_transparante_koop']) ? $data['een_transparante_koop'] : null;
        $this->thema  = isset($data['thema']) ? $data['thema'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return array_merge(parent::getArrayCopy(), [
            'lead'       => $this->lead,
            'warm_welkom'       => $this->warm_welkom,
            'beleving'          => $this->beleving,

            'aankomst'          => $this->aankomst,
            'showroom'          => $this->showroom,
            'ontvangst'         => $this->ontvangst,
            'inventarisatie'    => $this->inventarisatie,
            'presentatie'       => $this->presentatie,
            'proefrit'          => $this->proefrit,
            'offerte'           => $this->offerte,
            'help_mij_kiezen'   => $this->help_mij_kiezen,
            'een_transparante_koop' => $this->een_transparante_koop,
            'thema' => $this->thema,
        ]);
    }
}
