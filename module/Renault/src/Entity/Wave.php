<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 9:22
 */

namespace Renault\Entity;

use Application\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.waves")
 *
 * @category Renault
 * @package  Entity
 */
class Wave implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('lead_sales', 'lead_aftersales', 'feedback_sales', 'feedback_aftersales')")
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $wave;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $country = 1;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $external_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param string $wave
     */
    public function setWave($wave)
    {
        $this->wave = $wave;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->external_id;
    }

    /**
     * @param int $external_id
     */
    public function setExternalId($external_id)
    {
        $this->external_id = $external_id;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id     = isset($data['id']) ? $data['id'] : null;
        $this->type = isset($data['type']) ? $data['type'] : null;
        $this->wave  = isset($data['wave']) ? $data['wave'] : null;
        $this->country  = isset($data['country']) ? $data['country'] : null;
        $this->external_id  = isset($data['external_id']) ? $data['external_id'] : null;
    }

    /**
     * @return mixed
     */
    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'wave' => $this->wave,
            'country' => $this->country,
            'external_id' => $this->external_id,
        ];
    }
}
