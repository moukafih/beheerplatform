<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 15:07
 */

namespace Renault\Entity;

use Application\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/*
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.scores")
 *
 * @category Renault
 * @package  Entity
 */
class Score implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Wave")
     * @ORM\JoinColumn(name="wave_id", referencedColumnName="id")
     * @var Wave $wave
     */
    private $wave;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $category;

    /**
     * @ORM\Column(type="decimal")
     * @var double
     */
    private $score;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param string $wave
     */
    public function setWave($wave)
    {
        $this->wave = $wave;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param float $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        // TODO: Implement exchangeArray() method.
    }

    /**
     * @return mixed
     */
    public function getArrayCopy()
    {
        // TODO: Implement getArrayCopy() method.
    }
}
