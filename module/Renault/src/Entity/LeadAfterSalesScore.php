<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-12-17
 * Time: 10:09
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.data_nl_lead_aftersales_scores")
 *
 * @category Renault
 * @package  Entity
 */
class LeadAfterSalesScore extends LeadScore
{
}
