<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 14-12-17
 * Time: 12:17
 */

namespace Renault\Entity;

namespace Renault\Entity;

use Application\Entity\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.emailalert_check")
 *
 * @category Renault
 * @package  Entity
 */
class EmailAlertCheck implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="code_dealer")
     * @var int
     */
    private $dealer_code;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $source;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $in_queue = false;

    /**
     * @ORM\Column(type="datetime")
     * @var datetime
     */
    private $created;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDealerCode()
    {
        return $this->dealer_code;
    }

    /**
     * @param int $dealer_code
     */
    public function setDealerCode($dealer_code)
    {
        $this->dealer_code = $dealer_code;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool
     */
    public function isInQueue()
    {
        return $this->in_queue;
    }

    /**
     * @param bool $in_queue
     */
    public function setInQueue($in_queue)
    {
        $this->in_queue = $in_queue;
    }

    /**
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id           = isset($data['id']) ? $data['id'] : null;
        $this->dealer_code  = isset($data['dealer_code']) ? $data['dealer_code'] : null;
        $this->source       = isset($data['source']) ? $data['source'] : null;
        $this->url          = isset($data['url']) ? $data['url'] : null;
        $this->in_queue     = isset($data['in_queue']) ? $data['in_queue'] : null;
        $this->created      = isset($data['created']) ? $data['created'] : null;
    }

    /**
     * @return mixed
     */
    public function getArrayCopy()
    {
        return [
            'id'            => $this->id,
            'dealer_code'   => $this->dealer_code,
            'source'        => $this->source,
            'url'           => $this->url,
            'in_queue'      => $this->in_queue,
            'created'       => $this->created,
        ];
    }
}
