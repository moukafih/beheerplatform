<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-12-17
 * Time: 9:41
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\EntityInterface;

/**
 * @ORM\MappedSuperclass
 */
class MysteryScore implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Wave")
     * @ORM\JoinColumn(name="wave_id", referencedColumnName="id")
     * @var Wave $wave
     */
    protected $wave;

    /**
     * @ORM\Column(type="integer", name="code_dealer")
     * @var int
     */
    protected $dealer_code;

    /**
     * @ORM\Column(type="datetime")
     * @var datetime
     */
    protected $visit_date;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $report_id;

    /**
     * @ORM\Column(type="float")
     * @var double
     */
    protected $total;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param mixed $wave
     */
    public function setWave($wave)
    {
        $this->wave = $wave;
    }

    /**
     * @return int
     */
    public function getDealerCode()
    {
        return $this->dealer_code;
    }

    /**
     * @param int $dealer_code
     */
    public function setDealerCode($dealer_code)
    {
        $this->dealer_code = $dealer_code;
    }

    /**
     * @return datetime
     */
    public function getVisitDate()
    {
        return $this->visit_date;
    }

    /**
     * @param datetime $visit_date
     */
    public function setVisitDate($visit_date)
    {
        $this->visit_date = $visit_date;
    }

    /**
     * @return int
     */
    public function getReportId()
    {
        return $this->report_id;
    }

    /**
     * @param int $report_id
     */
    public function setReportId($report_id)
    {
        $this->report_id = $report_id;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id           = isset($data['id']) ? $data['id'] : null;
        $this->wave         = isset($data['wave']) ? $data['wave'] : null;
        $this->dealer_code  = isset($data['dealer_code']) ? $data['dealer_code'] : null;
        $this->visit_date   = isset($data['visit_date']) ? $data['visit_date'] : null;
        $this->report_id    = isset($data['report_id']) ? $data['report_id'] : null;
        $this->total        = isset($data['total']) ? $data['total'] : null;
    }

    /**
     * @return mixed
     */
    public function getArrayCopy()
    {
        return [
            'id'            => $this->id,
            'wave'          => $this->wave,
            'dealer_code'   => $this->dealer_code,
            'visit_date'    => $this->visit_date,
            'report_id'     => $this->report_id,
            'total'         => $this->total,
        ];
    }
}
