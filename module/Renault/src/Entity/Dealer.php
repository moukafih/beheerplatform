<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 16-3-17
 * Time: 16:43
 */

namespace Renault\Entity;

use Application\Entity\AbstractDealer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.dealer")
 *
 * @category Renault
 * @package  Entity
 */
class Dealer extends AbstractDealer
{
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $dealernr;

    /**
     * @ORM\Column(type="integer", name="code_dealer")
     * @var int
     */
    private $dealer_code;

    /**
     * @ORM\Column(type="integer", name="country_id")
     * @var int
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="Hub")
     * @ORM\JoinColumn(name="hub_id", referencedColumnName="hub_id")
     * @var Hub $hub
     */
    private $hub;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="zone_id")
     * @var Hub $hub
     */
    private $zone;

    /**
     * @ORM\Column(type="datetime")
     * @var string
     */
    private $timestamp;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $contract_level;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $renault_contract;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $renault_activity;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $dacia_contract;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $dacia_activity;

    /**
     * @return int
     */
    public function getDealernr()
    {
        return $this->dealernr;
    }

    /**
     * @param int $dealernr
     */
    public function setDealernr($dealernr)
    {
        $this->dealernr = $dealernr;
    }

    /**
     * @return int
     */
    public function getDealerCode()
    {
        return $this->dealer_code;
    }

    /**
     * @param int $dealer_code
     */
    public function setDealerCode($dealer_code)
    {
        $this->dealer_code = $dealer_code;
    }

    /**
     * @return Hub
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param Hub $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getContractLevel()
    {
        return $this->contract_level;
    }

    /**
     * @param string $contract_level
     */
    public function setContractLevel($contract_level)
    {
        $this->contract_level = $contract_level;
    }

    /**
     * @return string
     */
    public function getRenaultContract()
    {
        return $this->renault_contract;
    }

    /**
     * @param string $renault_contract
     */
    public function setRenaultContract($renault_contract)
    {
        $this->renault_contract = $renault_contract;
    }

    /**
     * @return string
     */
    public function getRenaultActivity()
    {
        return $this->renault_activity;
    }

    /**
     * @param string $renault_activity
     */
    public function setRenaultActivity($renault_activity)
    {
        $this->renault_activity = $renault_activity;
    }

    /**
     * @return string
     */
    public function getDaciaContract()
    {
        return $this->dacia_contract;
    }

    /**
     * @param string $dacia_contract
     */
    public function setDaciaContract($dacia_contract)
    {
        $this->dacia_contract = $dacia_contract;
    }

    /**
     * @return string
     */
    public function getDaciaActivity()
    {
        return $this->dacia_activity;
    }

    /**
     * @param string $dacia_activity
     */
    public function setDaciaActivity($dacia_activity)
    {
        $this->dacia_activity = $dacia_activity;
    }

    /**
     * @return mixed
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param mixed $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }

    /**
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param int $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /** @param $data */
    public function exchangeArray($data)
    {
        // TODO: Implement exchangeArray() method.
    }

    /** @return mixed */
    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'dealernr' => $this->dealernr,
            'dealer_code' => $this->dealer_code,
            'hub' => $this->getHub()->toArray(),
            'zone' => $this->getZone()->toArray(),
            'name' => $this->name,
            'address' => $this->address,
            'zipcode' => $this->zipcode,
            'city' => $this->city,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'email' => $this->email,
            'contract_level' => $this->contract_level,
            'renault_contract' => $this->renault_contract,
            'renault_activity' => $this->renault_activity,
            'dacia_contract' => $this->dacia_contract,
            'dacia_activity' => $this->dacia_activity,
        ];
    }
}
