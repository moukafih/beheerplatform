<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 15:13
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.data_nl_lead_sales_scores")
 *
 * @category Renault
 * @package  Entity
 */
class LeadSalesScore extends LeadScore
{
}
