<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-12-17
 * Time: 10:13
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class LeadScore extends MysteryScore
{
    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $proces;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var double
     */
    protected $klantbeleving;

    /**
     * @return float
     */
    public function getProces()
    {
        return $this->proces;
    }

    /**
     * @param float $proces
     */
    public function setProces($proces)
    {
        $this->proces = $proces;
    }

    /**
     * @return float
     */
    public function getKlantbeleving()
    {
        return $this->klantbeleving;
    }

    /**
     * @param float $klantbeleving
     */
    public function setKlantbeleving($klantbeleving)
    {
        $this->klantbeleving = $klantbeleving;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->proces  = isset($data['proces']) ? $data['proces'] : null;
        $this->klantbeleving  = isset($data['klantbeleving']) ? $data['klantbeleving'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return array_merge(parent::getArrayCopy(), [
            'proces'          => $this->proces,
            'klantbeleving'   => $this->klantbeleving,
        ]);
    }
}
