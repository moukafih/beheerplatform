<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 16-3-17
 * Time: 16:43
 */

namespace Renault\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="salesafter_dashboard.hub")
 *
 * @category Renault
 * @package  Entity
 */
class Hub
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="hub_id")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Dealer")
     * @ORM\JoinColumn(name="main_dealer_id", referencedColumnName="dealer_id")
     * @var Dealer $dealer
     */
    private $main_dealer;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="country_id")
     * @var Country $country
     */
    private $country;

    /** TODO this relation must be gone
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="zone_id")
     * @var Zone $zone
     */
    private $zone;

    /**
     * @ORM\OneToMany(targetEntity="Dealer", mappedBy="hub")
     * @var \Doctrine\Common\Collections\ArrayCollection
     **/
    private $dealers;

    public function __construct()
    {
        $this->dealers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMainDealer()
    {
        return $this->main_dealer;
    }

    /**
     * @param mixed $main_dealer
     */
    public function setMainDealer($main_dealer)
    {
        $this->main_dealer = $main_dealer;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param Zone $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getDealers()
    {
        return $this->dealers;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $dealers
     */
    public function setDealers($dealers)
    {
        $this->dealers = $dealers;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
