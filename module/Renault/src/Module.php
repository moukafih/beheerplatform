<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 11-4-17
 * Time: 15:44
 */

namespace Renault;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param ModuleManager $moduleManager
     */
    public function init(ModuleManager $moduleManager)
    {
        /**
         * @var \Zend\EventManager\EventManager $eventManager
         */
        $eventManager = $moduleManager->getEventManager();

        /**
         * @var \Zend\EventManager\SharedEventManager $sharedEventManager
         */
        $sharedEventManager = $eventManager->getSharedManager();

        //echo "User init <br/>";
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        //echo "User onBootstrap <br/>";
    }
}
