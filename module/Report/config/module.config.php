<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 23-11-17
 * Time: 12:23
 */
namespace Report;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'report' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/report',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index[/:report]',
                            'constraints' => [
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'pdf' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/pdf[/:report]',
                            'constraints' => [
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\PdfController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:project[/:report]]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                                'report'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ReportController::class,
                                'action'     => 'delete',
                            ],
                        ],
                    ],
                    'export' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/export/:project',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ExcelController::class,
                                'action'     => 'export',
                            ],
                        ],
                    ],
                    'publish' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/publish',
                            'defaults' => [
                                'controller' => Controller\ReportController::class,
                                'action'     => 'xhrPublish',
                            ],
                        ],
                    ],
                ],
            ],
        ]
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Factory\IndexControllerFactory::class,
            Controller\ReportController::class => Factory\ReportControllerFactory::class,
            Controller\PdfController::class => Factory\PdfControllerFactory::class,
            Controller\ExcelController::class => Factory\ExcelControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\ReportService::class => Factory\ReportServiceFactory::class,
            Service\ExcelService::class  => Factory\ExcelServiceFactory::class,

        ]
    ],

    'view_manager' => [
        'template_map' => [
            'report/pdf' => __DIR__ . '/../view/report/pdf/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    'acl' => [
        'resources' => [
            'allow' => [
                Controller\IndexController::class  => [
                    'index' => ['admin', 'country'],
                ],
                Controller\ReportController::class => [
                    'delete' => 'admin',
                    'xhrPublish' => 'admin',
                ],
                Controller\PdfController::class    => [
                    'index' => ['admin', 'country'],
                ],
                Controller\ExcelController::class  => [
                    'export' => ['admin', 'country'],
                ],
            ]
        ]
    ]
];
