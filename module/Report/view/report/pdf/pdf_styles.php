<style>
table{
	border-collapse: collapse;
}

table.pdf-title{
	width:100%;
	margin:0 0 25px 0;
}
	.pdf-title h1{
		text-align:left;
		margin:0 !important;
		color:#f7b100;
	}
		.pdf-title h1 span{
			color:grey !important;
			font-size:20px;
		}
	.pdf-title img{
		text-align:right;
	}


table.pdf-subtitle{
	width:100%;
	margin:0 0 10px 0;
}
	.pdf-subtitle label{
		display:inline;
		margin:0 15px 0 0;
	}
	.pdf-subtitle p{
		display:inline;
		font-weight:bold;
		margin:0;
	}

table.pdf-totalscore{
	width:100%;
	margin-bottom:10px;
}
	.pdf-totalscore tr{
		background-color:#f7b100;
	}
		.pdf-totalscore tr td{
			color:color:#fff;
			font-size:18px;
			padding:15px;
		}
			.pdf-totalscore tr td .score_bar{
				float:left;
				width:100%;
				height:25px;
				background-color:#f1f1f1;
				border-radius:3px;
				overflow:hidden;
				border: 1px solid #DADADA;
				padding:0;
				position:relative;
			}
				.pdf-totalscore tr td .score_bar .percentage_bar{
					display:inline-block;
					margin:0;
					height:25px;
				}
					.pdf-totalscore tr td .score_bar .percentage_bar p{
						float:left;
						margin:3px 0 0 5px !important;
						font-size:14px !important;
					}

table.pdf-description{
	width:100%;
	margin-bottom:10px;
}
	table.pdf-description tr td{
		border:solid 1px #dddddd;
		padding:15px;
		font-size:12px;
		color:#8e8a88;
	}

table.pdf-answer{
	width:100%;
}
	.pdf-answer .heading{
		background-color:#e6e6e6;
	}
		.pdf-answer .heading td{
			color:#8e8a88;
			padding:15px;
		}
		.pdf-answer .heading.cat-heading td{
			font-size:18px;
			font-weight:bold;
		}
			.pdf-answer .heading.cat-heading td .score_bar{
				float:left;
				width:100%;
				height:25px;
				background-color:#f1f1f1;
				border-radius:3px;
				overflow:hidden;
				border: 1px solid #DADADA;
				padding:0;
				position:relative;
			}
				.pdf-answer .heading.cat-heading td .score_bar .percentage_bar{
					display:inline-block;
					margin:0;
					height:25px;
				}
					.pdf-answer .heading.cat-heading td .score_bar .percentage_bar p{
						float:left;
						margin:3px 0 0 5px !important;
						font-size:14px !important;
					}
		.pdf-answer .heading.cols-heading td{
			font-size:12px;
			font-weight:normal;
			padding:5px 15px;
		}

	.pdf-answer .answer td.col1{
		border-left:solid 1px #e6e6e6;
	}
	.pdf-answer .answer td.col2{
		text-align:center;
	}
	.pdf-answer .answer td.col3{
		text-align:center;
		font-weight:bold;
	}
	.pdf-answer .answer td.col4{
		border-right:solid 1px #e6e6e6;
		text-align:center;
	}
	.pdf-answer .answer td.col5{
		
	}
	.pdf-answer .answer td {
		border-bottom:solid 1px #e6e6e6;
		border-bottom:solid 1px #e6e6e6;
		color:#8e8a88;
		padding:5px 15px;
		font-size:12px;
	}
			.pdf-answer .answer td span{
				font-size:10px;
			}
	.pdf-answer .remark td{
		border-right:solid 1px #e6e6e6;
		border-left:solid 1px #e6e6e6;
	}
	
</style>