<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-12-17
 * Time: 16:48
 */

namespace ReportTest\Service;

use Application\Entity\VisitRenault;
use Report\Service\ReportService;
use Survey\Entity\Report;

class ReportServiceTest extends \ApplicationTest\TestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var ExcelService
     */
    private $service;

    /** @var \Application\Entity\VisitRenault */
    private $visit;

    public function setUp()
    {
        /*$this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();*/

        $this->entityManager = $this->getEntityManager();
        $this->service = new ReportService($this->entityManager);

        $location_renault = $this->entityManager->getRepository('Renault\Entity\Dealer')->find(187);
        $advisor = $this->entityManager->getRepository('Renault\Entity\User')->findOneBy(['email' => 'beheer@nbeyond.nl']);


        $visit = new VisitRenault();
        $visit->exchangeArray([
            'project' => 9,
            'location'  => $location_renault,
            'advisor'  => $advisor,
            'date'  => new \DateTime('2017-12-14'),
            'remark'  => 'test',
        ]);

        $this->entityManager->persist($visit);
        $this->entityManager->flush();

        $this->visit = $visit;
    }

    public function testAddReport()
    {
        $report = new Report();
        $report->setVersion('v1');
        $report->setCreatedDate(new \DateTime());
        $report->setVisit($this->visit);
        $report->

        $this->entityManager->persist($report);
    }
}
