<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 29-11-17
 * Time: 15:22
 */

namespace ReportTest\Service;

use Doctrine\ORM\EntityManager;
use Report\Service\ExcelService;

class ExcelServiceTest extends \ApplicationTest\TestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var ExcelService
     */
    private $service;

    public function setUp()
    {
        /*$this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();*/

        $this->entityManager = $this->getEntityManager();
        $this->service = new ExcelService($this->entityManager);
    }

    protected static function getMethod($name)
    {
        $class = new \ReflectionClass(ExcelService::class);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    public function testCanGetResultsDataCompetitor()
    {
        //$foo = self::getMethod('resultsDataCompetitor');
        //$data = $foo->invokeArgs($this->service, [1]);

        $method = new \ReflectionMethod(ExcelService::class, 'resultsDataCompetitor');
        $method->setAccessible(true);
        $data = $method->invokeArgs($this->service, [1]);

        $this->assertNotNull($data);
        $this->assertEquals('Brand', $data[0][1]);
    }
}
