<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 16:11
 */

namespace Report\Service;

use Doctrine\ORM\EntityManager;
use Survey\Entity\Report;

class ReportService
{
    /**
     * @var EntityManager $entitymanager
     */
    private $entitymanager;

    /**
     * FeedService constructor.
     * @param EntityManager $entitymanager
     */
    public function __construct(EntityManager $entitymanager)
    {
        $this->entitymanager = $entitymanager;
    }

    /**
     * @param int $id
     *
     * @return \Survey\Entity\Report|null
     */
    public function get($id)
    {
        //TODO order by qnumber
        return $this->entitymanager->getRepository('Survey\Entity\Report')->findOneBy(['id'=> $id]);
    }

    /**
     * @param \Survey\Entity\Report $entity
     */
    public function save(Report $entity)
    {
        if ($entity->getId() == null) {
            $this->entitymanager->persist($entity);
        }

        if ($entity->getVersion() != 'def') {
            $entity->setCheckN('NO');
            $entity->setCheckR('NO');
            $entity->setPublish('NO');
            $entity->getVisit()
                ->setActive(true);
        }

        /*if ($entity->getVersion() == 'def') {
            $entity->getVisit()
                   ->setActive(false);
        }*/

        $this->entitymanager->flush();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $report = $this->entitymanager->find('Survey\Entity\Report', $id);
        if ($report) {
            $this->entitymanager->remove($report);
            $this->entitymanager->flush();
        }
    }

    public function publish($data)
    {
        if (empty($data) || $data['report_id'] == null) {
            throw new \Exception('Missing answer ID.');
        }

        $entity = $this->entitymanager->getRepository('Survey\Entity\Report')->findOneBy(['id' => $data['report_id']]);
        $entity->setPublish($data['publish']);

        // TODO calculate scores per parent category not childeren

        $this->entitymanager->flush();

        return ['status' => 'success', 'message' => 'Published'];
    }
}
