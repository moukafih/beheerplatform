<?php
namespace Report\Service;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Survey\Entity\Category;
use Survey\Entity\Report;

class ExcelService
{
    /**
     * @var EntityManager $entitymanager
     */
    private $entitymanager;

    private $response;

    private $project_id;

    /**
     * FeedService constructor.
     * @param EntityManager $entitymanager
     */
    public function __construct(EntityManager $entitymanager)
    {
        $this->entitymanager = $entitymanager;
    }

    public function setProjectId($project_id)
    {
        $this->project_id = $project_id;
    }

    public function getProjectId()
    {
        return $this->project_id;
    }

    private function getCheckedReportsByProjectId()
    {
        $query = $this->entitymanager->createQuery('SELECT r FROM Survey\Entity\Report r JOIN r.visit v WHERE r.check_n = :check_n AND v.project = :project_id');
        $query->setParameters(['project_id' => $this->project_id, 'check_n' => 'YES']);
        $reports = $query->getResult();

        return $reports;
    }

    /**
     * @param \Application\Entity\Project $project
     * @param $reports
     * @return mixed
     * @throws \Exception
     */
    public function getExport($project, $reports)
    {
        $answers_data = [];
        $results_data = [];

        // get Renault reports
        if (! $project->isCompetitor()) {
            $answers_data = $this->answersDataRenault($reports);
            $results_data = $this->resultsDataRenault($reports);
        }

        // get other reports
        if ($project->isCompetitor()) {
            $answers_data = $this->answersDataCompetitor($reports);
            $results_data = $this->resultsDataCompetitor($reports);
        }

        $filename = $project->getLabel() . ' - ' . $project->getWave() . ' - ' . date('Y-m-d');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator(" &beyond - beheerplatform")
            ->setTitle($filename);

        $sheet1 = $spreadsheet->getSheet(0);
        $sheet1->setTitle('results');

        $sheet1->fromArray($results_data, null, 'A1', true);

        $sheet2 = $spreadsheet->createSheet(1);
        $sheet2->setTitle('answers');

        $sheet2->fromArray($answers_data, null, 'A1', true);

        $highestColumn2 = $sheet2->getHighestDataColumn();

        foreach (range('A', 'D') as $columnID) {
            $sheet2->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $sheet2->getStyle("A1:{$highestColumn2}1")->getFont()->setBold(true);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        if (null === $this->response) {
            throw new \Exception("Could not find Response. Did you forget to set it?");
        }

        $this->response->getHeaders()
            ->clearHeaders()
            ->addHeaderLine('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            ->addHeaderLine('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
        ob_end_clean(); //clear the output buffer
        ob_start(); //start output buffer capture
        $writer->save('php://output');
        $output = ob_get_clean(); //store output buffer capture

        $this->response->setContent($output);
        return $this->response;
    }

    public function getFullExport()
    {
        /** @var \Application\Entity\Project $project */
        $project = $this->entitymanager->getRepository('Application\Entity\Project')->find($this->project_id);
        if (! $project) {
            throw new \Exception('no project found');
        }

        if (! $project->isCompetitor()) {
            $query = $this->entitymanager->createQuery('SELECT r FROM Survey\Entity\Report r JOIN Application\Entity\VisitRenault v WITH r.visit = v.id JOIN v.location l WHERE v.project = :project_id ORDER BY l.hub ASC, l.dealer_code ASC');
            $query->setParameters(['project_id' => $this->project_id]);
            $reports = $query->getResult();
        }

        if ($project->isCompetitor()) {
            $query = $this->entitymanager->createQuery('SELECT r FROM Survey\Entity\Report r JOIN Application\Entity\VisitCompetitor v WITH r.visit = v.id JOIN v.location l WHERE v.project = :project_id ORDER BY l.brand ASC, l.name ASC');
            $query->setParameters(['project_id' => $this->project_id]);
            $reports = $query->getResult();
        }

        return $this->getExport($project, $reports);
    }

    public function getExportCheckedByNbeyond()
    {
        /** @var \Application\Entity\Project $project */
        $project = $this->entitymanager->getRepository('Application\Entity\Project')->find($this->project_id);
        if (! $project) {
            throw new \Exception('no project found');
        }

        if (! $project->isCompetitor()) {
            $query = $this->entitymanager->createQuery("SELECT r FROM Survey\Entity\Report r JOIN Application\Entity\VisitRenault v WITH r.visit = v.id JOIN v.location l WHERE v.project = :project_id AND r.version = 'def' ORDER BY l.hub ASC, l.dealer_code ASC");
            $query->setParameters(['project_id' => $this->project_id]);
            $reports = $query->getResult();
        }

        if ($project->isCompetitor()) {
            $query = $this->entitymanager->createQuery("SELECT r FROM Survey\Entity\Report r JOIN Application\Entity\VisitCompetitor v WITH r.visit = v.id JOIN v.location l WHERE v.project = :project_id AND r.version = 'def' ORDER BY l.brand ASC, l.name ASC");
            $query->setParameters(['project_id' => $this->project_id]);
            $reports = $query->getResult();
        }

        return $this->getExport($project, $reports);
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     *
     * @return self
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @param $reports
     * @return array
     */
    private function answersDataCompetitor($reports)
    {
        // TODO Need refactoring
        $header = ['Brand', 'Dealer id', 'Dealer name', 'City'];
        $header = array_combine($header, $header);

        if (count($reports) < 1) {
            return [$header];
        }

        /** @var \Survey\Entity\Answer $answer */
        /** @var \Survey\Entity\Question $question */
        foreach ($reports[0]->getAnswers() as $answer) {
            $question = $answer->getQuestion();
            $i = $question->getQnumber();
            $header["Q{$i}"] = 'Q' . $i . ' - '. $question->getQuestion();
            $header["Q{$i}_remark"] = 'Q' . $i . ' - remark';
            $header["Q{$i}_score"] = 'Q' . $i . ' - score';
        }

        // create template row
        $template_row = array_combine(array_keys($header), array_fill(0, count($header), null));

        $data = [];
        $data[] = $header;

        foreach ($reports as $report) {
            $visit = $report->getVisit();
            /** @var \Application\Entity\CompetitorDealer $location */
            $location = $visit->getLocation();

            $row = $template_row;

            //$row['id'] = $report->getId();
            $row['Brand'] = $location->getBrand();
            $row['Dealer id'] = $location->getId();
            $row['Dealer name'] = $location->getName();
            $row['City'] = $location->getCity();

            foreach ($report->getAnswers() as $answer) {
                $i = $answer->getQuestion()->getQnumber();
                $row["Q{$i}"] = $answer->getAnswer();
                $row["Q{$i}_remark"] = $answer->getRemark();
                $row["Q{$i}_score"] = $answer->getScore();
            }
            $data[] = $row;
        }

        return $data;
    }

    private function resultsDataCompetitor($reports)
    {
        $categories = $this->getCategoriesPerProject($this->project_id);

        $header = [0 => '', 'brand' => 'Brand', 'dealer_id' => 'Dealer id', 'dealer_name' => 'Dealer name', 'city' => 'City'];
        $header = $this->makeHeader($categories, $header);
        $result[] = $header;

        $data = [];

        /** @var \Survey\Entity\Report $report */
        /** @var \Survey\Entity\Question $question */
        /** @var \Survey\Entity\Answer $answer */
        /** @var \Application\Entity\CompetitorDealer $location */
        foreach ($reports as $report) {
            $location = $report->getVisit()->getLocation();
            $brand = $location->getBrand();

            //$row = new \SplFixedArray(count($header));
            $row = array_combine(array_keys($header), array_fill(0, count($header), null));

            $row[0] = '';
            $row['brand'] = $brand;
            $row['dealer_id'] = $location->getId();
            $row['dealer_name'] = $location->getName();
            $row['city'] = $location->getCity();

            foreach ($categories as $cat_key => $mainCategory) {
                foreach ($mainCategory['subcat'] as $subCategory) {
                    foreach ($subCategory->getQuestions() as $question) {
                        $i = $question->getQnumber();
                        foreach ($report->getAnswers() as $answer) {
                            $qnumber = $answer->getQuestion()->getQnumber();
                            if ($qnumber == $i) {
                                $row['q'.$i] = $answer->getScoreInPercentage();
                            }
                        }
                    }
                    $row['total_' . $subCategory->getId()] = $report->getScorePerCategoryId($subCategory->getId());
                }
                $row['total_' . $cat_key] = $report->getScorePerCategoryId($cat_key);
            }

            $row['total_result'] = $report->getScore();

            $data[$brand][] = $row;
        }

        foreach ($data as $hub_id => $hub_data) {
            $hub_sum = array_combine(array_keys($header), array_fill(0, count($header), null));
            $hub_size = array_combine(array_keys($header), array_fill(0, count($header), null));
            foreach ($hub_data as $key1 => $report_data) {
                $result[] =  $report_data;

                foreach ($header as $key2 => $col) {
                    if (is_numeric($report_data[$key2]) && ! in_array($key2, ['dealer_id', 'dealer_code'])) {
                        ($hub_sum[$key2] == null) ? $hub_sum[$key2] = floatval($report_data[$key2]) : $hub_sum[$key2] += floatval($report_data[$key2]);
                        ($hub_size[$key2] == null) ? $hub_size[$key2] = 1 : $hub_size[$key2] += 1;
                    }
                }
            }

            $hub_total = array_combine(array_keys($header), array_fill(0, count($header), null));
            $hub_total[0] = "Total {$hub_id}";
            foreach ($hub_sum as $key => $sum) {
                if ($sum !== null && $hub_size[$key] > 0) {
                    $hub_total[$key] = round($sum / $hub_size[$key], 1);
                }
            }

            $result[] =  $hub_total;
        }

        return $result;
    }

    /**
     * @param $reports
     * @return array
     *
     * this function gives the answers of reports back as array
     */
    private function answersDataRenault($reports)
    {
        // TODO Need refactoring
        $header = ['Hub', 'Dealer code', 'Dealer name', 'City'];
        $header = array_combine($header, $header);
        // \Zend\Debug\Debug::Dump($header);exit();
        if (count($reports) < 1) {
            return [$header];
        }

        /** @var \Survey\Entity\Answer $answer */
        /** @var \Survey\Entity\Question $question */
        foreach ($reports[0]->getAnswers() as $answer) {
            $question = $answer->getQuestion();
            $i = $question->getQnumber();
            $header["Q{$i}"] = 'Q' . $i . ' - '. $question->getQuestion();
            $header["Q{$i}_remark"] = 'Q' . $i . ' - remark';
            $header["Q{$i}_score"] = 'Q' . $i . ' - score';
        }

        // create template row
        $template_row = array_combine(array_keys($header), array_fill(0, count($header), null));

        $data = [];
        $data[] = $header;

        foreach ($reports as $report) {
            $visit = $report->getVisit();
            $location = $visit->getLocation();

            $row = $template_row;

            //$row['id'] = $report->getId();
            $row['Hub'] = $location->getHub()->getId();
            $row['Dealer code'] = $location->getDealerCode();
            $row['Dealer name'] = $location->getName();
            $row['City'] = $location->getCity();

            foreach ($report->getAnswers() as $answer) {
                $i = $answer->getQuestion()->getQnumber();
                $row["Q{$i}"] = $answer->getAnswer();
                $row["Q{$i}_remark"] = $answer->getRemark();
                $row["Q{$i}_score"] = $answer->getScore();
            }
            $data[] = $row;
        }

        return $data;
    }

    /**
     * @param $reports
     * @return array
     *
     * this function calculate the total scores per category and hub
     */
    private function resultsDataRenault($reports)
    {
        // TODO Need refactoring
        //$query = $this->entitymanager->createQuery('SELECT h, d, v, r FROM Renault\Entity\Hub h JOIN h.dealers d JOIN d.visits v JOIN v.reports r WHERE v.project = :project_id ORDER BY h.id DESC, d.code_dealer ASC');
        /*$query = $this->entitymanager->createQuery('SELECT r FROM Survey\Entity\Report r JOIN Application\Entity\VisitRenault v WITH r.visit = v.id JOIN v.location l WHERE v.project = :project_id ORDER BY l.hub DESC, l.code_dealer ASC');
        $query->setParameters(['project_id' => $project_id]);
        $reports = $query->getResult();*/

        $categories = $this->getCategoriesPerProject($this->project_id);

        $header = [0 => '', 'brand' => 'Hub', 'dealer_code' => 'Dealer code', 'dealer_name' => 'Dealer name', 'city' => 'City'];
        $header = $this->makeHeader($categories, $header);
        $result[] = $header;

        $data = [];

        /** @var \Survey\Entity\Report $report */
        /** @var \Survey\Entity\Answer $answer */
        /** @var \Survey\Entity\Category $subCategory */
        /** @var \Renault\Entity\Hub $hub */
        /** @var \Renault\Entity\Dealer $location */
        foreach ($reports as $report) {
            $location = $report->getVisit()->getLocation();
            $hub = $location->getHub();

            //$row = new \SplFixedArray(count($header));
            $row = array_combine(array_keys($header), array_fill(0, count($header), null));

            $row[0] = '';
            $row['brand'] = $hub->getId();
            $row['dealer_code'] = $location->getDealerCode();
            $row['dealer_name'] = $location->getName();
            $row['city'] = $location->getCity();

            foreach ($categories as $cat_key => $mainCategory) {
                if (! empty($mainCategory['subcat'])) {
                    foreach ($mainCategory['subcat'] as $subCategory) {
                        foreach ($subCategory->getQuestions() as $question) {
                            $i = $question->getQnumber();
                            foreach ($report->getAnswers() as $answer) {
                                $qnumber = $answer->getQuestion()->getQnumber();
                                if ($qnumber == $i) {
                                    $row['q'.$i] = $answer->getScoreInPercentage();
                                }
                            }
                        }
                        //$key = 'total_' . str_replace(' ', '_', strtolower($subCategory->getName()));
                        $row['total_' . $subCategory->getId()] = $report->getScorePerCategoryId($subCategory->getId());
                    }
                }

                if (is_a($mainCategory['maincat'], Category::class)) {
                    foreach ($mainCategory['maincat']->getQuestions() as $question) {
                        $i = $question->getQnumber();
                        foreach ($report->getAnswers() as $answer) {
                            $qnumber = $answer->getQuestion()->getQnumber();
                            if ($qnumber == $i) {
                                $row['q'.$i] = $answer->getScoreInPercentage();
                            }
                        }
                    }
                }
                //$key = 'total_' . str_replace(' ', '_', strtolower($mainCategory['maincat']));
                $row['total_' . $cat_key] = $report->getScorePerCategoryId($cat_key);
            }

            $row['total_result'] = $report->getScore();

            $data[$hub->getId()][] = $row;
        }

        foreach ($data as $hub_id => $hub_data) {
            $hub_sum = array_combine(array_keys($header), array_fill(0, count($header), null));
            $hub_size = array_combine(array_keys($header), array_fill(0, count($header), null));
            foreach ($hub_data as $key1 => $report_data) {
                $result[] =  $report_data;

                foreach ($header as $key2 => $col) {
                    if (is_numeric($report_data[$key2]) && ! in_array($key2, ['dealer_id', 'dealer_code'], true)) {
                        ($hub_sum[$key2] == null) ? $hub_sum[$key2] = floatval($report_data[$key2]) : $hub_sum[$key2] += floatval($report_data[$key2]);
                        ($hub_size[$key2] == null) ? $hub_size[$key2] = 1 : $hub_size[$key2] += 1;
                    }
                }
            }

            $hub_total = array_combine(array_keys($header), array_fill(0, count($header), null));
            $hub_total[0] = "Total {$hub_id}";
            foreach ($hub_sum as $key => $sum) {
                if ($sum !== null && $hub_size[$key] > 0) {
                    $hub_total[$key] = round($sum / $hub_size[$key], 1);
                }
            }

            $result[] =  $hub_total;
        }

        return $result;
    }

    /**
     * @param $project_id
     * @return array
     */
    private function getCategoriesPerProject($project_id)
    {
        $query = $this->entitymanager->createQuery('SELECT c, q FROM Survey\Entity\Category c LEFT JOIN c.questions q WHERE q.project = :project_id ORDER BY c.name DESC, q.qnumber ASC');
        $query->setParameters(['project_id' => $project_id]);
        $result = $query->getResult();

        $categories = [];

        /** @var \Survey\Entity\Category $category */
        foreach ($result as $category) {
            if ($category->hasParent()) {
                $categories[$category->getParent()->getId()]['maincat'] = $category->getParent();
                $categories[$category->getParent()->getId()]['subcat'][$category->getId()] = $category;
            } else {
                $categories[$category->getId()]['maincat'] = $category;
            }
        }

        return $categories;
    }

    /**
     * @param $categories
     * @param $header
     * @return array
     */
    private function makeHeader($categories, $header)
    {
        /** @var \Survey\Entity\Category $subCategory */
        /** @var \Survey\Entity\Question $question */
        foreach ($categories as $cat_key => $mainCategory) {
            if (! empty($mainCategory['subcat'])) {
                foreach ($mainCategory['subcat'] as $subCategory) {
                    foreach ($subCategory->getQuestions() as $question) {
                        if ($question->getProject()->getId() == $this->project_id) {
                            $i = $question->getQnumber();
                            $header['q' . $i] = 'Q' . $i . ' - score';
                        }
                    }
                    //$key = 'total_' . str_replace(' ', '_', strtolower($subCategory->getName()));
                    $header['total_' . $subCategory->getId()] = "Total {$subCategory->getName()}";
                }
            }

            if (is_a($mainCategory['maincat'], Category::class)) {
                foreach ($mainCategory['maincat']->getQuestions() as $question) {
                    if ($question->getProject()->getId() == $this->project_id) {
                        $i = $question->getQnumber();
                        $header['q' . $i] = 'Q' . $i . ' - score';
                    }
                }
            }

            //$key = 'total_' . str_replace(' ', '_', strtolower($mainCategory['maincat']));
            $header['total_' . $cat_key] = "Total {$mainCategory['maincat']->getName()}";
        }
        $header['total_result'] = "Total result";

        return $header;
    }
}
