<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-8-17
 * Time: 14:17
 */

namespace Report\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class ReportForm extends Form
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    public function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'version',
            'options' => [
                'label' => 'Version:',
                'label_attributes' => [
                    'class'  => 'radio-inline'
                ],
                'value_options' => [
                    'v1' => 'Version 1',
                    'v2' => 'Version 2',
                    'def' => 'Def',
                ],
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'check_n',
            'options' => [
                'label' => 'Check &Beyond',
                'use_hidden_element' => true,
                'checked_value' => true,
                'unchecked_value' => false,
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'check_r',
            'options' => [
                'label' => 'Check Renault',
                'use_hidden_element' => true,
                'checked_value' => 'YES',
                'unchecked_value' => 'NO',
            ],
        ]);

        $submit = new Element\Submit('submit');
        $submit->setValue('Save');

        $this->add($submit);
    }
}
