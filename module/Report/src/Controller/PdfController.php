<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-9-17
 * Time: 14:23
 */

namespace Report\Controller;

use DOMPDFModule\View\Model\PdfModel;
use Report\Service\ReportService;
use Zend\Mvc\Controller\AbstractActionController;

class PdfController extends AbstractActionController
{
    /** @var \Report\Service\ReportService $reportService */
    private $reportService;

    /**
     * ReportPdfController constructor.
     * @param \Report\Service\ReportService $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    public function indexAction()
    {
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');

        if (0 === $report_id) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        try {
            $report = $this->reportService->get($report_id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'visit-'. $report->getVisit()->getId() .'_report-' . $report_id); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'portrait'); //"portrait | landscape"
        $pdf->setOption('isRemoteEnabled', true);
        // To set view variables
        $pdf->setVariables([
            'report' => $report
        ]);

        return $pdf;
    }
}
