<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 23-11-17
 * Time: 12:35
 */

namespace Report\Controller;

use Report\Form\ReportForm;
use Report\Service\ReportService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /** @var \Report\Service\ReportService */
    private $reportService;

    /**
     * @var \Report\Form\ReportForm $reportForm
     */
    private $reportForm;

    /**
     * ReportController constructor.
     * @param \Report\Service\ReportService $reportService
     * @param \Report\Form\ReportForm $reportForm
     */
    public function __construct(ReportService $reportService, ReportForm $reportForm)
    {
        $this->reportService = $reportService;
        $this->reportForm = $reportForm;
    }

    public function indexAction()
    {
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');

        if (0 === $report_id) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        try {
            $report = $this->reportService->get($report_id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('project', ['action' => 'index', 'client' => 1]);
        }

        $this->reportForm->bind($report);

        $viewData = [
            'form' => $this->reportForm,
            'report' => $report,
            'params' => $this->params()->fromQuery(),
        ];

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $viewData;
        }

        $this->reportForm->setData($request->getPost());

        if (! $this->reportForm->isValid()) {
            return $viewData;
        }

        $this->reportService->save($report);

        return [
            'form' => $this->reportForm,
            'report' => $report,
            'params' => $this->params()->fromQuery(),
            'status' => 'success'
        ];
    }
}
