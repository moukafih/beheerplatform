<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 16:06
 */

namespace Report\Controller;

use Report\Service\ExcelService;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class ReportController
 * @package Report\Controller
 */
class ExcelController extends AbstractActionController
{
    /** @var \Report\Service\ExcelService */
    private $excelService;

    /**
     * ReportController constructor.
     * @param \Report\Service\ExcelService $excelService
     */
    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    public function exportAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        
        $this->excelService->setResponse($this->getResponse());
        $this->excelService->setProjectId($project_id);

        if ($this->identity()->getRole() == 'admin') {
            return $this->excelService->getFullExport();
        }

        if ($this->identity()->getRole() == 'country') {
            return $this->excelService->getExportCheckedByNbeyond();
        }
    }
}
