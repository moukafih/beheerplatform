<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 23-11-17
 * Time: 12:47
 */

namespace Report\Controller;

use Report\Service\ReportService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    /** @var \Report\Service\ReportService */
    private $reportService;

    /**
     * ReportController constructor.
     * @param \Report\Service\ReportService $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    public function deleteAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $report_id = (int) $this->getEvent()->getRouteMatch()->getParam('report');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$report_id) {
            return $this->redirect()->toRoute('visit/index', ['action' => 'index', 'project' => $project_id]);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'project_id'    => $project_id,
                'report_id'    => $report_id,
                'report' => $this->reportService->get($report_id),
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->reportService->delete($id);
            //return $this->redirect()->toRoute('visit/index', ['action' => 'index', 'project' => $project_id], ['query' => $this->params()->fromQuery()]);
            return $this->redirect()->toRoute('project/index', ['action' => 'index', 'client' => 1]);
        }

        // Redirect to report
        return $this->redirect()->toRoute('report/index', ['action' => 'index', 'report' => $report_id]);
    }

    public function xhrPublishAction()
    {
        $view = new ViewModel();
        $request = $this->getRequest();
        $view->setTerminal(true);

        if (!$request->isPost()) {
            return $view;
        }

        try {
            $result = $this->reportService->publish($request->getPost());
        } catch (\Exception $e) {
            $result = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonModel($result);
        }
    }
}
