<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 23-11-17
 * Time: 14:00
 */

namespace Report\Factory;

use Report\Controller\IndexController;
use Report\Form\ReportForm;
use Report\Service\ReportService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(ReportService::class);

        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $reportForm = new ReportForm($entitymanager);

        return new IndexController($dataService, $reportForm);
    }
}
