<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 23-11-17
 * Time: 14:11
 */

namespace Report\Factory;

use Report\Controller\ExcelController;
use Report\Service\ExcelService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ExcelControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $excelService = $container->get(ExcelService::class);

        return new ExcelController($excelService);
    }
}
