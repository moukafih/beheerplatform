<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Category\Controller;

use Category\Service\CategoryService;
use Zend\Mvc\Controller\AbstractActionController;

class DeleteController extends AbstractActionController
{
    /**
     * @var CategoryService $categoryService
     */
    private $categoryService;

    /**
     * DeleteController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function indexAction()
    {
        $category_id = (int) $this->getEvent()->getRouteMatch()->getParam('category');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$category_id) {
            return $this->redirect()->toRoute('category/index', ['action' => 'index']);
        }

        $category = $this->categoryService->get($category_id);

        if ($category == null) {
            return $this->redirect()->toRoute('category/index', ['action' => 'index']);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'category_id'    => $category_id,
                'category' => $category,
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->categoryService->delete($id);
        }

        return $this->redirect()->toRoute('category/index', ['action' => 'index']);
    }
}
