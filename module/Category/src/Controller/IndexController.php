<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:31
 */

namespace Category\Controller;

use Category\Service\CategoryService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /** @var \Category\Service\CategoryService $categoryService */
    private $categoryService;

    /**
     * CategoryController constructor.
     * @param \Category\Service\CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function indexAction()
    {
        return [
            'categories' => $this->categoryService->getList()
        ];
    }
}
