<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 10:15
 */

namespace Category\Controller;

use DoctrineModule\Validator\UniqueObject;
use Category\Form\CategoryForm;
use Category\Service\CategoryService;
use Survey\Entity\Category;
use Zend\Mvc\Controller\AbstractActionController;

class SaveController extends AbstractActionController
{
    /** @var \Category\Service\CategoryService $categoryService */
    private $categoryService;

    /** @var \Category\Form\CategoryForm $categoryForm */
    private $categoryForm;

    /**
     * CategoryController constructor.
     * @param \Category\Service\CategoryService $categoryService
     * @param \Category\Form\CategoryForm $categoryForm
     */
    public function __construct(CategoryService $categoryService, CategoryForm $categoryForm)
    {
        $this->categoryService = $categoryService;
        $this->categoryForm = $categoryForm;
    }

    public function indexAction()
    {
        $category_id = (int) $this->getEvent()->getRouteMatch()->getParam('category');

        if (0 === $category_id) {
            $this->categoryForm->remove('id');
        }

        $viewData = [
            'form' => $this->categoryForm,
        ];

        /** @var \Survey\Entity\Category $category */
        $category = new Category();

        if ($category_id > 0) {
            $category = $this->categoryService->get($category_id);
            $viewData['category_id'] = $category_id;
        }

        if ($category_id > 0 && $category == null) {
            return $this->redirect()->toRoute('category/index', ['action' => 'index']);
        }

        $this->categoryForm->bind($category);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->categoryForm->setData($postData);

        if (! $this->categoryForm->isValid()) {
            return $viewData;
        }

        $entity = $this->categoryForm->getData();

        try {
            $this->categoryService->save($entity);
        } catch (\Exception $exception) {
            $viewData['message'] = $exception->getMessage();
            return $viewData;
        }

        return $this->redirect()->toRoute('category/index', ['action' => 'index']);
    }
}
