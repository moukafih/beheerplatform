<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 25-1-18
 * Time: 11:06
 */

namespace Category\Service;

use Doctrine\ORM\EntityManager;
use Survey\Entity\Category;

class CategoryService
{
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    private $entityManager;

    /**
     * CategoryService constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(Category::class)->findOneBy(['id'=> $id]);
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->entityManager->getRepository(Category::class)->findAll();
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function save($entity)
    {
        if (! $entity instanceof Category) {
            throw new \Exception('Category object is required');
        }

        if ($entity->getId() == null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function delete($id)
    {
        $question = $this->entityManager->find(Category::class, $id);
        if ($question) {
            $this->entityManager->remove($question);
            $this->entityManager->flush();
        }
    }
}
