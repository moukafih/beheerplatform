<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-11-17
 * Time: 12:27
 */

namespace Category\Form;

use Survey\Entity\Category;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Fieldset;
use Zend\Form\Element;
use Zend\InputFilter;

use Zend\InputFilter\InputFilterProviderInterface;

class CategoryFieldset extends Fieldset implements InputFilterProviderInterface, ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('category');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject(new Category());

        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    private function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'type',
            'options' => [
                'label' => 'Type',
                'value_options' => [
                    'lead_sales' => 'Mystery Lead Sales',
                    'lead_aftersales' => 'Mystery Lead Aftersales',
                    'feedback_sales' => 'Mystery Feedback Sales',
                    'feedback_aftersales' => 'Mystery Feedback Aftersales',
                ],
            ],
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'parent',
            'options' => [
                'label' => 'Parent',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Survey\Entity\Category',
                //'property'       => 'name',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getName();
                },
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findAll'
                ],
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'type' => [
                'required' => true,
            ],
            'name' => [
                'required' => true,
            ],
            'parent' => [
                'required' => false,
            ],
        ];
    }
}
