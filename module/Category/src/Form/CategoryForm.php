<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 25-1-18
 * Time: 9:41
 */

namespace Category\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Form;

class CategoryForm extends Form implements ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function addElements()
    {

        // Add the user fieldset, and set it as the base fieldset
        $categoryFieldset = new CategoryFieldset($this->objectManager);
        $categoryFieldset->setName('category');
        $categoryFieldset->setUseAsBaseFieldset(true);

        // We don't want City relationship, so remove it !!
        //$categoryFieldset->remove('city');

        $this->add($categoryFieldset);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
            ],
        ]);

        /*$submit = new Element\Submit('submit');
        $submit->setValue('Save');

        $this->add($submit);*/
    }
}
