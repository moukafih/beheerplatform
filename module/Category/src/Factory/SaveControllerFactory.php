<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Category\Factory;

use Category\Form\CategoryForm;
use Category\Controller\SaveController;
use Category\Service\CategoryService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(CategoryService::class);
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $categoryForm = new CategoryForm($entityManager);
        return new SaveController($dataService, $categoryForm);
    }
}
