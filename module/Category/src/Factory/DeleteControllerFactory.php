<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 12:07
 */

namespace Category\Factory;

use Category\Controller\DeleteController;
use Category\Service\CategoryService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class DeleteControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(CategoryService::class);
        return new DeleteController($dataService);
    }
}
