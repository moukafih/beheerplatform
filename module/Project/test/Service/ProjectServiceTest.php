<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 30-11-17
 * Time: 12:34
 */

namespace ProjectTest\Service;

use Application\Entity\Project;
use ApplicationTest\TestCase;
use Project\Service\ProjectService;

class ProjectServiceTest extends TestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Project\Service\ProjectService
     */
    private $service;

    /**
     * @var \Application\Entity\Client
     */
    private $client;

    public function setUp()
    {
        $this->entityManager = $this->getEntityManager();
        $this->service = new ProjectService($this->entityManager);
        $this->client = $this->entityManager->getRepository('Application\Entity\Client')->find(1);
    }

    public function testCanAddProject()
    {
        $entity = new Project();
        $entity->exchangeArray([
            'client' => $this->client,
            'type' => 'lead_sales',
            'wave' => 'Meetronde 4',
            'label' => 'Test',
            'year' => '2017',
            'description' => 'test',
        ]);

        $this->service->save($entity);

        $projects = $this->service->getProjects(1);
        /*
        print $key;
        var_dump($projects);*/
        $this->assertTrue(in_array($entity, $projects));

        $key = array_search($entity, $projects);
        $project = $projects[$key];
        $this->assertEquals($entity->getType(), $project->getType());

        return $project;
    }

    /**
     * @depends testCanAddProject
     * @param \Application\Entity\Project $entity
     * @return null|object
     */
    public function testCanEditProject(Project $entity)
    {
        $entity->setWave('Meetronde 5');
        $this->service->save($entity);

        $project = $this->service->get($entity->getId());

        $this->assertEquals($entity->getWave(), $project->getWave());

        return $project;
    }

    /**
     * @depends testCanEditProject
     * @param \Application\Entity\Project $entity
     */
    public function testCanDeleteProject(Project $entity)
    {
        $this->service->delete($entity->getId());

        $project = $this->service->get($entity->getId());

        $this->assertNull($project);
    }
}
