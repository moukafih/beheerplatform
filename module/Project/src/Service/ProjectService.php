<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:12
 */

namespace Project\Service;

use Application\Entity\Project;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class ProjectService
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * FeedService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $client
     * @return array
     */
    public function getProjects($client)
    {
        return $this->entityManager->getRepository(Project::class)->findBy(['client' => $client, 'archive' => false]);
    }

    /**
     * @param $id
     * @return Project|null|object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(Project::class)->findOneBy(['id'=> $id]);
    }

    /**
     * @param \Application\Entity\Project $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function save($entity)
    {
        if (! $entity instanceof Project) {
            throw new \Exception('Project object is required');
        }

        if ($entity->getId() == null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function delete($id)
    {
        $project = $this->entityManager->find(Project::class, $id);
        if ($project) {
            $this->entityManager->remove($project);
            $this->entityManager->flush();
        }
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function archive($id)
    {
        $project = $this->entityManager->find(Project::class, $id);
        if ($project) {
            $project->setArchive(true);
            $this->entityManager->flush();
        }
    }

    /**
     * @param \Application\Entity\Project $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function duplicate($entity)
    {
        if (! $entity instanceof Project) {
            throw new \Exception('Project object is required');
        }

        $project = clone $entity;

        /** @var \Doctrine\Common\Collections\ArrayCollection $questions */
        /** @var \Survey\Entity\Question $question */
        /** @var \Survey\Entity\QuestionOption $option */
        $questions = $entity->getQuestions();

        $this->entityManager->detach($entity);

        $project->setVisits(new ArrayCollection());

        $project->setQuestions(new ArrayCollection());

        $this->entityManager->persist($project);

        foreach ($questions as $question) {
            $question_c = clone $question;
            $this->entityManager->detach($question);
            $question_c->setProject($project);
            $question_c->setOptions(new ArrayCollection());
            $this->entityManager->persist($question_c);

            $question_options = $question->getOptions();
            foreach ($question_options as $option) {
                $option_c = clone $option;
                $this->entityManager->detach($option);
                $option_c->setQuestion($question_c);
                $this->entityManager->persist($option_c);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param $client
     * @return Project[]|array
     */
    public function getArchive($client)
    {
        return $this->entityManager->getRepository(Project::class)->findBy(['client' => $client, 'archive' => true]);
    }
}
