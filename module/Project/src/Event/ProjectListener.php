<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 11:06
 */

namespace Project\Event;

use Renault\Entity\Wave;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

class ProjectListener
{
    use ListenerAggregateTrait;

    public function attachShared(SharedEventManagerInterface $manager)
    {
        $this->listeners[] = $manager->attach('Application\Entity\Project', EntityEvent::EVENT_PROJECT_ADD, [$this, 'addProject'], -100);
    }

    public function addProject(EntityEvent $event)
    {
        $name = $event->getName();

        /** @var \Application\Entity\Project $entity */
        $entity = $event->getParam('project');

        $date = new \DateTime('now');

        error_log("[" . $date->format("d-m-Y H:i:s") . "] Event1:" . $name . " ; " . $entity->getWave() . " is uitgelogd\n", 3, "./my-project.log");

        $appConfig = require __DIR__ . '/../../../../config/application.config.php';
        if (file_exists(__DIR__ . '/../../../../config/development.config.php')) {
            $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/../../../../config/development.config.php');
        }
        // Run the application!
        $application = Application::init($appConfig);
        $serviceManager = $application->getServiceManager();

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $serviceManager->get('doctrine.entitymanager.orm_renault');

        $wave = new Wave();
        $wave->exchangeArray([
            'type' => $entity->getType(),
            'wave' => $entity->getWave(),
        ]);

        $em->persist($wave);
        $em->flush();
    }
}
