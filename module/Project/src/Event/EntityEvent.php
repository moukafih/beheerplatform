<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-12-17
 * Time: 10:56
 */

namespace Project\Event;

use Zend\EventManager\Event;
use Zend\Mvc\ApplicationInterface;

class EntityEvent extends Event
{
    /**#@+
     * Mvc events triggered by eventmanager
     */
    const EVENT_PROJECT_ADD = 'project.add';

    /**#@-*/
}
