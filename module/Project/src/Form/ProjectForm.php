<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 9:33
 */

namespace Project\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Element;
use Zend\Form\Form;

class ProjectForm extends Form implements ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function addElements()
    {

        // Add the user fieldset, and set it as the base fieldset
        $projectFieldset = new ProjectFieldset($this->objectManager);
        $projectFieldset->setName('project');
        $projectFieldset->setUseAsBaseFieldset(true);

        // We don't want City relationship, so remove it !!
        //$projectFieldset->remove('city');

        $this->add($projectFieldset);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
            ],
        ]);

        /*$submit = new Element\Submit('submit');
        $submit->setValue('Save');

        $this->add($submit);*/
    }
}
