<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-11-17
 * Time: 12:27
 */

namespace Project\Form;

use Application\Entity\Project;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Fieldset;
use Zend\Form\Element;
use Zend\InputFilter;

use Zend\InputFilter\InputFilterProviderInterface;

class ProjectFieldset extends Fieldset implements InputFilterProviderInterface, ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('project');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject(new Project());

        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    private function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'client',
            'options' => [
                'label' => 'Client',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Application\Entity\Client',
                'property'       => 'name',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findAll'
                ],
            ],
            'attributes' => [
                //'disabled' => 'disabled',
            ]
        ]);

        /*$this->add([
            'type' => Element\Select::class,
            'name' => 'type',
            'options' => [
                'label' => 'Type',
                'value_options' => [
                    'lead_sales' => 'Mystery Lead Sales',
                    'lead_aftersales' => 'Mystery Lead Aftersales',
                    'feedback_sales' => 'Mystery Feedback Sales',
                    'feedback_aftersales' => 'Mystery Feedback Aftersales',
                ],
            ],
        ]);*/

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'type',
            'options' => [
                'label' => 'Type',
                'value_options' => [
                    'lead_sales' => 'Mystery Lead Sales',
                    'lead_aftersales' => 'Mystery Lead Aftersales',
                    'feedback_sales' => 'Mystery Feedback Sales',
                    'feedback_aftersales' => 'Mystery Feedback Aftersales',
                ],
            ],
        ]);


        $this->add([
            'name' => 'wave',
            'type' => 'text',
            'options' => [
                'label' => 'Wave',
            ],
        ]);

        $this->add([
            'name' => 'label',
            'type' => 'text',
            'options' => [
                'label' => 'Label',
            ],
        ]);

        $this->add([
            'name' => 'year',
            'type' => 'text',
            'options' => [
                'label' => 'Year',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Description',
            ],
            'attributes' => [
                'placeholder' => 'Description',
            ]
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'competitor',
            'options' => [
                'label' => 'Competitor',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'archive',
            'options' => [
                'label' => 'Archive',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        $inputFilter->add([
            'name'     => 'label',
            'required' => true,
            'filters'  => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Zend\Validator\NotEmpty',
                    'options' => [],
                ],
                [
                    'name' => 'Zend\Validator\StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 256,
                    ],
                ],
            ]
        ]);

        $this->setInputFilter($inputFilter);
    }

    public function getInputFilterSpecification()
    {
        return [
            'type' => [
                'required' => true,
            ],
            'wave' => [
                'required' => true,
            ],
        ];
    }
}
