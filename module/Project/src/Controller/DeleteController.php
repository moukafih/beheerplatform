<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Project\Controller;

use Project\Service\ProjectService;
use Zend\Mvc\Controller\AbstractActionController;

class DeleteController extends AbstractActionController
{
    /**
     * @var ProjectService $projectService
     */
    private $projectService;

    /**
     * VisitController constructor.
     * @param ProjectService $projectService
     */
    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$project_id) {
            return $this->redirect()->toRoute('project/index', ['action' => 'index']);
        }

        $project = $this->projectService->get($project_id);

        if ($project == null) {
            return $this->redirect()->toRoute('project/index', ['action' => 'index']);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'project_id'    => $project_id,
                'project' => $project,
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->projectService->delete($id);
        }

        return $this->redirect()->toRoute('project/index', ['action' => 'index']);
    }
}
