<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 10:15
 */

namespace Project\Controller;

use Application\Entity\Project;
use Project\Form\ProjectForm;
use Project\Service\ProjectService;
use Zend\Mvc\Controller\AbstractActionController;

class SaveController extends AbstractActionController
{
    /** @var \Project\Service\ProjectService $projectService */
    private $projectService;

    /** @var \Project\Form\ProjectForm $projectForm */
    private $projectForm;

    /**
     * ProjectController constructor.
     * @param \Project\Service\ProjectService $projectService
     * @param \Project\Form\ProjectForm $projectForm
     */
    public function __construct(ProjectService $projectService, ProjectForm $projectForm)
    {
        $this->projectService = $projectService;
        $this->projectForm = $projectForm;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');

        if (0 === $project_id) {
            $this->projectForm->remove('id');
        }

        $viewData = [
            'form' => $this->projectForm,
        ];

        /** @var \Application\Entity\Project $project */
        $project = new Project();

        if ($project_id > 0) {
            $project = $this->projectService->get($project_id);
            $viewData['project_id'] = $project_id;
        }

        if ($project_id > 0 && $project == null) {
            return $this->redirect()->toRoute('project/index', ['action' => 'index']);
        }

        $this->projectForm->bind($project);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->projectForm->setData($postData);

        if (! $this->projectForm->isValid()) {
            return $viewData;
        }

        /** @var \Application\Entity\Project $entity */
        $entity = $this->projectForm->getData();

        try {
            $this->projectService->save($entity);
        } catch (\Exception $exception) {
            $viewData['message'] = $exception->getMessage();
            return $viewData;
        }

        return $this->redirect()->toRoute('project/index', ['action' => 'index']);
    }

    public function duplicateAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');

        if (0 === $project_id) {
            return $this->redirect()->toRoute('project/index', ['action' => 'index']);
        }

        $viewData = [
            'form' => $this->projectForm,
        ];

        /** @var \Application\Entity\Project $project */
        $project = new Project();

        if ($project_id > 0) {
            $project = $this->projectService->get($project_id);
            $viewData['project_id'] = $project_id;
        }

        if ($project_id > 0 && $project == null) {
            return $this->redirect()->toRoute('project/index', ['action' => 'index']);
        }

        $this->projectForm->bind($project);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->projectForm->setData($postData);

        if (! $this->projectForm->isValid()) {
            return $viewData;
        }

        /** @var \Application\Entity\Project $entity */
        $entity = $this->projectForm->getData();

        try {
            $this->projectService->duplicate($entity);
        } catch (\Exception $exception) {
            $viewData['message'] = $exception->getMessage();
            return $viewData;
        }

        return $this->redirect()->toRoute('project/index', ['action' => 'index']);
    }
}
