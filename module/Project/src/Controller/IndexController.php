<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:31
 */

namespace Project\Controller;

use Project\Service\ProjectService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /**
     * @var ProjectService $projectService
     */
    private $projectService;

    /**
     * ProjectController constructor.
     * @param ProjectService $projectService
     */
    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function indexAction()
    {
        $client = (int) $this->getEvent()->getRouteMatch()->getParam('client');

        $archives = $this->projectService->getArchive($client);
        return [
            'projects' => $this->projectService->getProjects($client),
            'count_archive' => count($archives)
        ];
    }
}
