<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Project\Factory;

use Project\Form\ProjectForm;
use Project\Controller\SaveController;
use Project\Service\ProjectService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(ProjectService::class);
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $projectForm = new ProjectForm($entitymanager);
        return new SaveController($dataService, $projectForm);
    }
}
