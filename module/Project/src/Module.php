<?php

namespace Project;

use Project\Event\ProjectListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();

        //$serviceManager = $application->getServiceManager();
        $eventManager = $application->getEventManager();
        $sharedManager = $eventManager->getSharedManager();

        $accessListener = new ProjectListener();
        $accessListener->attachShared($sharedManager);
    }
}
