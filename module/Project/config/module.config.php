<?php
namespace Project;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'project' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/project',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/index[/:client]',
                            'constraints' => [
                                'client'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action'     => 'index',
                                'client'     => 1,
                            ],
                        ],
                    ],
                    'save' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/save[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\SaveController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'duplicate' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/duplicate[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\SaveController::class,
                                'action'     => 'duplicate',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/delete[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\DeleteController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'archive-list' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/archive/index[/:client]',
                            'constraints' => [
                                'client' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ArchiveController::class,
                                'action'     => 'index',
                                'client'     => 1,
                            ],
                        ],
                    ],
                    'archive' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/archive[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ArchiveController::class,
                                'action'     => 'archive',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class  => Factory\IndexControllerFactory::class,
            Controller\SaveController::class   => Factory\SaveControllerFactory::class,
            Controller\DeleteController::class => Factory\DeleteControllerFactory::class,
            Controller\ArchiveController::class => Factory\ArchiveControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\ProjectService::class => Factory\ProjectServiceFactory::class,
        ]
    ],

    'view_helpers' => [
        'invokables' => [
            'formRadio' => View\Helper\FormRadio::class
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'acl' => [
        'resources' => [
            'allow' => [
                Controller\IndexController::class  => [
                    'index' => 'user',
                ],
                Controller\SaveController::class   => [
                    'index' => 'admin',
                    'duplicate' => 'admin',
                ],
                Controller\DeleteController::class => [
                    'index' => 'admin',
                ],
                Controller\ArchiveController::class => [
                    'index' => 'admin',
                    'archive' => 'admin',
                ],
            ],
            'deny' => [

            ]
        ]
    ]
];
