<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Question\Factory;

use Question\Form\QuestionForm;
use Question\Controller\SaveController;
use Question\Service\QuestionService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(QuestionService::class);
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $questionForm = new QuestionForm($entitymanager);
        return new SaveController($dataService, $questionForm);
    }
}
