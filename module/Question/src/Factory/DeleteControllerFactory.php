<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 12:07
 */

namespace Question\Factory;

use Question\Controller\DeleteController;
use Question\Service\QuestionService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class DeleteControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(QuestionService::class);
        return new DeleteController($dataService);
    }
}
