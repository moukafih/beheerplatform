<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-7-17
 * Time: 13:31
 */

namespace Question\Controller;

use Question\Service\QuestionService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /** @var \Question\Service\QuestionService $questionService */
    private $questionService;

    /**
     * QuestionController constructor.
     * @param \Question\Service\QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');

        return [
            'project_id' => $project_id,
            'questions' => $this->questionService->getQuestions($project_id)
        ];
    }
}
