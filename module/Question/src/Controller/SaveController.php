<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 10:15
 */

namespace Question\Controller;

use DoctrineModule\Validator\UniqueObject;
use Question\Form\QuestionForm;
use Question\Service\QuestionService;
use Survey\Entity\Question;
use Zend\Mvc\Controller\AbstractActionController;

class SaveController extends AbstractActionController
{
    /** @var \Question\Service\QuestionService $questionService */
    private $questionService;

    /** @var \Question\Form\QuestionForm $questionForm */
    private $questionForm;

    /**
     * QuestionController constructor.
     * @param \Question\Service\QuestionService $questionService
     * @param \Question\Form\QuestionForm $questionForm
     */
    public function __construct(QuestionService $questionService, QuestionForm $questionForm)
    {
        $this->questionService = $questionService;
        $this->questionForm = $questionForm;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $question_id = (int) $this->getEvent()->getRouteMatch()->getParam('question');

        if (0 === $project_id) {
            return $this->redirect()->toRoute('project/index');
        }

        /** @var \Survey\Entity\Question $question */
        $question = new Question();

        if (0 === $question_id) {
            $this->questionForm->remove('id');
        }

        $this->questionForm->get('questionCo')->get('project')->setValue($project_id);

        $viewData = [
            'project_id' => $project_id,
            'form' => $this->questionForm,
        ];

        if ($question_id > 0) {
            $question = $this->questionService->get($question_id);
            $viewData['question_id'] = $question_id;
        }

        if ($question_id > 0 && $question == null) {
            return $this->redirect()->toRoute('question/index', ['action' => 'index', 'project' => $project_id]);
        }

        $this->questionForm->bind($question);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->questionForm->setData($postData);

        if (! $this->questionForm->isValid()) {
            return $viewData;
        }

        $data = $this->questionForm->getData();

        try {
            $this->questionService->save($data);
        } catch (\Exception $exception) {
            $viewData['message'] = $exception->getMessage();
            return $viewData;
        }

        return $this->redirect()->toRoute('question/index', ['action' => 'index', 'project' => $question->getProject()->getId()]);
    }
}
