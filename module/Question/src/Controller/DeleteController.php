<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Question\Controller;

use Question\Service\QuestionService;
use Zend\Mvc\Controller\AbstractActionController;

class DeleteController extends AbstractActionController
{
    /**
     * @var QuestionService $questionService
     */
    private $questionService;

    /**
     * VisitController constructor.
     * @param QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $question_id = (int) $this->getEvent()->getRouteMatch()->getParam('question');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$question_id) {
            return $this->redirect()->toRoute('question/index', ['action' => 'index', 'project' => $project_id]);
        }

        $question = $this->questionService->get($question_id);

        if ($question == null) {
            return $this->redirect()->toRoute('question/index', ['action' => 'index', 'project' => $project_id]);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'project_id'    => $project_id,
                'question_id'    => $question_id,
                'question' => $question,
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->questionService->delete($id);
        }

        return $this->redirect()->toRoute('question/index', ['action' => 'index', 'project' => $project_id]);
    }
}
