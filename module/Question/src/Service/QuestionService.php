<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 15:37
 */

namespace Question\Service;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Validator\UniqueObject;
use Survey\Entity\Question;

class QuestionService
{
    /** @var EntityManager $entitymanager */
    private $entitymanager;

    /**
     * FeedService constructor.
     * @param EntityManager $entitymanager
     */
    public function __construct(EntityManager $entitymanager)
    {
        $this->entitymanager = $entitymanager;
    }

    /**
     * @param $project
     * @return array
     */
    public function getQuestions($project)
    {
        return $this->entitymanager->getRepository(Question::class)->findBy(['project' => $project], ['qnumber' => 'ASC']);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->entitymanager->getRepository(Question::class)->findOneBy(['id'=> $id]);
    }

    /**
     * @param $project_id
     * @return array
     */
    public function getList($project_id)
    {
        $select = "SELECT q, cat, p, t, op FROM Survey\Entity\Question q LEFT JOIN q.category cat LEFT JOIN cat.parent p LEFT JOIN q.type t LEFT JOIN q.options op WHERE q.project = :project";
        $query = $this->entitymanager->createQuery($select);
        $query->setParameters(['project' => $project_id]);
        return $query->getArrayResult();
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function save($entity)
    {
        if (! $entity instanceof Question) {
            throw new \Exception('Question object is required');
        }

        $uniqueValidator = new UniqueObject([
            'object_manager'    => $this->entitymanager,
            'object_repository'  => $this->entitymanager->getRepository(Question::class),
            'use_context'       => false,
            'fields' => ['qnumber', 'project'],
            'messages' => [
                //'objectFound' => 'A user with this email already exists.',
            ],
        ]);

        if (! $uniqueValidator->isValid($entity->getArrayCopy(), $entity->getArrayCopy())) {
            throw new \Exception('Question number is already exists');
        }

        if ($entity->getId() == null) {
            $this->entitymanager->persist($entity);
        }

        $this->entitymanager->flush();
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function delete($id)
    {
        $question = $this->entitymanager->find(Question::class, $id);
        if ($question) {
            $this->entitymanager->remove($question);
            $this->entitymanager->flush();
        }
    }
}
