<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 3-11-17
 * Time: 16:25
 */

namespace Question\Form;

use Survey\Entity\QuestionOption;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\Form\Element;

use Zend\InputFilter\InputFilterProviderInterface;

class QuestionOptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('questionOption');

        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject(new QuestionOption());

        $this->addElements();
        //$this->addInputFilter();
    }

    private function addElements()
    {
        $this->add([
            'type' => Element\Hidden::class,
            'name' => 'id',
        ]);

        $this->add([
            'type'    => Element\Text::class,
            'name'    => 'option',
            'options' => [
                //'label' => 'Option',
            ],
            'attributes' => [
                'placeholder' => 'Option',
                'class' => 'form-control'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name'    => 'points',
            'options' => [
                //'label' => 'Points',
            ],
            'attributes' => [
                'placeholder' => 'Points',
                'class' => 'form-control',
                'min' => '0',
                'max' => '50',
                'step' => '1', // default step interval is 1
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'skipTo',
            'options' => [
                //'label' => 'Skip To',
            ],
            'attributes' => [
                'placeholder' => 'Skip To',
                'class' => 'form-control',
                'min' => '0',
                'max' => '50',
                'step' => '1', // default step interval is 1
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'photo_required',
            'options' => [
                'label' => 'Photo required',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
        ]);

        $this->add([
            'type' => Element\Button::class,
            'name' => 'removeOption',
            'options' => [
                'label' => '-',
            ],
            'attributes' => [
                'class' => 'removeButton'
            ],
        ]);
    }

    /*public function addInputFilter()
    {

    }*/

    public function getInputFilterSpecification()
    {
        return [
            'id' => [
                'required' => false,
            ],
            'option' => [
                'required' => true,
            ],
            'points' => [
                'required' => false,
            ],
            'skipTo' => [
                'required' => false,
            ],
            'photo_required' => [
                'required' => false,
            ],
        ];
    }
}
