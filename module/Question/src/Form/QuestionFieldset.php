<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-11-17
 * Time: 12:27
 */

namespace Question\Form;

use DoctrineModule\Validator\NoObjectExists;
use DoctrineModule\Validator\ObjectExists;
use DoctrineModule\Validator\UniqueObject;
use Survey\Entity\Question;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Fieldset;
use Zend\Form\Element;
use Zend\InputFilter;

use Zend\InputFilter\InputFilterProviderInterface;

class QuestionFieldset extends Fieldset implements ObjectManagerAwareInterface, InputFilterProviderInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('questionCo');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject(new Question());

        $this->addElements();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    private function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'project',
            'type' => 'hidden',
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'qnumber',
            'options' => [
                'label' => 'Question number',
            ],
            'attributes' => [
                'min' => '1',
                'max' => '100',
                'step' => '1', // default step interval is 1
            ],
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'category',
            'options' => [
                'label' => 'Category',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Survey\Entity\Category',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'property'       => 'name',
                'optgroup_identifier' => 'parentName',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findAll',
                    'orderBy'  => ['name' => 'ASC'],
                ],
            ],
            'attributes' => [
                //'disabled' => 'disabled',
            ]
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'type',
            'options' => [
                'label' => 'Type',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Survey\Entity\QuestionType',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'property'       => 'type',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findAll'
                ],
            ],
            'attributes' => [
                'class' => 'question_type',
                //'disabled' => 'disabled',
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'question',
            'options' => [
                'label' => 'Question',
            ],
            'attributes' => [
                'placeholder' => 'Question',
            ]
        ]);

        $this->add([
            'type'  => Element\File::class,
            'name' => 'image',
            'options' => [
                'label' => 'Image',
            ],
            'attributes' => [
                'placeholder' => 'Image',
            ]
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'optional',
            'options' => [
                'label' => 'Optional',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
        ]);

        $this->add([
            'name' => 'instruction',
            'type' => 'textarea',
            'options' => [
                'label' => 'Instruction',
            ],
            'attributes' => [
                'placeholder' => 'Instruction',
            ]
        ]);

        $this->add([
            'type' => Element\Collection::class,
            'name'    => 'options',
            'options' => [
                'label' => 'Options',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                /*'target_element' => [
                    'type' => CategoryFieldset::class,
                ],*/
                'target_element' => new QuestionOptionsFieldset($this->getObjectManager())
            ],
            'attributes' => [
                'class' => 'form-group row options',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'id' => [
                'required' => false,
            ],
            'project' => [
                'required' => true,
            ],
            'qnumber' => [
                'required' => true,
                /*'validators' => [
                    [
                        'name' => UniqueObject::class,
                        'options' => [
                            'object_manager' => $this->objectManager,
                            'object_repository' => $this->getObjectManager()->getRepository(Question::class),
                            'fields' => ['qnumber', 'project'],
                            'use_context' => false,
                            'messages' => array(
                                'objectNotUnique' => 'Sorry, a project with this identifier already exists !',
                            )
                        ],
                    ]
                ],*/
            ],
            'category' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Zend\Validator\NotEmpty',
                        'options' => [],
                    ],
                ]
            ],
            'type' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Zend\Validator\NotEmpty',
                        'options' => [],
                    ],
                ]
            ],
            'question' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Zend\Validator\NotEmpty',
                        'options' => [],
                    ],
                ]
            ],
        ];
    }
}
