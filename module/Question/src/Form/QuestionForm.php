<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 2-11-17
 * Time: 15:40
 */

namespace Question\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use DoctrineModule\Validator\UniqueObject;
use Survey\Entity\QuestionOption;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;

class QuestionForm extends Form implements ObjectManagerAwareInterface, InputFilterProviderInterface
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    private function addElements()
    {
        // Add the user fieldset, and set it as the base fieldset
        $questionFieldset = new QuestionFieldset($this->objectManager);
        $questionFieldset->setName('questionCo');
        $questionFieldset->setUseAsBaseFieldset(true);

        // We don't want City relationship, so remove it !!
        //$questionFieldset->remove('city');

        $this->add($questionFieldset);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            /*'questionCo' => [
                'validators' => [
                    [
                        'name' => UniqueObject::class,
                        'options' => [
                            'object_manager' => $this->objectManager,
                            'object_repository' => $this->getObjectManager()->getRepository('Survey\Entity\Question'),
                            'fields' => ['qnumber', 'project'],
                            'use_context' => false,
                            'messages' => [
                                'objectNotUnique' => 'Sorry, a question with this number already exists !',
                            ]
                        ],
                    ]
                ],
            ],*/
        ];
    }
}
