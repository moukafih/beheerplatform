<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User;

use Zend\Authentication\AuthenticationService;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

//use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [

        //'router_class' => Zend\Mvc\I18n\Router\TranslatorAwareTreeRouteStack::class,
        'translator_text_domain' => 'router',
        // Open configuration for all possible routes
        'routes' => [
            // Define a new route called "blog"
            'user' => [
                // Define a "literal" route type:
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    // Listen to "/blog" as uri:
                    'route' => '/user',
                    // Define default controller and action to be called when
                    // this route is matched
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],

                'may_terminate' => true,
                'child_routes'  => [
                    'login' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route' => '/login[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\LoginController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route' => '/logout',
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action'     => 'logout',
                            ],
                        ],
                    ],
                ]

            ]
        ],

    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => 'User\Factory\IndexControllerFactory',
            Controller\LoginController::class => 'User\Factory\LoginControllerFactory',
        ],
    ],

    'service_manager' => [
        'aliases' => [
            AuthenticationService::class => 'doctrine.authenticationservice.orm_default',
            'translator' => 'MvcTranslator',
        ],
        'factories' => [
            //__NAMESPACE__ . '\Cache\Redis' => __NAMESPACE__ . '\Factory\RedisFactory',
            //'doctrine.sql_logger_collector.other_orm' => new \DoctrineORMModule\Service\SQLLoggerCollectorFactory('other_orm'),
            'User\Service\User' => 'User\Factory\UserServiceFactory',
            'User\Acl\MyAcl' => 'User\Factory\AclFactory'
        ],
    ],

    'view_manager' => [
        'template_map' => [
            'error/403'       => __DIR__ . '/../view/error/403.phtml',
            'user/login'       => __DIR__ . '/../view/user/login/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    /*'view_helpers' => [
        'factories' => [
            'navigation' => 'User\Factory\NavigationServiceFactory'
        ]
    ],*/

    'acl' => [

        'use_database_storage' => false,
        'redirect_route' => [
            'guest' => [
                'params' => [
                    'controller' => 'login',
                    'action' => 'index',
                ],
                'options' => [
                    'name' => 'user/login'
                ]
            ],
            //'options' => array(
            // We should redirect to an action Controller accessable for everyone. And this is "home" route
            // There should be a rule in the Acl allowing every role access to the action and controller
            // Usually this is the homepage action in our case CsnCms\Controller\Index action frontPageAction
            // the route 'home' = '/' should be overriden by CsnCms
            // In the case we are using login we enter an endless redirect. If you are loged in in the system as a member
            // to hide from the navigation the login action the coleagues are using Acl to deny access to login.
            // The CsnAuthorisation trys to redirect to not accessable action loginAction and it gets redirected back to it.
            // Much better is to redirect to an action for sure accessable from everyone and there is no better candidate than the homepage
            // the landing page for the requests to the domain.
            //'name' => 'home' // 'login',
            //)
        ],
        /**
         * Access Control List
         * -------------------
         */
        'roles' => [
            'guest' => null,
            'user' => 'guest',
            'advisor' => 'user',
            'nbeyond' => 'user',
            'country' => 'user',
            'admin' => 'user',
            
        ],
        'resources' => [
            'allow' => [
                'User\Controller\IndexController' => [
                    'index' => 'user',
                    'logout' => 'user',
                ],
                'User\Controller\LoginController' => [
                    'index' => 'guest',
                    'xhr' => 'guest',
                ],

                'Zend' => [
                    'uri' => 'user'
                ],
            ],
            'deny' => [
                'Application\Controller\VisitController' => [
                    'index' => 'guest',
                ],
                'User\Controller\LoginController' => [
                    'index' => 'user',
                ]
            ]
        ]
    ]
];
