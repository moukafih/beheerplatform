<?php
namespace User\Service;

use Renault\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;

use Zend\EventManager\EventManagerAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;

class UserService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    public $eventIdentifier = 'user_identifier';

    /**
     * @var AuthenticationService $authService
     */
    protected $authService;

    protected $sessionManager;

    /**
     * UserService constructor.
     * @param AuthenticationService $authService
     */
    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param $email
     * @param $password
     * @param $rememberme
     * @return array
     */
    public function login($email, $password, $rememberme)
    {
        /**
         * @var \DoctrineModule\Authentication\Adapter\ObjectRepository $adapter
         */
        $adapter = $this->authService->getAdapter();

        $adapter->setIdentity($email);
        $adapter->setCredential($password);
        $select = $adapter->getDbSelect();
        $select->where('active = "TRUE"');

        $result = $this->authService->authenticate();

        if (! $result->isValid()) {
            return ['status' => 'faild', 'messages' => $result->getMessages()];
        }

        $identity = $result->getIdentity();
        $this->authService->getStorage()->write($identity);
        $time = 1209600;//14 dagen
        if ($rememberme == true) {
            $session = new \Zend\Session\SessionManager();
            $session->rememberMe($time);
        }
        $this->authService->getStorage()->write($identity);

        //$this->getEventManager()->trigger(AccessEvent::EVENT_LOGIN, null, [ 'email' => $identity->getEmail() ]);

        return ['status' => 'valid', 'user' => ['id' => 1, 'email' => $email, 'discr' => "ggdfgfg"], 'token' => base64_encode("sdfsdfsfsfsdfsdf"), 'method' => 'hmac-sha-256', 'secret' => base64_encode("fsdfsfsfsdf"), 'expiration' => strtotime('+1 day', strtotime(date("Y-m-d H:i:s")))];
    }

    public function doSomething()
    {
        $this->getEventManager()->trigger(__FUNCTION__, $this, ['foo' => 'bar']);
    }

    public function logout()
    {
        $this->authService->clearIdentity();
        $this->getSessionManager()->forgetMe();
    }

    /**
     * @param User $user
     * @param $passwordGiven
     * @return bool
     */
    public static function verifyHashedPassword(User $user, $passwordGiven)
    {
        $bcrypt = new Bcrypt([
            'cost' => 10
        ]);
        return $bcrypt->verify($passwordGiven, $user->getPassword());
    }

    /**
     *
     * @return EntityManager
     */
    public function getSessionManager()
    {
        if (null === $this->sessionManager) {
            $config = new \Zend\Session\Config\StandardConfig();
            $config->setOptions([
                // 'remember_me_seconds' => 1800,
                'name' => 'nieuws'
            ]);
            $this->sessionManager = new \Zend\Session\SessionManager($config);
        }
        return $this->sessionManager;
    }
}
