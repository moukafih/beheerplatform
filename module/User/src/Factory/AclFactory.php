<?php
/**
 * Created by PhpStorm.
 * User: i.moukafih
 * Date: 09-04-17
 * Time: 13:52
 */

namespace User\Factory;

use Interop\Container\ContainerInterface;

use User\Acl\MyAcl;
use User\Acl\MyAclDb;
use Zend\ServiceManager\Factory\FactoryInterface;

class AclFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        if ($config['acl']['use_database_storage']) {
            $entityManager = $container->get('doctrine.entitymanager.orm_default');
            return new MyAclDb($entityManager);
        }
        return new MyAcl($config);
    }
}
