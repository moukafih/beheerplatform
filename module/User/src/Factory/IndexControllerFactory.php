<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-4-17
 * Time: 10:51
 */

namespace User\Factory;

use User\Controller\IndexController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authService = $container->get('doctrine.authenticationservice.orm_default');

        return new IndexController($authService);
    }
}
