<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-4-17
 * Time: 10:51
 */

namespace User\Factory;

use User\Form\LoginForm;
use User\Controller\LoginController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authService = $container->get('doctrine.authenticationservice.orm_default');
        $loginForm = new LoginForm();

        return new LoginController($authService, $loginForm);
    }
}
