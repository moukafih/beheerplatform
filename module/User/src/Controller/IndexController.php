<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-4-17
 * Time: 12:53
 */

namespace User\Controller;

use User\Event\AccessEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public $eventIdentifier = 'logout_identifier';

    /**
     * @var AuthenticationService $authService
     */
    private $authService;

    /**
     * IndexController constructor.
     * @param AuthenticationService $authService
     */
    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
        /*$this->getEventManager()->addIdentifiers([
            'user_identifier',
        ]);*/
    }

    public function indexAction()
    {
        /*$user = $this->identity();
        if (! $user) {
            // someone is logged !
            return $this->redirect()->toRoute('user/login');
        }

        $events = $this->getEventManager();
        $events->trigger('user.login', $this, []);*/

        return new ViewModel();
    }

    public function logoutAction()
    {
        $identity = $this->identity();
        $this->getEventManager()->trigger(AccessEvent::EVENT_LOGOUT, null, ['email' => $identity->getEmail()]);
        $this->authService->clearIdentity();

        $request = $this->getRequest();
        if (! $request->isPost() || ! $request->isXmlHttpRequest()) {
            return $this->redirect()->toRoute('user/login');
        }

        $view = new ViewModel();
        $view->setTerminal(true);

        return new JsonModel(['status' => 'success']);
    }
}
