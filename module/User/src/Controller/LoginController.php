<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-4-17
 * Time: 11:28
 */

namespace User\Controller;

use User\Event\AccessEvent;
use User\Form\LoginForm;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

class LoginController extends AbstractActionController
{
    public $eventIdentifier = 'login_identifier';

    /**
     * @var AuthenticationService $authService
     */
    private $authService;

    /**
     * @var LoginForm $loginForm
     */
    private $loginForm;

    /**
     * UserController constructor.
     * @param AuthenticationService $authService
     * @param LoginForm $loginForm
     */
    public function __construct(AuthenticationService $authService, LoginForm $loginForm)
    {
        $this->authService = $authService;
        $this->loginForm = $loginForm;
        /*$this->getEventManager()->addIdentifiers([
            'user_identifier',
        ]);*/
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: X-Requested-With");
        
        $view = new ViewModel();

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [ 'form' => $this->loginForm ];
        }

        $this->loginForm->setData($request->getPost());

        if (! $this->loginForm->isValid() && ! $request->isXmlHttpRequest()) {
            return [ 'form' => $this->loginForm ];
        }

        $email = $this->loginForm->get('email')->getValue();
        $password = $this->loginForm->get('password')->getValue();
        //$rememberMe = $request->getPost('rememberme');

        //$result = $this->userService->login($email, $password, $rememberMe);

        $adapter = $this->authService->getAdapter();
        $adapter->setIdentity($email);
        $adapter->setCredential($password);

        $authResult = $this->authService->authenticate();

        if (! $authResult->isValid() && ! $request->isXmlHttpRequest()) {
            return new ViewModel([
                'form' => $this->loginForm,
                'error' => 'Your authentication credentials are not valid',
            ]);
        }

        $identity = $authResult->getIdentity();

        if (! $identity->isActive() && ! $request->isXmlHttpRequest()) {
            return new ViewModel([
                'form' => $this->loginForm,
                'error' => 'Your account is not activated',
            ]);
        }

        $this->authService->getStorage()->write($identity);

        //var_dump($this->getEventManager()->getIdentifiers());
        $this->getEventManager()->trigger(AccessEvent::EVENT_LOGIN, null, ['email' => $identity->getEmail()]);

        if (! $request->isXmlHttpRequest()) {
            //return $this->redirect()->toRoute('user');
            return $this->redirect()->toRoute('project/index');
        }

        $view->setTerminal(true);
        return new JsonModel(['status' => 'success', 'user' => ['id' => $identity->getId()]]);
    }


    public function xhrAction()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $request = $this->getRequest();

        if (! $request->isXmlHttpRequest() || ! $request->isPost()) {
            return $this->redirect()->toRoute('user/login');
        }
        
        $this->loginForm->setData($request->getPost());

        if (! $this->loginForm->isValid()) {
            return [ 'form' => $this->loginForm ];
        }

        $email = $this->loginForm->get('email')->getValue();
        $password = $this->loginForm->get('password')->getValue();
      

        $adapter = $this->authService->getAdapter();
        $adapter->setIdentity($email);
        $adapter->setCredential($password);

        $authResult = $this->authService->authenticate();

        if (! $authResult->isValid()) {
            return new JsonModel(['status' => 'error', 'user' => null, 'message' => 'Unauthorized']);
        }

        $identity = $authResult->getIdentity();

        if (! $identity->isActive()) {
            return new JsonModel(['status' => 'error', 'user' => null, 'message' => 'Your account is not activated']);
        }

        $this->authService->getStorage()->write($identity);
        
        $this->getEventManager()->trigger(AccessEvent::EVENT_LOGIN, null, ['email' => $identity->getEmail()]);
        

        return new JsonModel(['status' => 'success', 'user' => ['id' => $identity->getId()]]);
    }
}
