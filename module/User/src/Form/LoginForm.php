<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 30-7-16
 * Time: 11:57
 */

namespace User\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('user');

        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'Email',
            ],
            'attributes' => []
        ])
            ->getInputFilter()
            ->add([
                'name'     => 'email',
                'required' => true,
                'filters'  => [
                    ['name' => 'Zend\Filter\StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Zend\Validator\NotEmpty',
                        'options' => [],
                    ],
                    [
                        'name' => 'Zend\Validator\EmailAddress',
                        'options' => [],
                    ],
                    [
                        'name' => 'Zend\Validator\StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 256,
                        ],
                    ],
                ]
            ]);

        $this->add([
            'type' => 'password',
            'name' => 'password',
            'options' => [
                'label' => 'Password',
            ],
        ])->getInputFilter()
            ->add([
                'name'     => 'password',
                'required' => true,
                'filters'  => [
                    ['name' => 'Zend\Filter\StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Zend\Validator\NotEmpty',
                        'options' => [

                        ],
                    ],
                    [
                        'name' => 'Zend\Validator\StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 256,
                        ],
                    ],
                ]
            ]);

        $this->add([
            'type' => 'submit',
            'name' => 'login',
            'attributes' => [
                'value' => 'Login',
            ],
        ]);
    }
}
