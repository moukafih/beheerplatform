<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 13-10-16
 * Time: 13:26
 */

namespace User\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use User\Entity\User;

class UserFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('admin@nbeyond.nl');

        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $password = $bcrypt->create('pa$$w0rd');
        $user->setPassword($password);
        $user->setRole('admin');
        $user->setActive(true);

        $manager->persist($user);

        /*88888888888888888888888888888888888888*/

        $user = new User();
        $user->setEmail('user@nbeyond.nl');

        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $password = $bcrypt->create('pa$$w0rd');
        $user->setPassword($password);
        $user->setRole('user');
        $user->setActive(true);

        $manager->persist($user);

        /*88888888888888888888888888888888888888*/

        $user = new User();
        $user->setEmail('rvanlaak@nbeyond.nl');

        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $password = $bcrypt->create('pa$$w0rd');
        $user->setPassword($password);
        $user->setRole('advisor');
        $user->setActive(true);

        $manager->persist($user);

        /*88888888888888888888888888888888888888*/
        $user = new User();
        $user->setEmail('ewilhelm@nbeyond.nl');

        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $password = $bcrypt->create('pa$$w0rd');
        $user->setPassword($password);
        $user->setRole('advisor');
        $user->setActive(true);

        $manager->persist($user);

        /*88888888888888888888888888888888888888*/
        $user = new User();
        $user->setEmail('srook@nbeyond.nl');

        $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        $password = $bcrypt->create('pa$$w0rd');
        $user->setPassword($password);
        $user->setRole('advisor');
        $user->setActive(true);

        $manager->persist($user);

        $manager->flush();
    }
}
