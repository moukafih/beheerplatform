<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User;

use User\Event\AccessListener;
use User\Event\RouteListner as MyRouteListner;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param ModuleManager $moduleManager
     */
    public function init(ModuleManager $moduleManager)
    {
        /**
         * @var \Zend\EventManager\EventManager $eventManager
         */
        $eventManager = $moduleManager->getEventManager();

        /**
         * @var \Zend\EventManager\SharedEventManager $sharedEventManager
         */
        $sharedEventManager = $eventManager->getSharedManager();

        //echo "User init <br/>";
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        //echo "User onBootstrap <br/>";

        $application = $event->getApplication();

        $serviceManager = $application->getServiceManager();
        $eventManager = $application->getEventManager();
        $sharedManager = $eventManager->getSharedManager();

        $accessListener = new AccessListener();
        $accessListener->attachShared($sharedManager);

        $routeListener = new MyRouteListner();
        $routeListener->attach($eventManager);
    }
}
