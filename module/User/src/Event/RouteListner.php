<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 10-4-17
 * Time: 14:06
 */

namespace User\Event;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class RouteListner extends AbstractListenerAggregate
{

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'onRoute'], -100);
    }

    public function onRoute(MvcEvent $event)
    {
        if ($event->getRequest() instanceof \Zend\Console\Request) {
            return;
        }

        /**
         * @var \Zend\Mvc\Application $application
         */
        $application = $event->getApplication();
        $routeMatch = $event->getRouteMatch();
        $sm = $application->getServiceManager();
        $eventManager = $application->getEventManager();
        $auth = $sm->get('Zend\Authentication\AuthenticationService');
        $acl = $sm->get('User\Acl\MyAcl');
        // everyone is guest until logging in
        $role = \User\Acl\MyAcl::DEFAULT_ROLE;

        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();

            $role = $user->getRole();
        }

        if (! in_array($role, ['user', 'advisor','nbeyond', 'country', 'admin'])) {
            $role = \User\Acl\MyAcl::DEFAULT_ROLE;// The default role is guest $acl
        }

        $route = $routeMatch->getMatchedRouteName();
        $controller = $routeMatch->getParam('controller');
        $action = $routeMatch->getParam('action');
        if (!$acl->hasResource($controller)) {
            throw new \Exception('Resource ' . $controller . ' not defined');
            //return;
        }

        /**
         * @var \Zend\View\Model\ViewModel $model
         */
        $model = $event->getViewModel();

        $model->setVariable('acl', $acl);
        $model->setVariable('role', $role);

        if ($acl->isAllowed($role, $controller, $action)) {
            return;
        }

        if ($role == 'guest') {
            $url = $event->getRouter()->assemble([], ['name' => 'user/login']);
            $response=$event->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $url);
            $response->setStatusCode(302);
            $response->sendHeaders();
            // When an MvcEvent Listener returns a Response object,
            // It automatically short-circuit the Application running
            // -> true only for Route Event propagation see Zend\Mvc\Application::run

            // To avoid additional processing
            // we can attach a listener for Event Route with a high priority
            $stopCallBack = function ($event) use ($response) {
                $event->stopPropagation();
                return $response;
            };
            //Attach the "break" as a listener with a high priority
            $event->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, -10000);
            return $response;
        }

        $event->setError("guard-unauthorized");
        $event->setParam('exception', new \Exception(
            'You are not authorized to access this resource',
            403
        ));

        $model->setTemplate("layout/layout");

        $errorView = new ViewModel();
        $errorView->setTemplate('error/403');
        $errorView->setVariable('message', 'Not authorized.');

        $model->addChild($errorView, 'content');

        $response = $event->getResponse();
        $response->setStatusCode(403);

        $event->setResponse($response);
        $event->setResult($model);
    }
}
