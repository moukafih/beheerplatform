<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-4-17
 * Time: 15:46
 */

namespace User\Event;

use Zend\EventManager\Event;
use Renault\Entity\User;

class AccessEvent extends Event
{
    const EVENT_SUBMIT             = 'user.submit';
    const EVENT_LOGIN              = 'user.login';
    const EVENT_LOGOUT             = 'user.logout';

    protected $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}
