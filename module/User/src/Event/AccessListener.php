<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 7-4-17
 * Time: 15:35
 */

namespace User\Event;

use Zend\EventManager\Event;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\EventManager\SharedEventManagerInterface;

class AccessListener
{
    use ListenerAggregateTrait;

    public function attachShared(SharedEventManagerInterface $manager)
    {
        $this->listeners[] = $manager->attach('login_identifier', AccessEvent::EVENT_LOGIN, [$this, 'logLogin'], 100);
        $this->listeners[] = $manager->attach('logout_identifier', AccessEvent::EVENT_LOGOUT, [$this, 'logLogout'], 100);
    }

    public function logLogin(Event $event)
    {
        $name = $event->getName();
        $email = $event->getParam('email');
        $date = new \DateTime('now');

        error_log("[" . $date->format("d-m-Y H:i:s") . "] Event:" . $name . " ; " . $email . " is ingelogd\n", 3, "./my-login.log");
    }

    public function logLogout(Event $event)
    {
        $name = $event->getName();
        $email = $event->getParam('email');
        $date = new \DateTime('now');

        error_log("[" . $date->format("d-m-Y H:i:s") . "] Event:" . $name . " ; " . $email . " is uitgelogd\n", 3, "./my-login.log");
    }
}
