<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 1-12-17
 * Time: 11:23
 */

namespace VisitTest\Service;

use Application\Entity\Project;
use Application\Entity\Visit;
use Application\Entity\VisitRenault;
use Project\Service\ProjectService;
use Visit\Service\VisitService;

class VisitServiceTest extends \ApplicationTest\TestCase
{
    /** @var \Doctrine\ORM\EntityManager */
    private $entityManager;

    /** @var ProjectService */
    private $projectService;

    /** @var VisitService */
    private $visitService;

    /** @var Project */
    private $project;

    /** @var  \Renault\Entity\Dealer */
    private $location_renault;

    /** @var  \Renault\Entity\User */
    private $advisor;

    public function setUp()
    {
        /*$this->entityManager  = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();*/

        $this->entityManager = $this->getEntityManager();
        $this->projectService = new ProjectService($this->entityManager);
        $this->visitService = new VisitService($this->entityManager);

        $client = $this->entityManager->getRepository('Application\Entity\Client')->find(1);
        $this->location_renault = $this->entityManager->getRepository('Renault\Entity\Dealer')->find(187);
        $this->advisor = $this->entityManager->getRepository('Renault\Entity\User')->findOneBy(['email' => 'beheer@nbeyond.nl']);

        $this->project = new Project();

        $this->project->exchangeArray([
            'client' => $client,
            'type' => 'lead_sales',
            'wave' => 'Meetronde 4',
            'label' => uniqid(),
            'year' => '2017',
            'description' => 'test',
        ]);

        $this->entityManager->persist($this->project);
        $this->entityManager->flush();

        $this->project = $this->entityManager->getRepository('Application\Entity\Project')->findOneBy(['label' => $this->project->getLabel()]);

        //$this->projectService->save($this->project);
    }

    public function testCanAddVisitRenault()
    {
        $entity = new VisitRenault();
        $entity->exchangeArray([
            'project' => $this->project,
            'location'  => $this->location_renault,
            'advisor'  => $this->advisor,
            'date'  => new \DateTime('2017-12-14'),
            'remark'  => 'test',
        ]);

        $this->visitService->save($entity);

        $visit = $this->entityManager->getRepository('Application\Entity\VisitRenault')->findOneBy(['project' => $this->project->getId(), 'location' => $this->location_renault, 'advisor' => $this->advisor]);
        $this->assertEquals($entity->getRemark(), $visit->getRemark());

        return $visit;
    }

    /**
     * @depends testCanAddVisitRenault
     * @param \Application\Entity\visit $entity
     */
    public function testCanDeleteProject(Visit $entity)
    {
        $this->visitService->delete($entity->getId());

        $visit = $this->visitService->get($entity->getId());

        $this->assertNull($visit);
    }

    public function tearDown()
    {
        //$this->projectService->delete($this->project->getId());

        parent::tearDown();
        //$this->entityManager->remove($this->project);
        //$this->entityManager->flush();
    }
}
