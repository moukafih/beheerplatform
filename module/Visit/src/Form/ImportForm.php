<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:48
 */

namespace Visit\Form;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class ImportForm extends Form implements ObjectManagerAwareInterface
{
    protected $objectManager;

    public function __construct($entitymanager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->objectManager = $entitymanager;
        $this->addElements();
        $this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function addElements()
    {
        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'project',
            'options' => [
                'label' => 'Project',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Application\Entity\Project',
                //'property'       => 'name',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getLabel() . ' - ' . $targetEntity->getWave();
                },
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => ['client' => 1],

                        // Use key 'orderBy' if using ORM
                        //'orderBy'  => ['name' => 'ASC'],

                        // Use key 'sort' if using ODM
                        //'sort'  => ['name' => 'ASC'],
                    ],
                ],
                'option_attributes' => [
                    'data-competitor' => function (\Application\Entity\Project $entity) {
                        return $entity->isCompetitor()? 1 : 0;
                    },
                ],
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'competitor',
            'options' => [
                'label' => 'Competitor',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0,
            ],
        ]);

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'overwrite',
            'options' => [
                'label' => 'Overwrite',
                'value_options' => [
                    [
                        'value' => 'NO',
                        'label' => 'No',
                        'selected' => true,
                    ],
                    [
                        'value' => 'YES',
                        'label' => 'Yes',
                    ],
                ],
            ],
        ]);

        // File Input
        $file = new Element\File('excel');
        $file->setLabel('Excel Upload');
        $file->setAttribute('id', 'excel');

        $this->add($file);

        $submit = new Element\Submit('submit');
        $submit->setValue('Upload');

        $this->add($submit);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        $fileInput = new InputFilter\FileInput('excel');
        $fileInput->setRequired(true);

        $fileInput->getValidatorChain()
            ->attachByName('filesize', ['max' => 2048000])
            //->attachByName('filemimetype',  ['mimeType' => 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'])
            ->attachByName('Zend\Validator\File\Extension', ['extension' => ['xls', 'xlsx']]);

        $fileInput->getFilterChain()->attachByName(
            'filerenameupload',
            [
                'target'    => './data/uploads/excel',
                'overwrite' => false,
                'randomize' => true,
                'use_upload_name' => false,
                'use_upload_extension' => true,
            ]
        );
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
    }
}
