<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:54
 */

namespace Visit\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Form;

class FilterForm extends Form
{
    protected $objectManager;

    public function __construct(ObjectManager $objectManager, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'GET');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager));
        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function addElements()
    {
        // from
        $this->add([
            'type' => 'Zend\Form\Element\Text',
            'name' => 'start',
            'options' => [
                'label' => 'start',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
            ],
            'attributes' => [
                'class' => 'filter-start date',
            ],
        ], ['priority' => 7]);

        // till
        $this->add([
            'type' => 'Zend\Form\Element\Text',
            'name' => 'end',
            'options' => [
                'label' => 'end',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
            ],
            'attributes' => [
                'class' => 'filter-end date',
            ],
        ], ['priority' => 6]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'advisor',
            'attributes' => [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ],
            'options' => [
                'label' => 'Advisor',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Renault\Entity\User',
                'property'       => 'name',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => ['role' => 'nbeyond'],
                        'orderBy'  => ['name' => 'ASC'],
                    ],
                ],
            ],
        ], ['priority' => 2]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                //'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ],
        ], ['priority' => 1]);
    }
}
