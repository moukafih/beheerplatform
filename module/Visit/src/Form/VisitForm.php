<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 24-7-17
 * Time: 11:04
 */

namespace Visit\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\InputFilter;
use Zend\Form\Element;
use Zend\Form\Form;

class VisitForm extends Form
{
    protected $visitFieldset;

    public function __construct($visitFieldset, $name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->visitFieldset = $visitFieldset;
        $this->addElements();
    }

    public function addElements()
    {
        // We don't want City relationship, so remove it !!
        //$visitFieldset->remove('city');

        $this->add($this->visitFieldset);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
            ],
        ], ['priority' => 1]);

        $submit = new Element\Submit('submit');
        $submit->setValue('Save');

        $this->add($submit);
    }
}
