<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 6-11-17
 * Time: 12:27
 */

namespace Visit\Form;

use Application\Entity\Visit;
use Application\Entity\VisitRenault;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Form\Fieldset;
use Zend\Form\Element;
use Zend\InputFilter;

use Zend\InputFilter\InputFilterProviderInterface;

class VisitFieldset extends Fieldset implements ObjectManagerAwareInterface
{
    protected $objectManager;
    protected $competitor;

    public function __construct(ObjectManager $objectManager, $visit, $competitor = false)
    {
        parent::__construct('visit');
        $this->objectManager = $objectManager;
        $this->setHydrator(new DoctrineHydrator($objectManager))
            ->setObject($visit);

        $this->competitor = $competitor;

        $this->addElements();
        //$this->addInputFilter();
    }

    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    protected function addElements()
    {
        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'project',
            'type' => 'hidden',
        ]);

        $this->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'advisor',
            'options' => [
                'label' => 'Advisor',
                'object_manager' => $this->getObjectManager(),
                'target_class'   => 'Renault\Entity\User',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'property'       => 'name',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => ['role' => ['nbeyond', 'admin']],
                        'orderBy'  => ['name' => 'ASC'],
                    ],
                ],
            ],
        ], ['priority' => 4]);

        $this->add([
            'name' => 'date',
            'type' => 'date',
            'options' => [
                'label' => 'Datum',
            ],
        ], ['priority' => 3]);

        $this->add([
            'name' => 'remark',
            'type' => 'textarea',
            'options' => [
                'label' => 'remark',
            ],
        ], ['priority' => 2]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();

        $inputFilter->add([
            'name'     => 'location',
            'required' => true,
            'filters'  => [
                ['name' => 'Zend\Filter\StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Zend\Validator\NotEmpty',
                    'options' => [],
                ],
                [
                    'name' => 'Zend\Validator\StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 256,
                    ],
                ],
            ]
        ]);

        $this->setInputFilter($inputFilter);
    }
}
