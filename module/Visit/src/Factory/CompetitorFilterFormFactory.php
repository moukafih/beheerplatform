<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 22-11-17
 * Time: 12:11
 */

namespace Visit\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Visit\Form\FilterForm;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class CompetitorFilterFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $filterForm = new FilterForm($entityManager, 'visitfilter');

        $select = new \Zend\Form\Element\Select('brand');
        $select->setLabel('brand');

        $brands = $entityManager->getRepository('Application\Entity\CompetitorDealer')->getBrands();
        $select->setEmptyOption('----');
        $select->setAttributes([
            'class' => 'selectpicker',
            'data-live-search' => 'true',
        ]);
        $select->setLabelAttributes([
            'class' => 'col-xs-6 col-sm-4 col-md-2',
        ]);
        $select->setValueOptions($brands);
        $filterForm->add($select, ['priority' => 5]);

        /*$filterForm->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'brand',
            'attributes' => [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ],

            'options' => [
                'label' => 'Brand',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
                'object_manager' => $entityManager,
                'target_class'   => 'Application\Entity\CompetitorDealer',
                'property' => 'brand',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'getBrands',
                ],
                'option_attributes' => [
                    'class'   => 'styledOption',
                ],
            ],
        ], ['priority' => 5]);*/

        $filterForm->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'location',
            'attributes' => [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ],

            'options' => [
                'label' => 'Dealer',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
                'object_manager' => $entityManager,
                'target_class'   => 'Application\Entity\CompetitorDealer',
                //'property'       => 'name',
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getBrand() . ' - ' . $targetEntity->getName();
                },
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy'  => ['name' => 'ASC'],
                    ],
                ],
                'option_attributes' => [
                    'class'   => 'styledOption',
                    'data-brand' => function (\Application\Entity\CompetitorDealer $entity) {
                        return $entity->getBrand();
                    },
                ],
            ],
        ], ['priority' => 4]);

        return $filterForm;
    }
}
