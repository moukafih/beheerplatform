<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:55
 */

namespace Visit\Factory;

use Visit\Service\VisitService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class VisitServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $db_config = $container->get('config')['db'];

        return new VisitService($entityManager, $db_config);
    }
}
