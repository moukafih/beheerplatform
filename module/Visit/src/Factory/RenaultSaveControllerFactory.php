<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 12:07
 */

namespace Visit\Factory;

use Application\Entity\VisitCompetitor;
use Application\Entity\VisitRenault;
use Visit\Controller\SaveController;
use Visit\Form\VisitForm;
use Visit\Service\VisitService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class RenaultSaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(VisitService::class);
        $visitForm = $container->get('RenaultVisitForm');
        return new SaveController($dataService, $visitForm, new VisitRenault());
    }
}
