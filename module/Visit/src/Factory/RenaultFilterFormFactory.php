<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 22-11-17
 * Time: 12:10
 */

namespace Visit\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Visit\Form\FilterForm;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class RenaultFilterFormFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $filterForm = new FilterForm($entitymanager, 'visitfilter');

        $filterForm->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'hub',
            'attributes' => [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ],
            'options' => [
                'label' => 'Hub',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
                'object_manager' => $entitymanager,
                'target_class'   => 'Renault\Entity\Hub',
                'label_generator' => function ($targetEntity) {
                    return substr($targetEntity->getId(), -3) . ' - ' . $targetEntity->getName();
                },
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => ['country' => 1],
                        'orderBy'  => ['id' => 'ASC'],
                    ],
                ],
            ],
        ], ['priority' => 4]);

        $filterForm->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'location',
            'attributes' => [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ],

            'options' => [
                'label' => 'Dealer',
                'label_attributes' => [
                    'class' => 'col-xs-6 col-sm-4 col-md-2',
                ],
                'object_manager' => $entitymanager,
                'target_class'   => 'Renault\Entity\Dealer',
                'label_generator' => function ($targetEntity) {
                    return substr($targetEntity->getDealerCode(), -3) . ' - ' . $targetEntity->getName();
                },
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => ['country' => 1],
                        'orderBy'  => ['dealer_code' => 'ASC'],
                    ],
                ],
                'option_attributes' => [
                    'class'   => 'styledOption',
                    'data-hub' => function (\Renault\Entity\Dealer $entity) {
                        return $entity->getHub()->getId();
                    },
                ],
            ],
        ], ['priority' => 3]);

        return $filterForm;
    }
}
