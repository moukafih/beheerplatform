<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 12:07
 */

namespace Visit\Factory;

use Visit\Controller\IndexController;

use Visit\Service\VisitService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class RenaultIndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(VisitService::class);
        $filterForm = $container->get('RenaultFilterForm');
        return new IndexController($dataService, $filterForm);
    }
}
