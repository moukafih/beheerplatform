<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:54
 */

namespace Visit\Factory;

use Visit\Form\ImportForm;
use Visit\Controller\ImportController;
use Visit\Service\VisitService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ImportControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(VisitService::class);
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');
        $importForm = new ImportForm($entitymanager, 'importForm');

        return new ImportController($dataService, $importForm);
    }
}
