<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 21-11-17
 * Time: 9:43
 */

namespace Visit\Factory;

use Application\Entity\VisitCompetitor;
use Application\Entity\VisitRenault;
use Interop\Container\ContainerInterface;
use Visit\Form\CompetitorVisitFieldset;
use Visit\Form\VisitFieldset;
use Visit\Form\VisitForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class CompetitorVisitFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entitymanager = $container->get('doctrine.entitymanager.orm_default');

        $visit = new VisitCompetitor();
        $visitFieldset = new VisitFieldset($entitymanager, $visit, true);
        $visitFieldset->setName('visit');
        $visitFieldset->add([
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'location',
            'options' => [
                'label' => 'Dealer',
                'object_manager' => $entitymanager,
                'target_class'   => 'Application\Entity\CompetitorDealer',
                'display_empty_item' => true,
                'empty_item_label'   => '---',
                'label_generator' => function ($targetEntity) {
                    return $targetEntity->getBrand() . ' - ' . $targetEntity->getName();
                },
                'is_method'      => true,
                'find_method'    => [
                    'name'   => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy'  => ['name' => 'ASC'],
                    ],
                ],
            ],
            'attributes' => [
                //'disabled' => 'disabled',
            ]
        ], ['priority' => 5]);
        $visitFieldset->setUseAsBaseFieldset(true);

        $visitForm = new VisitForm($visitFieldset);

        return $visitForm;
    }
}
