<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 12:07
 */

namespace Visit\Factory;

use Application\Entity\VisitCompetitor;
use Visit\Controller\SaveController;
use Visit\Form\VisitForm;
use Visit\Service\VisitService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class CompetitorSaveControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataService = $container->get(VisitService::class);
        $visitForm = $container->get('CompetitorVisitForm');
        return new SaveController($dataService, $visitForm, new VisitCompetitor());
    }
}
