<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:45
 */

namespace Visit\Controller;

use Visit\Form\FilterForm;
use Visit\Service\VisitService;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;

use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;

class IndexController extends AbstractActionController
{
    /**
     * @var VisitService $visitService
     */
    private $visitService;
    /**
     * @var FilterForm $filterForm
     */

    private $filterForm;
    /**
     * VisitController constructor.
     * @param VisitService $visitService
     * @param FilterForm $filterForm
     */
    public function __construct(VisitService $visitService, FilterForm $filterForm)
    {
        $this->visitService = $visitService;
        $this->filterForm = $filterForm;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');

        $filter_params = $this->params()->fromQuery();
        $this->filterForm->setData($filter_params);

        $result = $this->visitService->listByProject($project_id, $filter_params);

        return [
            'form' => $this->filterForm,
            'params' => $this->params()->fromQuery(),
            'project_id' => $project_id,
            'result' => $result
        ];
    }
}
