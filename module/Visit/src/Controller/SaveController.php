<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Visit\Controller;

use Application\Entity\Visit;
use Application\Entity\VisitRenault;
use Visit\Service\VisitService;
use Visit\Form\VisitForm;
use Zend\Mvc\Controller\AbstractActionController;

class SaveController extends AbstractActionController
{
    /** @var \Visit\Service\VisitService $visitService */
    private $visitService;

    /** @var \Visit\Form\VisitForm $visitForm */
    private $visitForm;

    /** @var \Visit\Entity\Visit $visit */
    private $visit;

    /**
     * VisitController constructor.
     * @param \Visit\Service\VisitService $visitService
     * @param \Visit\Form\VisitForm $visitForm
     * @param \Application\Entity\Visit $visit
     */
    public function __construct(VisitService $visitService, VisitForm $visitForm, Visit $visit)
    {
        $this->visitService = $visitService;
        $this->visitForm = $visitForm;
        $this->visit = $visit;
    }

    public function indexAction()
    {
        //$id = (int) $this->params()->fromRoute('id', 0);
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $visit_id = (int) $this->getEvent()->getRouteMatch()->getParam('visit');

        if (0 === $project_id) {
            return $this->redirect()->toRoute('project/index');
        }

        if (0 === $visit_id) {
            $this->visitForm->remove('id');
        }

        $this->visitForm->get('visit')->get('project')->setValue($project_id);

        $viewData = [
            'form' => $this->visitForm,
            'project_id' => $project_id,
            //'params' => $this->params()->fromQuery(),
        ];

        if ($visit_id > 0) {
            $this->visit = $this->visitService->get($visit_id);
            $viewData['visit_id'] = $visit_id;
        }

        $this->visitForm->bind($this->visit);

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $postData = $request->getPost();

        $this->visitForm->setData($postData);

        if (! $this->visitForm->isValid()) {
            return $viewData;
        }

        $visit = $this->visitForm->getData();

        $this->visitService->save($visit);

        //return $this->redirect()->toRoute('visit/index', ['action' => 'index', 'project' => $visit->getProject()->getId()], ['query' => $this->params()->fromQuery()]);
        return $this->redirect()->toRoute('project/index', ['action' => 'index', 'client' => 1]);
    }
}
