<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 27-7-17
 * Time: 11:55
 */

namespace Visit\Controller;

use Visit\Service\VisitService;
use Zend\Mvc\Controller\AbstractActionController;

class DeleteController extends AbstractActionController
{
    /**
     * @var VisitService $visitService
     */
    private $visitService;

    /**
     * VisitController constructor.
     * @param VisitService $visitService
     */
    public function __construct(VisitService $visitService)
    {
        $this->visitService = $visitService;
    }

    public function indexAction()
    {
        $project_id = (int) $this->getEvent()->getRouteMatch()->getParam('project');
        $visit_id = (int) $this->getEvent()->getRouteMatch()->getParam('visit');

        //$id = (int) $this->params()->fromRoute('id', 0);
        if (!$visit_id) {
            return $this->redirect()->toRoute('visit/index', ['action' => 'index', 'project' => $project_id], ['query' => $this->params()->fromQuery()]);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return [
                'project_id'    => $project_id,
                'visit_id'    => $visit_id,
                'visit' => $this->visitService->get($visit_id),
                'params' => $this->params()->fromQuery(),
            ];
        }

        $del = $request->getPost('del', 'No');

        if ($del == 'Yes') {
            $id = (int) $request->getPost('id');
            $this->visitService->delete($id);
        }

        // Redirect to list of albums
        return $this->redirect()->toRoute('project/index', ['action' => 'index', 'client' => 1], ['query' => $this->params()->fromQuery()]);
    }
}
