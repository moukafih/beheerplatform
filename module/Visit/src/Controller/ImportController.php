<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:50
 */

namespace Visit\Controller;

use Visit\Service\VisitService;
use Visit\Form\ImportForm;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ImportController extends AbstractActionController
{
    /**
     * @var VisitService $visitService
     */
    private $visitService;
    /**
     * @var ImportForm $importForm
     */
    private $importForm;

    /**
     * ImportController constructor.
     * @param VisitService $visitService
     * @param ImportForm $importForm
     */
    public function __construct(VisitService $visitService, ImportForm $importForm)
    {
        $this->visitService = $visitService;
        $this->importForm = $importForm;
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return ['form' => $this->importForm];
        }

        $post = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getFiles()->toArray()
        );

        $this->importForm->setData($post);
        if (!$this->importForm->isValid()) {
            return ['form' => $this->importForm];
        }

        $data = $this->importForm->getData();

        $inputFileName = $data['excel']['tmp_name'];

        $visits_added = 0;
        $not_found_dealers = [];
        $not_found_advisors = [];

        $message = "success";

        try {
            list($visits_added, $not_found_dealers, $not_found_advisors) = $this->visitService->import($data, $inputFileName);
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        //TODO show success message
        //$view->setTemplate('import/success');


        return [
            'form' => $this->importForm,
            'message' => $message,
            'visit_added' => $visits_added,
            'not_found_dealers' => $not_found_dealers,
            'not_found_advisors' => $not_found_advisors,
        ];

        //return $this->redirect()->toRoute('zfcadmin/quality-import', ['action' => 'index', 'project' => $data['project']]);
    }
}
