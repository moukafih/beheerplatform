<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 12-4-17
 * Time: 16:49
 */

namespace Visit\Service;

use Application\Entity\CompetitorDealer;
use Application\Entity\Visit;
use Application\Entity\VisitCompetitor;
use Application\Entity\VisitRenault;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use PHPExcel_Shared_Date;
use Renault\Entity\User;

class VisitService
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    private $db_config;
    /**
     * FeedService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, $db_config)
    {
        $this->entityManager = $entityManager;
        $this->db_config = $db_config;
    }

    /**
     * @param $post_data
     * @param $inputFileName
     * @return array
     * @throws \Exception
     */
    public function import($post_data, $inputFileName)
    {
        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

        /****************************************************************************/

        /**  Identify the type of $inputFileName  **/
        //$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        /**  Create a new Reader of the type that has been identified  **/
        //$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        //$spreadsheet = $reader->load($inputFileName);

        //  TODO test the other formats
        /*$objPHPExcel = new \PHPExcel();
        try {

        } catch (\Exception $e) {
            echo 'Error loading file "'.pathinfo($inputFileName, PATHINFO_BASENAME).'": '.$e->getMessage();
        }*/

        // TODO refactoring and header check
        //  Get worksheet dimensions
        $sheet = $spreadsheet->getSheet(0);
        $highestRow = $sheet->getHighestDataRow();
        $highestColumn = $sheet->getHighestDataColumn();

        $not_found_dealers = [];
        $not_found_advisors = [];

        $visits_added = 0;

        $project = $this->entityManager->getRepository('Application\Entity\Project')->findOneBy(['id' => $post_data['project']]);

        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

            /*if (empty($rowData[0][1]) || ! empty($rowData[0][0])) {
                // TODO return dealers and advisors to the view
                continue;
            }*/

            $user = null;
            $dealer = null;
            $date_val = null;

            if ($project->isCompetitor()) {
                $dealer = $this->entityManager->getRepository('Application\Entity\CompetitorDealer')->findOneBy([
                    'brand' => $rowData[0][0],
                    'name' => $rowData[0][1],
                    'city' => $rowData[0][4],
                ]);

                $user = $this->entityManager->getRepository('Renault\Entity\User')->findOneBy(['email' => $rowData[0][7]]);
                $date_val = $rowData[0][6];
            }

            if (! $project->isCompetitor()) {
                $user = $this->entityManager->getRepository('Renault\Entity\User')->findOneBy(['email' => $rowData[0][1]]);
                $dealer = $this->entityManager->getRepository('Renault\Entity\Dealer')->findOneBy(['dealer_code' => $rowData[0][0]]);
                $date_val = $rowData[0][2];
            }

            if (is_numeric($date_val)) {
                $date_val = date('d-m-Y', \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_val));
            }

            if (! $this->validateDate($date_val, 'd-m-Y')) {
                throw new \Exception('invalid date format, date must be d-m-Y');
            }

            if (! $dealer && ! $project->isCompetitor()) {
                $not_found_dealers[] = $rowData[0][0];
            }

            if (! $dealer && $project->isCompetitor()) {
                $dealer = new CompetitorDealer();
                $dealer->exchangeArray([
                    'brand' => $rowData[0][0],
                    'name' => $rowData[0][1],
                    'address' => $rowData[0][2],
                    'zipcode' => $rowData[0][3],
                    'city' => $rowData[0][4],
                ]);

                $this->entityManager->persist($dealer);
                //$not_found_dealers[] = $rowData[0][1];
            }

            if (! $user instanceof User && ! $project->isCompetitor()) {
                $not_found_advisors[] = $rowData[0][1];
            }

            if (! $user instanceof User && $project->isCompetitor()) {
                $not_found_advisors[] = $rowData[0][6];
            }

            if (! $user || ! $dealer) {
                // TODO return dealers and advisors to the view
                continue;
            }

            $visit = null;
            $data = null;

            if ($project->isCompetitor()) {
                $visit = $this->entityManager->getRepository('Application\Entity\VisitCompetitor')->findOneBy([
                    'project' => $project->getId(),
                    'location' => $dealer,
                    'advisor' => $user,
                    'date' => new \DateTime($date_val)
                ]);

                $data = [
                    'project' => $post_data['project'],
                    'location'  => $dealer,
                    'advisor'  => $user,
                    'date' => $date_val,
                ];
            }

            if (! $project->isCompetitor()) {
                $visit = $this->entityManager->getRepository('Application\Entity\VisitRenault')->findOneBy([
                    'project' => $project->getId(),
                    'location' => $dealer,
                    'advisor' => $user,
                    'date' => new \DateTime($date_val)
                ]);

                $data = [
                    'project' => $post_data['project'],
                    'location'  => $dealer,
                    'advisor'  => $user,
                    'date' => $date_val,
                    'remark' => $rowData[0][3],
                ];
            }

            if ($visit != null && $post_data['overwrite'] == "NO") {
                continue;
            }

            if (! $visit && $project->isCompetitor()) {
                $visit = new VisitCompetitor();
            }

            if (! $visit && ! $project->isCompetitor()) {
                $visit = new VisitRenault();
            }

            $hydrator = new DoctrineHydrator($this->entityManager);

            $visit = $hydrator->hydrate($data, $visit);

            $this->entityManager->persist($visit);


            $visits_added += 1;
        }

        $this->entityManager->flush();

        return [$visits_added, $not_found_dealers, $not_found_advisors];
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * @return array
     */
    public function listByProject($project_id, $filter_params = null)
    {
        $project = $this->entityManager->getRepository('Application\Entity\Project')->findOneBy(['id' => $project_id]);

        $select = null;
        if ($project->isCompetitor()) {
            $select = "SELECT vi FROM Application\Entity\VisitCompetitor vi JOIN vi.location lc JOIN vi.advisor ad";
        }

        if (! $project->isCompetitor()) {
            $select = "SELECT vi FROM Application\Entity\VisitRenault vi JOIN vi.location lc JOIN vi.advisor ad";
        }

        $where = "WHERE vi.project = $project_id";

        $params = [];

        if (! empty($filter_params['start'])) {
            $where .= " AND vi.date >= :start";
            $start = new \DateTime($filter_params['start']);
            $params['start'] = $start->format('Y-m-d');
        }

        if (! empty($filter_params['end'])) {
            $where .= " AND vi.date <= :end";
            $end = new \DateTime($filter_params['end']);
            $params['end'] = $end->format('Y-m-d');
        }

        if (! empty($filter_params['hub']) && ! $project->isCompetitor()) {
            $where .= " AND lc.hub = :hub";
            $params['hub'] = $filter_params['hub'];
        }

        if (! empty($filter_params['brand']) && $project->isCompetitor()) {
            $where .= " AND lc.brand = :brand";
            $params['brand'] = $filter_params['brand'];
        }

        if (! empty($filter_params['location'])) {
            $where .= " AND lc.id = :dealer";
            $params['dealer'] = $filter_params['location'];
        }

        if (! empty($filter_params['advisor'])) {
            $where .= " AND ad.id = :advisor";
            $params['advisor'] = $filter_params['advisor'];
        }

        $sql = $select . " " . $where;

        if (! empty($filter_params['sort'])) {
            $sql .= " ORDER BY " . $filter_params['sort'] . " " . $filter_params['order'];
        }

        $query = $this->entityManager->createQuery($sql);
        $query->setParameters($params);
        $sql = $query->getSQL();

        $result = [];
        $result[0] = $project;
        $result[1] = $query->getResult();
        return $result;
    }

    public function get($id)
    {
        return $this->entityManager->getRepository('Application\Entity\Visit')->findOneBy(['id'=> $id]);
    }

    /**
     * @param int $user_id
     * @return array
     *
     * API function
     */
    public function listByUser($user_id)
    {
        $select = "SELECT vi, partial lo.{id, dealer_code, name}, partial ad.{id, name}, partial pr.{id, label, wave}, partial re.{id, version, publish} FROM Application\Entity\VisitRenault vi JOIN vi.location lo JOIN vi.advisor ad JOIN vi.project pr LEFT JOIN vi.report re WHERE ad.id = :user AND vi.active = true AND pr.archive = false";
        $query = $this->entityManager->createQuery($select);
        $query->setParameters(['user' => $user_id]);

        $result1 = $query->getArrayResult();

        $select = "SELECT vi, partial lo.{id, name}, partial ad.{id, name}, partial pr.{id, label, wave}, partial re.{id, version, publish} FROM Application\Entity\VisitCompetitor vi JOIN vi.location lo JOIN vi.advisor ad JOIN vi.project pr LEFT JOIN vi.report re WHERE ad.id = :user AND vi.active = true AND pr.archive = false";
        $query = $this->entityManager->createQuery($select);
        $query->setParameters(['user' => $user_id]);

        $result2 = $query->getArrayResult();

        $mot1Arr = $this->getDealersAvg('data_nl_voc_sales', 'mot1');
        $mot7Arr = $this->getDealersAvg('data_nl_voc_sales', 'mot7');

        foreach ($result1 as $key => $item) {
            $result1[$key]['motinfo'] = [
                'mot1' => $this->getRank($mot1Arr, $item['location']['dealer_code']),
                'mot7' => $this->getRank($mot7Arr, $item['location']['dealer_code']),
            ];
        }

        $merged_result = array_merge($result1, $result2);

        return $merged_result;
    }

    private function getDealersAvg($table, $mot)
    {
        try {
            $dbh = new \PDO($this->db_config['dsn'], $this->db_config['username'], $this->db_config['password'], [\PDO::ATTR_PERSISTENT => true]);
            $result = $dbh->query("SELECT code_dealer, AVG($mot) FROM $table WHERE `date_service` BETWEEN DATE_FORMAT(CURDATE(), '%Y-01-01') AND CURDATE() GROUP BY code_dealer ORDER BY AVG($mot) DESC");
            return $result->fetchAll();
        } catch (\Exception $e) {
            die("Unable to connect: " . $e->getMessage());
        }
    }

    private function getRank($data, $dealer_code)
    {
        $code_dealers = array_column($data, 'code_dealer');
        $key = array_search($dealer_code, $code_dealers);
        $rank = 100*  (1 - $key / count($code_dealers));
        return round($rank, 2);
    }

    /**
     * @param Visit $entity
     */
    public function save(Visit $entity)
    {
        /*if (! $entity instanceof Visit) {
            throw new \Exception('Question object is required');
        }*/

        if ($entity->getId() == null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * @param int $id
     */
    public function delete($id)
    {
        $visit = $this->entityManager->find('Application\Entity\Visit', $id);
        if ($visit) {
            $this->entityManager->remove($visit);
            $this->entityManager->flush();
        }
    }
}
