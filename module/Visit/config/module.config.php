<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Visit;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'visit' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/visit',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'renault_index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/renault/index[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => 'RenaultIndexController',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'renault_save' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/renault/save[/:project[/:visit]]',
                            'defaults' => [
                                'controller' => 'RenaultSaveController',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'competitor_index' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/competitor/index[/:project]',
                            'constraints' => [
                                'project'          => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => 'CompetitorIndexController',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'competitor_save' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/competitor/save[/:project[/:visit]]',
                            'defaults' => [
                                'controller' => 'CompetitorSaveController',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'del' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/del[/:project[/:visit]]',
                            'defaults' => [
                                'controller' => Controller\DeleteController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'import' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/import[/:action[/:project]]',
                            'constraints' => [
                                'controller'  => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'      => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'project'     => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ImportController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            'RenaultIndexController'  => Factory\RenaultIndexControllerFactory::class,
            'CompetitorIndexController'  => Factory\CompetitorIndexControllerFactory::class,
            'CompetitorSaveController'   => Factory\CompetitorSaveControllerFactory::class,
            'RenaultSaveController'   => Factory\RenaultSaveControllerFactory::class,
            Controller\DeleteController::class => Factory\DeleteControllerFactory::class,
            Controller\ImportController::class => Factory\ImportControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            Service\VisitService::class => Factory\VisitServiceFactory::class,
            'CompetitorVisitForm' => Factory\CompetitorVisitFormFactory::class,
            'RenaultVisitForm' => Factory\RenaultVisitFormFactory::class,
            'RenaultFilterForm' => Factory\RenaultFilterFormFactory::class,
            'CompetitorFilterForm' => Factory\CompetitorFilterFormFactory::class,
        ]
    ],

    'view_manager' => [
        'template_map' => [
            'import/success' => __DIR__ . '/../view/visit/import/success.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    'acl' => [
        'resources' => [
            'allow' => [
                'RenaultIndexController'  => [
                    'index' => ['admin', 'country']
                ],
                'CompetitorIndexController'  => [
                    'index' => ['admin', 'country']
                ],
                'CompetitorSaveController'   => [
                    'index' => 'admin'
                ],
                'RenaultSaveController'   => [
                    'index' => 'admin'
                ],
                Controller\DeleteController::class => [
                    'index' => 'admin'
                ],
                Controller\ImportController::class => [
                    'index' => 'admin',
                ],
            ],
        ]
    ]
];
