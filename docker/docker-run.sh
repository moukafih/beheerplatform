#!/bin/bash
echo "-------------------------------------------------------------------"
echo "-                              BEHEERSPLATFORM                    -"
echo "-------------------------------------------------------------------"

composer install
composer update
composer development:disable
composer development:enable

chmod 0777 -R data/uploads

until netcat -z database 3306 2>/dev/null; do
 >&2 echo "DB is unavailable - sleeping"
  sleep 1
done

# Application
#php public/index.php moukafih:database:drop --if-exists --connection orm_default
php public/index.php moukafih:database:create --if-not-exists --connection orm_default
# first create salesafter_beheersplatform
echo "y y" | php public/index.php moukafih:migrations:migrate orm_default Application
echo "y y" | php public/index.php moukafih:migrations:migrate orm_default Survey
#php public/index.php moukafih:fixtures:load orm_default Survey --append
php-cs-fixer fix --verbose --show-progress=estimating

chmod 0777 -R data/proxies
touch ./my-login.log

chmod 0777 ./my-login.log

/usr/bin/apachectl -D FOREGROUND
