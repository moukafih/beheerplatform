#!/usr/bin/env bash
# Delete all containers
docker rm -f $(docker ps -a -q)
docker volume rm -f $(docker volume ls)
# Delete all images
#docker rmi -f $(docker images -q)

mysqldump -u salesafter_beheersplatform -p salesafter_beheersplatform | gzip > salesafter_beheersplatform.sql.gz
mysqldump -u salesafter_benelux -p salesafter_benelux | gzip > salesafter_benelux.sql.gz

sed -i 's|salesafter_beheersplatform|root|' docker/data/salesafter_beheersplatform.sql