<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 5-4-17
 * Time: 11:40
 */

return [

    'navigation' => [

        // navigation with name default
        'default' => [
            [
                'label' => 'Home',
                'route' => 'home',
            ],
            [
                'label' => 'User',
                'route' => 'user',
                'pages' => [
                    [
                        'label' => 'Login',
                        'route' => 'user/login',
                    ],
                ],
            ],
            [
                'label' => 'Application',
                'route' => 'visit',
            ],
        ],

        // navigation with name special
        'special' => [
            [
                'label' => 'Clients',
                'route' => 'client',
                //'controller' => 'client',
                'action' => 'index',
                //'params' => [],
                //'query' => [],
                'order' => 1,
                //'class' => 'special-one',
                //'title' => 'This element has a special class',
                // ACL
                'resource' => 'Application\Controller\ClientController',
                'privilege' => 'index'
            ],
            [
                'label' => 'Projects',
                'route' => 'project/index',
                //'controller' => 'project',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 2,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                'resource' => 'Application\Controller\IndexController',
                'privilege' => 'index'
            ],
            [
                'label' => 'Categories',
                'route' => 'category/index',
                //'controller' => 'project',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 2,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                'resource' => 'Application\Controller\IndexController',
                'privilege' => 'index'
            ],
            [
                'label' => 'Competitors',
                'route' => 'competitor/index',
                //'controller' => 'project',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 2,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                'resource' => 'Application\Controller\IndexController',
                'privilege' => 'index'
            ],
            /*[
                'label' => 'User-manager',
                'route' => 'application',
                'controller' => 'index',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 1,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                //'resource' => ''
                //'privilege' =>
            ],*/
            [
                'label' => 'Import visits',
                'route' => 'visit/import',
                //'controller' => 'import',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 3,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                'resource' => 'Visit\Controller\ImportController',
                'privilege' => 'index'
            ],
            /*[
                'label' => 'Data- export',
                'route' => 'application',
                'controller' => 'index',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 1,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                //'resource' => ''
                //'privilege' =>
            ],
            [
                'label' => 'Email alert / notification',
                'route' => 'application',
                'controller' => 'index',
                'action' => 'index',
                'params' => [],
                'query' => [],
                'order' => 1,
                'class' => 'special-one',
                'title' => 'This element has a special class',
                //'active' => false,
                // ACL
                //'resource' => ''
                //'privilege' =>
            ],*/
            [
                'label' => 'Login',
                'route' => 'user/login',
                //'module' => 'User',
                //'controller' => 'login',
                'action' => 'index',
                'resource' => 'User\Controller\LoginController',
                'privilege' => 'index'
            ],
            [
                'label' => 'Logout',
                'route' => 'user/logout',
                //'module' => 'User',
                'controller' => 'index',
                'action' => 'logout',
                'params' => [],
                'query' => [],
                'order' => 4,
                // ACL
                'resource' => 'User\Controller\IndexController',
                'privilege' => 'logout'
            ],
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            Zend\Navigation\Service\NavigationAbstractServiceFactory::class,
        ],
    ],
];
