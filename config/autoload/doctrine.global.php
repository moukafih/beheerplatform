<?php
/**
 * Created by PhpStorm.
 * User: ismail
 * Date: 4-4-17
 * Time: 17:32
 */

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                //'configuration' => 'orm_default',
                //'eventmanager'  => 'orm_default',
                'driverClass' => 'Doctrine\\DBAL\\Driver\\PDOMySql\\Driver',
                'params' => [
                    'driver' => 'pdo_mysql',
                    'host' => 'localhost',
                    'port' => '3306',
                    'dbname' => 'salesafter_beheersplatform',
                    'charset' => 'utf8',
                    'driverOptions' => []
                ],
                'doctrine_type_mappings' => [
                    'enum' => 'string'
                ],
            ],
            'orm_renault' => [
                //'configuration' => 'orm_default',
                //'eventmanager'  => 'orm_default',
                'driverClass' => 'Doctrine\\DBAL\\Driver\\PDOMySql\\Driver',
                'params' => [
                    'driver' => 'pdo_mysql',
                    'host' => 'localhost',
                    'port' => '3306',
                    //'dbname' => 'salesafter_dashboard',
                    'charset' => 'utf8',
                    'driverOptions' => []
                ],
                'doctrine_type_mappings' => [
                    'enum' => 'string',
                    'bit' => 'string'
                ],
            ],
        ],

        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache'   => 'array',
                'driver' => 'orm_default', // This driver will be defined later
                'generate_proxies' => true,
                'proxy_dir' => 'data/proxies',
                'proxy_namespace' => 'Proxy',
                'filters' => [],
                'datetime_functions' => [],
                'string_functions' => [],
                'numeric_functions' => [],
                'second_level_cache' => []
            ],
            'orm_renault' => [
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache'   => 'array',
                'driver' => 'orm_renault', // This driver will be defined later
                'generate_proxies' => true,
                'proxy_dir' => 'data/proxies',
                'proxy_namespace' => 'Proxy',
                'filters' => [],
                'datetime_functions' => [],
                'string_functions' => [],
                'numeric_functions' => [],
                'second_level_cache' => []
            ],
        ],
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'identity_class' => 'Renault\Entity\User',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => 'User\Service\UserService::verifyHashedPassword'
            ]
        ],
        'authenticationadapter' => [
            'orm_default' => true,
        ],
        'authenticationstorage' => [
            'orm_default' => true,
        ],
        'authenticationservice' => [
            'orm_default' => true,
        ],
        'driver' => [
            'Application' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../module/Application/src/Entity',
                ]
            ],
            'User' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../module/User/src/Entity',
                ]
            ],
            'Survey' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../module/Survey/src/Entity',
                ]
            ],
            'Renault' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../module/Renault/src/Entity',
                ]
            ],
            'orm_default' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => [
                    'Application\Entity' => 'Application',
                    'User\Entity' => 'User',
                    'Survey\Entity' => 'Survey',
                    'Renault\Entity' => 'Renault',
                ]
            ],
            'orm_renault' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => [
                    'Renault\Entity' => 'Renault',
                ]
            ],
        ],
        // now you define the entity manager configuration
        'entitymanager' => [
            'orm_default' => [
                'connection' => 'orm_default',
                'configuration' => 'orm_default'
            ],
            'orm_renault' => [
                'connection' => 'orm_renault',
                'configuration' => 'orm_renault'
            ],
        ],
        'eventmanager' => [
            'orm_default' => []
        ],

        'sql_logger_collector' => [
            'orm_default' => []
        ],
        // entity resolver configuration, allows mapping associations to interfaces
        'entity_resolver' => [
            // configuration for the `doctrine.entity_resolver.orm_default` service
            'orm_default' => [],
        ],
        'mapping_collector' => [
            'orm_default' => [],
        ],
        'formannotationbuilder' => [
            'orm_default' => [],
        ],

        'migrations_configuration' => [
            'orm_default' => [
                'directory' => __DIR__ . '/../../data/migrations',
                'name' => 'Application Migrations',
                'namespace' => 'Application\Migrations',
                'table' => 'migrations',
                'column' => 'version',
                'Application' => [
                    //'connection' => 'doctrine.connection.orm_renault',
                    'directory' => realpath(__DIR__ . '/../../module/Application/config/migrations'),
                    'name' => 'Application Migrations',
                    'namespace' => 'Application\Migrations',
                    'table' => 'migrations',
                    'column' => 'version',
                    //'organize_migrations' => 'year_and_month',
                ],
                'User' => [
                    //'connection' => 'doctrine.connection.orm_renault',
                    'directory' => realpath(__DIR__ . '/../../module/User/config/migrations'),
                    'name' => 'User Migrations',
                    'namespace' => 'User\Migrations',
                    'table' => 'migrations',
                    'column' => 'version',
                    //'organize_migrations' => 'year_and_month',
                ],
                'Survey' => [
                    //'connection' => 'doctrine.connection.orm_renault',
                    'directory' => realpath(__DIR__ . '/../../module/Survey/config/migrations'),
                    'name' => 'Survey Migrations',
                    'namespace' => 'Survey\Migrations',
                    'table' => 'migrations',
                    'column' => 'version',
                    //'organize_migrations' => 'year_and_month',
                ],
            ],
            'orm_renault' => [
                'Renault' => [
                    //'connection' => 'doctrine.connection.orm_renault',
                    'directory' => realpath(__DIR__ . '/../../module/Renault/config/migrations'),
                    'name' => 'Renault Migrations',
                    'namespace' => 'Renault\Migrations',
                    'table' => 'migrations',
                    'column' => 'version',
                    //'organize_migrations' => 'year_and_month',
                ],
            ],
        ],
    ],
];
