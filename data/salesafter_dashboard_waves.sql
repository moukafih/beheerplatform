INSERT INTO waves (id, type, wave, country, external_id) VALUES (1, 'lead_sales', 'Meetronde 3', 1, 1);
INSERT INTO waves (id, type, wave, country, external_id) VALUES (2, 'lead_aftersales', 'Meetronde 3', 1, 2);
INSERT INTO waves (id, type, wave, country, external_id) VALUES (3, 'lead_sales', 'Meetronde 4', 1, 3);
INSERT INTO waves (id, type, wave, country, external_id) VALUES (4, 'lead_aftersales', 'Meetronde 4', 1, 4);
INSERT INTO waves (id, type, wave, country, external_id) VALUES (5, 'feedback_sales', 'Meetronde 3', 1, 7);
INSERT INTO waves (id, type, wave, country, external_id) VALUES (6, 'feedback_aftersales', 'Meetronde 2', 1, 9);