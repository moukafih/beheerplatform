/*visit 1*/

INSERT INTO salesafter_beheersplatform.visits VALUES (1, 1, '2017-06-20 18:00:00', 'ochtend', 1, null, 325,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (1, 1, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES',  'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (1, 1, 1, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (2, 2, 1, 'Ja, binnen 4 uur reactie', 20, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (3, 4, 1, 'Voor ontvangst brochure en na ontvangst brochure', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (4, 5, 1, '2017-06-26 10:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (5, 6, 1, 'Email', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (6, 7, 1, 'Simon', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (7, 8, 1, 'Ja, een bezoek aan de showroom wordt voorgesteld', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (8, 9, 1, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (9, 10, 1, 'open answer', 0, 'test');

INSERT INTO salesafter_beheersplatform.answers VALUES (10, 11, 1, '2 genoemd', 5, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (11, 12, 1, 'Geen genoemd', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (12, 13, 1, 'No', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (13, 14, 1, 'open answer 2', 0, 'test');

/*visit 2*/

INSERT INTO salesafter_beheersplatform.visits VALUES (2, 2, '2017-06-20 18:00:00', 'ochtend', 1, null, 188,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (2, 2, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES', 'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (14, 1, 2, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (15, 2, 2, 'Nee, geen reactie binnen 4 uur (automatic reply is geen contact)', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (16, 3, 2, 'Nee, geen contact met de klant', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (17, 9, 2, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (18, 10, 2, 'open answer', 0, 'test');

/*visit 3*/
INSERT INTO salesafter_beheersplatform.visits VALUES (3, 1, '2017-06-20 18:00:00', 'ochtend', 1, null, 189,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (3, 3, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES',  'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (19, 1, 3, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (20, 2, 3, 'Ja, binnen 4 uur reactie', 20, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (21, 4, 3, 'Voor ontvangst brochure en na ontvangst brochure', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (22, 5, 3, '2017-06-26 10:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (23, 6, 3, 'Email', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (24, 7, 3, 'Simon', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (25, 8, 3, 'Ja, een bezoek aan de showroom wordt voorgesteld', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (26, 9, 3, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (27, 10, 3, 'open answer', 0, 'test');

INSERT INTO salesafter_beheersplatform.answers VALUES (28, 11, 3, '2 genoemd', 5, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (29, 12, 3, 'Geen genoemd', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (30, 13, 3, 'No', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (31, 14, 3, 'open answer 2', 0, 'test');

/*report 4*/
INSERT INTO salesafter_beheersplatform.visits VALUES (4, 2, '2017-06-20 18:00:00', 'ochtend', 1, null, 190,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (4, 4, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES', 'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (32, 1, 4, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (33, 2, 4, 'Nee, geen reactie binnen 4 uur (automatic reply is geen contact)', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (34, 3, 4, 'Nee, geen contact met de klant', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (35, 9, 4, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (36, 10, 4, 'open answer', 0, 'test');

/*visit 5*/
INSERT INTO salesafter_beheersplatform.visits VALUES (5, 1, '2017-06-20 18:00:00', 'ochtend', 1, null, 191,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (5, 5, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES',  'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (37, 1, 5, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (38, 2, 5, 'Ja, binnen 4 uur reactie', 20, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (39, 4, 5, 'Voor ontvangst brochure en na ontvangst brochure', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (40, 5, 5, '2017-06-26 10:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (41, 6, 5, 'Email', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (42, 7, 5, 'Simon', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (43, 8, 5, 'Ja, een bezoek aan de showroom wordt voorgesteld', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (44, 9, 5, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (45, 10, 5, 'open answer', 0, 'test');

INSERT INTO salesafter_beheersplatform.answers VALUES (46, 11, 5, '2 genoemd', 5, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (47, 12, 5, 'Geen genoemd', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (48, 13, 5, 'No', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (49, 14, 5, 'open answer 2', 0, 'test');

/*visit 6*/
INSERT INTO salesafter_beheersplatform.visits VALUES (6, 2, '2017-06-20 18:00:00', 'ochtend', 1, null, 192,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (6, 6, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES', 'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (50, 1, 6, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (51, 2, 6, 'Nee, geen reactie binnen 4 uur (automatic reply is geen contact)', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (52, 3, 6, 'Nee, geen contact met de klant', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (53, 9, 6, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (54, 10, 6, 'open answer', 0, 'test');

/*visit 7*/
INSERT INTO salesafter_beheersplatform.visits VALUES (7, 1, '2017-06-20 18:00:00', 'ochtend', 1, null, 193,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (7, 7, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES',  'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (55, 1, 7, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (56, 2, 7, 'Ja, binnen 4 uur reactie', 20, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (57, 4, 7, 'Voor ontvangst brochure en na ontvangst brochure', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (58, 5, 7, '2017-06-26 10:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (59, 6, 7, 'Email', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (60, 7, 7, 'Simon', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (61, 8, 7, 'Ja, een bezoek aan de showroom wordt voorgesteld', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (62, 9, 7, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (63, 10, 7, 'open answer', 0, 'test');

INSERT INTO salesafter_beheersplatform.answers VALUES (64, 11, 7, '2 genoemd', 5, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (65, 12, 7, 'Geen genoemd', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (66, 13, 7, 'No', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (67, 14, 7, 'open answer 2', 0, 'test');

/*report 8*/
INSERT INTO salesafter_beheersplatform.visits VALUES (8, 2, '2017-06-20 18:00:00', 'ochtend', 1, null, 194,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (8, 8, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES', 'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (68, 1, 8, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (69, 2, 8, 'Nee, geen reactie binnen 4 uur (automatic reply is geen contact)', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (70, 3, 8, 'Nee, geen contact met de klant', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (71, 9, 8, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (72, 10, 8, 'open answer', 0, 'test');

/*visit 9*/
INSERT INTO salesafter_beheersplatform.visits VALUES (9, 1, '2017-06-20 18:00:00', 'ochtend', 1, null, 195,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (9, 9, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES',  'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (73, 1, 9, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (74, 2, 9, 'Ja, binnen 4 uur reactie', 20, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (75, 4, 9, 'Voor ontvangst brochure en na ontvangst brochure', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (76, 5, 9, '2017-06-26 10:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (77, 6, 9, 'Email', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (78, 7, 9, 'Simon', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (79, 8, 9, 'Ja, een bezoek aan de showroom wordt voorgesteld', 10, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (80, 9, 9, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (81, 10, 9, 'open answer', 0, 'test');

INSERT INTO salesafter_beheersplatform.answers VALUES (82, 11, 9, '2 genoemd', 5, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (83, 12, 9, 'Geen genoemd', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (84, 13, 9, 'No', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (85, 14, 9, 'open answer 2', 0, 'test');

/*report 10*/
INSERT INTO salesafter_beheersplatform.visits VALUES (10, 2, '2017-06-20 18:00:00', 'ochtend', 1, null, 196,  864);

INSERT INTO salesafter_beheersplatform.reports VALUES (10, 10, '2017-06-20 18:00:00', '2017-06-20 18:00:00', 'def', 'YES', 'YES', 'YES');

INSERT INTO salesafter_beheersplatform.answers VALUES (86, 1, 10, '2017-06-20 18:00:00', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (87, 2, 10, 'Nee, geen reactie binnen 4 uur (automatic reply is geen contact)', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (88, 3, 10, 'Nee, geen contact met de klant', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (89, 9, 10, 'Nee, na 2 werkdagen', 0, 'test');
INSERT INTO salesafter_beheersplatform.answers VALUES (90, 10, 10, 'open answer', 0, 'test');