create table salesafter_beheersplatform.clients
(
	id int auto_increment
		primary key,
	name longtext not null
)
;

create table salesafter_beheersplatform.migrations
(
	version varchar(255) not null
		primary key
)
;

create table salesafter_beheersplatform.projects
(
	id int auto_increment
		primary key,
	client int null,
	name longtext not null,
	ronde longtext not null,
	year int not null,
	constraint FK_F7A67F75C7440455
		foreign key (client) references salesafter_beheersplatform.clients (id)
)
;

create index IDX_F7A67F75C7440455
	on projects (client)
;

create table salesafter_beheersplatform.users
(
	id int auto_increment
		primary key,
	email varchar(255) not null,
	password varchar(255) not null,
	role varchar(255) not null,
	active tinyint(1) not null,
	constraint UNIQ_5CCD5930E7927C74
		unique (email)
)
;

create table salesafter_beheersplatform.visits
(
	id int auto_increment
		primary key,
	advisor int null,
	date datetime not null,
	remark longtext null,
	location int null,
	project int null,
	report_status int default '0' not null,
	constraint FK_BB0F1D662FB3D0EE
		foreign key (project) references salesafter_beheersplatform.projects (id)
)
;

create index IDX_BB0F1D6619ADC9F4
	on visits (advisor)
;

create index IDX_BB0F1D662FB3D0EE
	on visits (project)
;

create index IDX_BB0F1D665E9E89CB
	on visits (location)
;